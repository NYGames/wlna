﻿using System.Diagnostics;

namespace UBeau
{
    public abstract partial class Pool<T> where T : class
    {
        public delegate T Constructor(Pool<T> inPool);

        public abstract int Capacity { get; }

        public Pool(Constructor inConstructor)
        {
            Assert.True(!typeof(T).IsAbstract && !typeof(T).IsInterface, "Type cannot be constructed.");

            Assert.NotNull(inConstructor, "Cannot provide null constructor for pool.");
            m_Constructor = inConstructor;
        }

        public abstract void Reset();
        public abstract T Pop();
        public abstract void Push(T inValue);

        protected readonly Constructor m_Constructor;

        [Conditional("DEVELOPMENT")]
        protected void VerifyObject(T inObject)
        {
            Assert.NotNull(inObject, "Null object provided for pool.");
            Assert.True(inObject.GetType() == typeof(T), "Object of incorrect type provided for pool.");
        }
    }
}
