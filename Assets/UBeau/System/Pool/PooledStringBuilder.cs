﻿using System;
using System.Text;

namespace UBeau
{
    /// <summary>
    /// Reusable string builder.
    /// </summary>
    public sealed class PooledStringBuilder : IDisposable
    {
        /// <summary>
        /// The actual StringBuilder.
        /// </summary>
        public readonly StringBuilder Builder;

        private PooledStringBuilder()
        {
            Builder = new StringBuilder(64);
        }

        /// <summary>
        /// Clears the string builder and returns it to the pool.
        /// </summary>
        public void Dispose()
        {
            Builder.Length = 0;
            Builder.EnsureCapacity(64);
            s_Pool.Push(this);
        }

        #region Pooling

        static private Pool<PooledStringBuilder> s_Pool = new Pool<PooledStringBuilder>.Static(8, (p) => { return new PooledStringBuilder(); });

        static public PooledStringBuilder Create()
        {
            return s_Pool.Pop();
        }

        static public PooledStringBuilder Create(string inString)
        {
            var builder = s_Pool.Pop();
            builder.Builder.Append(inString);
            return builder;
        }

        #endregion
    }
}
