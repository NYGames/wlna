﻿using System;
using System.Collections.Generic;

namespace UBeau
{
    /// <summary>
    /// Reusable hash set.
    /// </summary>
    public sealed class PooledSet<T> : HashSet<T>, IDisposable
    {
        private PooledSet() { }

        /// <summary>
        /// Clears the set and returns it to the pool.
        /// </summary>
        public void Dispose()
        {
            Clear();
            s_Pool.Push(this);
        }

        #region Pooling

        static private Pool<PooledSet<T>> s_Pool = new Pool<PooledSet<T>>.Static(4, (p) => { return new PooledSet<T>(); });

        static public PooledSet<T> Create()
        {
            return s_Pool.Pop();
        }

        static public PooledSet<T> Create(IEnumerable<T> inSource)
        {
            var set = s_Pool.Pop();
            foreach (var obj in inSource)
                set.Add(obj);
            return set;
        }

        #endregion
    }
}
