﻿namespace UBeau
{
    /// <summary>
    /// Contains lookup tables for common cached values.
    /// Shares string values between int.ToString()
    /// </summary>
    static public class LookupTables
    {
        static private string[] s_IntegerTable;
        private const int INTEGER_MIN = -100;
        private const int INTEGER_MAX = 100;
        private const int INTEGER_TABLE_SIZE = INTEGER_MAX - INTEGER_MIN + 1;

        static LookupTables()
        {
            s_IntegerTable = new string[INTEGER_TABLE_SIZE];
            for (int i = 0; i < INTEGER_TABLE_SIZE; ++i)
                s_IntegerTable[i] = (INTEGER_MIN + i).ToString();
        }

        /// <summary>
        /// Returns a cached version of int.ToString()
        /// Range of [-100, 100], uses default if outside that range.
        /// </summary>
        static public string ToStringLookup(this int inValue)
        {
            if (inValue >= INTEGER_MIN && inValue <= INTEGER_MAX)
                return s_IntegerTable[inValue - INTEGER_MIN];
            return inValue.ToString();
        }
    }
}
