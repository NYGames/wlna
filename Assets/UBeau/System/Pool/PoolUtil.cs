﻿using System.Diagnostics;

namespace UBeau
{
    static public class PoolUtil
    {
        static public Pool<T>.Constructor Constructor<T>() where T : class, new()
        {
            return (pool) => { return new T(); };
        }
    }
}
