﻿using System.Collections.Generic;

namespace UBeau
{
    public abstract partial class Pool<T> where T : class
    {
        /// <summary>
        /// Pool that dynamically expands as more objects are requested.
        /// </summary>
        public sealed class Dynamic : Pool<T>
        {
            private List<T> m_ObjectPool;
            private int m_MinCapacity;

            public override int Capacity { get { return m_ObjectPool.Capacity; } }

            public Dynamic(int inCapacity, Constructor inConstructor)
                : base(inConstructor)
            {
                Assert.True(inCapacity > 0, "Pool capacity {0} not valid.", inCapacity);
                m_MinCapacity = inCapacity;

                m_ObjectPool = new List<T>(m_MinCapacity);
                Reset();
            }

            public override void Reset()
            {
                while (m_ObjectPool.Count < m_MinCapacity)
                {
                    T newObject = m_Constructor(this);
                    VerifyObject(newObject);

                    m_ObjectPool.Add(newObject);
                }
            }

            public override T Pop()
            {
                T pooledObject;

                if (m_ObjectPool.Count > 0)
                {
                    pooledObject = m_ObjectPool[m_ObjectPool.Count - 1];
                    m_ObjectPool.RemoveAt(m_ObjectPool.Count - 1);
                }
                else
                {
                    pooledObject = m_Constructor(this);
                    VerifyObject(pooledObject);
                }

                return pooledObject;
            }

            public override void Push(T inValue)
            {
                VerifyObject(inValue);

                m_ObjectPool.Add(inValue);
            }
        }
    }
}
