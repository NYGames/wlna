﻿using System;
using System.Collections.Generic;

namespace UBeau
{
    /// <summary>
    /// Reusable list.
    /// </summary>
    public sealed class PooledList<T> : List<T>, IDisposable
    {
        private PooledList() { }

        /// <summary>
        /// Clears the list and returns it to the pool.
        /// </summary>
        public void Dispose()
        {
            Clear();
            s_Pool.Push(this);
        }

        #region Pooling

        static private Pool<PooledList<T>> s_Pool = new Pool<PooledList<T>>.Static(4, (p) => { return new PooledList<T>(); });

        static public PooledList<T> Create()
        {
            return s_Pool.Pop();
        }

        static public PooledList<T> Create(IEnumerable<T> inSource)
        {
            var list = s_Pool.Pop();
            list.AddRange(inSource);
            return list;
        }

        #endregion
    }
}
