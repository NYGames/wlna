﻿namespace UBeau
{
    public abstract partial class Pool<T> where T : class
    {
        /// <summary>
        /// Pool with a fixed capacity.
        /// </summary>
        public sealed class Static : Pool<T>
        {
            private T[] m_ObjectPool;
            private int m_Capacity;
            private int m_CurrentIndex;

            public override int Capacity { get { return m_Capacity; } }

            public Static(int inCapacity, Constructor inConstructor)
                : base(inConstructor)
            {
                Assert.True(inCapacity > 0, "Pool capacity {0} not valid.", inCapacity);
                m_Capacity = inCapacity;

                m_ObjectPool = new T[m_Capacity];
                m_CurrentIndex = 0;

                Reset();
            }

            public override void Reset()
            {
                while (m_CurrentIndex < m_Capacity)
                {
                    T newObject = m_Constructor(this);
                    VerifyObject(newObject);

                    m_ObjectPool[m_CurrentIndex++] = newObject;
                }
            }

            public override T Pop()
            {
                T pooledObject;

                if (m_CurrentIndex > 0)
                {
                    pooledObject = m_ObjectPool[--m_CurrentIndex];
                    m_ObjectPool[m_CurrentIndex] = null;
                }
                else
                {
                    pooledObject = m_Constructor(this);
                    VerifyObject(pooledObject);
                }

                return pooledObject;
            }

            public override void Push(T inValue)
            {
                VerifyObject(inValue);

                if (m_CurrentIndex < m_Capacity)
                {
                    m_ObjectPool[m_CurrentIndex++] = inValue;
                }
            }
        }
    }
}
