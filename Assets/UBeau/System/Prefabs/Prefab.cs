﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UBeau
{
    /// <summary>
    /// Links types to prefabs.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public abstract class Prefab : Attribute
    {
        protected abstract void EnsureLoaded<T>() where T : MonoBehaviour;
        protected abstract IEnumerator EnsureLoadedAsync<T>() where T : MonoBehaviour;
        protected abstract void EnsureUnloaded<T>() where T : MonoBehaviour;

        protected abstract bool GetLoaded<T>() where T : MonoBehaviour;
        protected abstract T GetPrefab<T>() where T : MonoBehaviour;

        protected abstract T Spawn<T>() where T : MonoBehaviour;

        #region Loading/Unloading

        /// <summary>
        /// Ensures a prefab is loaded.
        /// </summary>
        static public void Load<T>() where T : MonoBehaviour
        {
            Prefab prefab = GetPrefab(typeof(T), true);
            if (prefab != null)
                prefab.EnsureLoaded<T>();
        }

        /// <summary>
        /// Asynchronously loads a prefab.
        /// </summary>
        static public IEnumerator LoadAsync<T>() where T : MonoBehaviour
        {
            Prefab prefab = GetPrefab(typeof(T), true);
            if (prefab != null)
                return prefab.EnsureLoadedAsync<T>();
            return null;
        }

        /// <summary>
        /// Returns if a prefab is loaded.
        /// </summary>
        static public bool IsLoaded<T>() where T : MonoBehaviour
        {
            Prefab prefab = GetPrefab(typeof(T), false);
            if (prefab != null)
                return prefab.GetLoaded<T>();
            return false;
        }

        /// <summary>
        /// Returns the prefab for this type.
        /// </summary>
        static public T Get<T>() where T : MonoBehaviour
        {
            Prefab prefab = GetPrefab(typeof(T), false);
            if (prefab != null)
                return prefab.GetPrefab<T>();
            return null;
        }

        /// <summary>
        /// Unloads a prefab.
        /// </summary>
        static public void Unload<T>() where T : MonoBehaviour
        {
            Prefab prefab = GetPrefab(typeof(T), false);
            if (prefab != null)
                prefab.EnsureUnloaded<T>();
        }

        #endregion

        #region Instantiation

        /// <summary>
        /// Instantiates an instance of the prefab.
        /// </summary>
        static public T Instantiate<T>() where T : MonoBehaviour
        {
            Prefab prefab = GetPrefab(typeof(T), true);
            if (prefab != null)
                return prefab.Spawn<T>();
            return null;
        }

        /// <summary>
        /// Instantiates an instance of the prefab at a position.
        /// </summary>
        static public T Instantiate<T>(Vector3 inPosition) where T : MonoBehaviour
        {
            Prefab prefab = GetPrefab(typeof(T), true);
            if (prefab != null)
            {
                T obj = prefab.Spawn<T>();
                obj.transform.position = inPosition;
                return obj;
            }
            return null;
        }

        #endregion

        #region Cache

        static private Dictionary<IntPtr, Prefab> s_CachedPrefabs = new Dictionary<IntPtr, Prefab>();

        static private Prefab GetPrefab(Type inType, bool inbLoad)
        {
            Prefab prefab;
            if (!s_CachedPrefabs.TryGetValue(inType.TypeHandle.Value, out prefab) && inbLoad)
            {
                prefab = Types.GetCustomAttribute<Prefab>(inType);
                s_CachedPrefabs.Add(inType.TypeHandle.Value, prefab);
            }

            return prefab;
        }

        #endregion
    }
}
