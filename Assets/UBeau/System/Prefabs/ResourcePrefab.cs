﻿using System;
using System.Collections;
using UnityEngine;

namespace UBeau
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public sealed class ResourcePrefab : Prefab
    {
        public ResourcePrefab(string inPath)
        {
            m_Path = inPath;
        }

        private string m_Path;
        private MonoBehaviour m_Prefab;

        protected override void EnsureLoaded<T>()
        {
            if (m_Prefab.IsNull())
            {
                m_Prefab = Resources.Load<T>(m_Path);
                Assert.NotNull(m_Prefab, "Unable to load resources of type {0} at path {1}", typeof(T).Name, m_Path);
            }
        }

        protected override IEnumerator EnsureLoadedAsync<T>()
        {
            if (m_Prefab.IsNull())
            {
                var request = Resources.LoadAsync<T>(m_Path);
                yield return request;
                m_Prefab = (T)request.asset;
                Assert.NotNull(m_Prefab, "Unable to load resources of type {0} at path {1}", typeof(T).Name, m_Path);
            }
        }

        protected override bool GetLoaded<T>()
        {
            return m_Prefab != null;
        }

        protected override T GetPrefab<T>()
        {
            return (T)m_Prefab;
        }

        protected override void EnsureUnloaded<T>()
        {
            Resources.UnloadAsset(m_Prefab);
            m_Prefab = null;
        }

        protected override T Spawn<T>()
        {
            EnsureLoaded<T>();
            T finished = GameObject.Instantiate<T>((T)m_Prefab);
            return finished;
        }
    }
}
