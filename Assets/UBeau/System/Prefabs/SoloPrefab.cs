﻿using System;
using System.Collections;
using UnityEngine;

namespace UBeau
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public sealed class SoloPrefab : Prefab
    {
        protected override void EnsureLoaded<T>() { }

        protected override IEnumerator EnsureLoadedAsync<T>() { yield break; }

        protected override bool GetLoaded<T>() { return true; }

        protected override T GetPrefab<T>() { return null; }

        protected override void EnsureUnloaded<T>() { }

        protected override T Spawn<T>()
        {
            GameObject rootGO = new GameObject(typeof(T).Name);
            return rootGO.AddComponent<T>();
        }
    }
}
