﻿using System;
using System.Diagnostics;

namespace UBeau
{
    /// <summary>
    /// Diagnostic timer.
    /// </summary>
    public class Timer : IDisposable
    {
        private string m_Name;
        private Stopwatch m_StopWatch;

        private Timer(string inName)
        {
            m_Name = inName;
            Log.Debug("START: {0}", m_Name);
            m_StopWatch = Stopwatch.StartNew();
        }

        /// <summary>
        /// Completes the timer.
        /// </summary>
        public void Dispose()
        {
            if (m_StopWatch != null)
            {
                m_StopWatch.Stop();
                Log.Debug("FINISH: {0} [{1}ms]", m_Name, m_StopWatch.Elapsed.TotalMilliseconds);
                m_StopWatch = null;
            }
        }

        /// <summary>
        /// If in development mode, returns a new timer.
        /// Otherwise, returns null.
        /// </summary>
        static public Timer Start(string inName)
        {
#if DEVELOPMENT
            return new Timer(inName);
#else
            return null;
#endif
        }
    }
}
