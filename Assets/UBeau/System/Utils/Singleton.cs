﻿using UnityEngine;

namespace UBeau
{
    [DisallowMultipleComponent]
    public abstract class Singleton<SType> : MonoBehaviour where SType : Singleton<SType>
    {
        // Current instance
        static private SType s_Instance;

        static private Transform GetRoot()
        {
            GameObject root = GameObject.Find("Singletons");
            if (root == null)
            {
                root = new GameObject("Singletons");
                DontDestroyOnLoad(root);
            }

            return root.transform;
        }

        /// <summary>
        /// Creates the singleton if it doesn't already exist.
        /// </summary>
        static public SType Create()
        {
            if (s_Instance == null)
            {
                s_Instance = FindObjectOfType<SType>();
                if (s_Instance == null)
                {
                    s_Instance = Prefab.Instantiate<SType>();
                    Assert.NotNull(s_Instance, "Cannot find or create singleton of type '{0}'", typeof(SType).Name);
                    s_Instance.transform.SetParent(GetRoot());
                    s_Instance.gameObject.name = typeof(SType).Name + "::Instance";
                }
            }

            return s_Instance;
        }

        /// <summary>
        /// Destroys the singleton.
        /// </summary>
        static public void Destroy()
        {
            if (s_Instance != null)
            {
                Destroy(s_Instance);
                s_Instance = null;
            }
        }

        /// <summary>
        /// Returns the singleton instance.
        /// Will lazily instantiates the singleton.
        /// </summary>
        static public SType I
        {
            get
            {
                if (s_Instance == null)
                    Create();
                return s_Instance;
            }
        }

        /// <summary>
        /// Returns if the singleton has been created.
        /// </summary>
        static public bool Exists
        {
            get { return s_Instance != null; }
        }

        /// <summary>
        /// Returns if this is the singleton instance.
        /// Good for checking if we don't need to continue
        /// our Awake routine.
        /// </summary>
        protected bool IsInstance()
        {
            return s_Instance == this;
        }

        protected virtual void Awake()
        {
            if (s_Instance == null)
            {
                s_Instance = (SType)this;
                transform.SetParent(GetRoot());
            }
            else if (!ReferenceEquals(s_Instance, this))
            {
                Destroy(this);
            }
        }

        protected virtual void OnDestroy()
        {
            if (s_Instance == this)
                s_Instance = null;
        }
    }
}
