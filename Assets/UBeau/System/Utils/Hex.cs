﻿using System;
using System.Globalization;

namespace UBeau
{
    /// <summary>
    /// Performs operations on hexadecimal values.
    /// </summary>
    static public class Hex
    {
        /// <summary>
        /// The characters used for base-16 (hex) representation of numbers.
        /// </summary>
        public const string HEX_CHARACTERS = "0123456789ABCDEF";

        /// <summary>
        /// Converts a hex character to its byte representation.
        /// </summary>
        static public byte FromChar(char inValue)
        {
            Assert.True(HEX_CHARACTERS.IndexOf(Char.ToUpper(inValue)) >= 0, "Given character is a hex character.",
                "'{0}' is not a hex character.", inValue);
            return (byte)HEX_CHARACTERS.IndexOf(Char.ToUpper(inValue));
        }

        /// <summary>
        /// Converts a hex string to its integer representation.
        /// </summary>
        static public uint FromString(string inHexString)
        {
            return UInt32.Parse(inHexString, NumberStyles.HexNumber);
        }

        /// <summary>
        /// Converts an integer to its hexadecimal representation.
        /// </summary>
        static public string ToString(uint inValue)
        {
            return inValue.ToString("X").PadLeft(2, '0');
        }

        /// <summary>
        /// Converts a byte to its hexadecimal representation.
        /// </summary>
        static public string ToString(byte inValue)
        {
            return inValue.ToString("X").PadLeft(2, '0');
        }
    }
}
