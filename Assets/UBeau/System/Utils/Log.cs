﻿using System.Diagnostics;
using System.Text;

namespace UBeau
{
    /// <summary>
    /// Logging shortcuts.
    /// </summary>
    static public class Log
    {
        // used to format strings without instantiating a new StringBuilder each time
        static private StringBuilder s_Formatter = new StringBuilder();

        /// <summary>
        /// Logs a message.
        /// Only in development builds.
        /// </summary>
        [Conditional("DEVELOPMENT")]
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void Debug(string inMessage)
        {
            UnityEngine.Debug.Log(inMessage);
        }

        /// <summary>
        /// Logs a message.
        /// Only in development builds.
        /// </summary>
        [Conditional("DEVELOPMENT")]
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void Debug(string inMessage, params object[] inMessageParams)
        {
            s_Formatter.Length = 0;
            s_Formatter.AppendFormat(inMessage, inMessageParams);
            UnityEngine.Debug.Log(s_Formatter.ToString());
            s_Formatter.Length = 0;
        }

        /// <summary>
        /// Logs a message.
        /// Will appear in non-development builds.
        /// </summary>
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void Info(string inMessage)
        {
            UnityEngine.Debug.Log(inMessage);
        }

        /// <summary>
        /// Logs a message.
        /// Will appear in non-development builds.
        /// </summary>
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void Info(string inMessage, params object[] inMessageParams)
        {
            s_Formatter.Length = 0;
            s_Formatter.AppendFormat(inMessage, inMessageParams);
            UnityEngine.Debug.Log(s_Formatter.ToString());
            s_Formatter.Length = 0;
        }

        /// <summary>
        /// Logs a warning.
        /// Will appear in non-development builds.
        /// </summary>
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void Warn(string inMessage)
        {
            UnityEngine.Debug.LogWarning(inMessage);
        }

        /// <summary>
        /// Logs a warning.
        /// Will appear in non-development builds.
        /// </summary>
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void Warn(string inMessage, params object[] inMessageParams)
        {
            s_Formatter.Length = 0;
            s_Formatter.AppendFormat(inMessage, inMessageParams);
            UnityEngine.Debug.LogWarning(s_Formatter.ToString());
            s_Formatter.Length = 0;
        }

        /// <summary>
        /// Logs an error.
        /// Will appear in non-development builds.
        /// </summary>
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void Error(string inMessage)
        {
            UnityEngine.Debug.LogError(inMessage);
        }

        /// <summary>
        /// Logs an error.
        /// Will appear in non-development builds.
        /// </summary>
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void Error(string inMessage, params object[] inMessageParams)
        {
            s_Formatter.Length = 0;
            s_Formatter.AppendFormat(inMessage, inMessageParams);
            UnityEngine.Debug.LogError(s_Formatter.ToString());
            s_Formatter.Length = 0;
        }
    }
}
