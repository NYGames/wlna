﻿using System.Collections.Generic;

namespace UBeau
{
    /// <summary>
    /// Performs bitwise operations on unsigned integers.
    /// </summary>
    static public class Bits
    {
        /// <summary>
        /// Total number of bits in an unsigned integer.
        /// </summary>
        public const int LENGTH = 32;

        /// <summary>
        /// Returns if the given uint has a bit toggled on.
        /// </summary>
        static public bool Contains(uint inBitArray, byte inBitIndex)
        {
            Assert.True(inBitIndex < LENGTH, "Byte index {0} is outside range.", inBitIndex);
            return (inBitArray & (1U << inBitIndex)) > 0;
        }

        /// <summary>
        /// Returns if the given int has a bit toggled on.
        /// </summary>
        static public bool Contains(int inBitArray, byte inBitIndex)
        {
            Assert.True(inBitIndex < LENGTH, "Byte index {0} is outside range.", inBitIndex);
            return (inBitArray & (1 << inBitIndex)) != 0;
        }

        /// <summary>
        /// Returns if the given uint contains a mask.
        /// </summary>
        static public bool ContainsMask(uint inBitArray, uint inBitMask)
        {
            return (inBitArray & inBitMask) > 0;
        }

        /// <summary>
        /// Returns if the given int contains a mask.
        /// </summary>
        static public bool ContainsMask(int inBitArray, int inBitMask)
        {
            return (inBitArray & inBitMask) != 0;
        }

        #region Modifications

        #region Add

        /// <summary>
        /// Toggles the bit in the given uint to on.
        /// </summary>
        static public void Add(ref uint ioBitArray, byte inBitIndex)
        {
            Assert.True(inBitIndex < LENGTH, "Byte index {0} is outside range.", inBitIndex);
            ioBitArray |= (1U << inBitIndex);
        }

        /// <summary>
        /// Toggles the bit in the given int to on.
        /// </summary>
        static public void Add(ref int ioBitArray, byte inBitIndex)
        {
            Assert.True(inBitIndex < LENGTH, "Byte index {0} is outside range.", inBitIndex);
            ioBitArray |= (1 << inBitIndex);
        }

        /// <summary>
        /// Toggles the bit in the given uint to on.
        /// </summary>
        static public uint Add(uint inBitArray, byte inBitIndex)
        {
            Assert.True(inBitIndex < LENGTH, "Byte index {0} is outside range.", inBitIndex);
            return inBitArray | (1U << inBitIndex);
        }

        /// <summary>
        /// Toggles the bit in the given int to on.
        /// </summary>
        static public int Add(int inBitArray, byte inBitIndex)
        {
            Assert.True(inBitIndex < LENGTH, "Byte index {0} is outside range.", inBitIndex);
            return inBitArray | (1 << inBitIndex);
        }

        #endregion

        #region Remove

        /// <summary>
        /// Toggles the bit in the given uint to off.
        /// </summary>
        static public void Remove(ref uint ioBitArray, byte inBitIndex)
        {
            Assert.True(inBitIndex < LENGTH, "Byte index {0} is outside range.", inBitIndex);
            ioBitArray &= ~(1U << inBitIndex);
        }

        /// <summary>
        /// Toggles the bit in the given int to off.
        /// </summary>
        static public void Remove(ref int ioBitArray, byte inBitIndex)
        {
            Assert.True(inBitIndex < LENGTH, "Byte index {0} is outside range.", inBitIndex);
            ioBitArray &= ~(1 << inBitIndex);
        }

        /// <summary>
        /// Toggles the bit in the given uint to off.
        /// </summary>
        static public uint Remove(uint inBitArray, byte inBitIndex)
        {
            Assert.True(inBitIndex < LENGTH, "Byte index {0} is outside range.", inBitIndex);
            return inBitArray & ~(1U << inBitIndex);
        }

        /// <summary>
        /// Toggles the bit in the given int to off.
        /// </summary>
        static public int Remove(int inBitArray, byte inBitIndex)
        {
            Assert.True(inBitIndex < LENGTH, "Byte index {0} is outside range.", inBitIndex);
            return inBitArray & ~(1 << inBitIndex);
        }

        #endregion

        #region Set

        /// <summary>
        /// Toggles the bit in the given uint to the given state.
        /// </summary>
        static public void Set(ref uint ioBitArray, byte inBitIndex, bool inbState)
        {
            if (inbState)
                Add(ref ioBitArray, inBitIndex);
            else
                Remove(ref ioBitArray, inBitIndex);
        }

        /// <summary>
        /// Toggles the bit in the given int to the given state.
        /// </summary>
        static public void Set(ref int ioBitArray, byte inBitIndex, bool inbState)
        {
            if (inbState)
                Add(ref ioBitArray, inBitIndex);
            else
                Remove(ref ioBitArray, inBitIndex);
        }

        /// <summary>
        /// Toggles the bit in the given uint to the given state.
        /// </summary>
        static public uint Set(uint inBitArray, byte inBitIndex, bool inbState)
        {
            if (inbState)
                return Add(inBitArray, inBitIndex);
            else
                return Remove(inBitArray, inBitIndex);
        }

        /// <summary>
        /// Toggles the bit in the given int to the given state.
        /// </summary>
        static public int Set(int inBitArray, byte inBitIndex, bool inbState)
        {
            if (inbState)
                return Add(inBitArray, inBitIndex);
            else
                return Remove(inBitArray, inBitIndex);
        }

        #endregion

        #endregion

        #region Queries

        /// <summary>
        /// Adds all the set bits in the given uint to a collection.
        /// </summary>
        static public void All(uint inBitArray, List<byte> ioByteCollection)
        {
            Assert.NotNull(ioByteCollection, "Null collection provided to Bits.All");

            for (byte bit = 0; bit < LENGTH; ++bit)
            {
                if ((inBitArray & (1U << bit)) > 0)
                    ioByteCollection.Add(bit);
            }
        }

        /// <summary>
        /// Adds all the set bits in the given int to a collection.
        /// </summary>
        static public void All(int inBitArray, List<byte> ioByteCollection)
        {
            Assert.NotNull(ioByteCollection, "Null collection provided to Bits.All");

            for (byte bit = 0; bit < LENGTH; ++bit)
            {
                if ((inBitArray & (1 << bit)) > 0)
                    ioByteCollection.Add(bit);
            }
        }

        /// <summary>
        /// Returns the index of the first set bit in the given uint.
        /// </summary>
        static public int First(uint inBitArray)
        {
            if (inBitArray == 0)
                return -1;

            for (byte bit = 0; bit < LENGTH; ++bit)
            {
                if ((inBitArray & (1U << bit)) > 0)
                    return bit;
            }

            return -1;
        }

        /// <summary>
        /// Returns the index of the first set bit in the given int.
        /// </summary>
        static public int First(int inBitArray)
        {
            if (inBitArray == 0)
                return -1;

            for (byte bit = 0; bit < LENGTH; ++bit)
            {
                if ((inBitArray & (1 << bit)) > 0)
                    return bit;
            }

            return -1;
        }

        /// <summary>
        /// Returns the index of the given bit array value.
        /// </summary>
        static public int IndexOf(uint inValue)
        {
            int index = -1;
            if ((inValue & (inValue - 1)) == 0)
            {
                while (inValue > 0)
                {
                    inValue >>= 1;
                    ++index;
                }
            }
            return index;
        }

        /// <summary>
        /// Returns the index of the given bit array value.
        /// </summary>
        static public int IndexOf(int inValue)
        {
            int index = -1;
            if ((inValue & (inValue - 1)) == 0)
            {
                while (inValue != 0)
                {
                    inValue >>= 1;
                    ++index;
                }
            }
            return index;
        }

        /// <summary>
        /// Returns if the given uint is a power of two.
        /// </summary>
        static public bool IsPowerOfTwo(uint inValue)
        {
            return (inValue & (inValue - 1)) == 0;
        }

        /// <summary>
        /// Returns if the given uint is a power of two.
        /// </summary>
        static public bool IsPowerOfTwo(int inValue)
        {
            return (inValue & (inValue - 1)) == 0;
        }

        #endregion

        /// <summary>
        /// Writes an unsigned integer into the given bitfield.
        /// </summary>
        static public void WriteUInt(ref uint ioBitArray, byte inLength, uint inData)
        {
            ioBitArray <<= inLength;
            uint trimmedData = (inData & ((1U << inLength) - 1));
            ioBitArray |= trimmedData;
        }

        /// <summary>
        /// Writes a boolean into the given bitfield.
        /// </summary>
        static public void WriteBool(ref uint ioBitArray, bool inbData)
        {
            ioBitArray <<= 1;
            ioBitArray |= (uint)(inbData ? 1 : 0);
        }

        /// <summary>
        /// Reads an unsigned integer from the given bitfield.
        /// </summary>
        static public uint ReadUInt(ref uint ioBitArray, byte inLength)
        {
            uint value = (ioBitArray & ((1U << inLength) - 1));
            ioBitArray >>= inLength;
            return value;
        }

        /// <summary>
        /// Reads a bool from the given bitfield.
        /// </summary>
        static public bool ReadBool(ref uint ioBitArray)
        {
            return ReadUInt(ref ioBitArray, 1) > 0;
        }

        /// <summary>
        /// Writes an integer into the given bitfield.
        /// </summary>
        static public void WriteInt(ref int ioBitArray, byte inLength, int inData)
        {
            ioBitArray <<= inLength;
            int trimmedData = (inData & ((1 << inLength) - 1));
            ioBitArray |= trimmedData;
        }

        /// <summary>
        /// Writes a boolean into the given bitfield.
        /// </summary>
        static public void WriteBool(ref int ioBitArray, bool inbData)
        {
            ioBitArray <<= 1;
            ioBitArray |= inbData ? 1 : 0;
        }

        /// <summary>
        /// Reads an integer from the given bitfield.
        /// </summary>
        static public int ReadInt(ref int ioBitArray, byte inLength)
        {
            int value = (ioBitArray & ((1 << inLength) - 1));
            ioBitArray >>= inLength;
            return value;
        }

        /// <summary>
        /// Reads a bool from the given bitfield.
        /// </summary>
        static public bool ReadBool(ref int ioBitArray)
        {
            return ReadInt(ref ioBitArray, 1) > 0;
        }
    }
}
