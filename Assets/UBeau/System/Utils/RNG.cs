﻿using System;
using System.Collections.Generic;
using UnityEngine;

using Random = System.Random;

namespace UBeau
{
    /// <summary>
    /// Global reference and extension methods
    /// for random number generation.
    /// </summary>
    static public class RNG
    {
        static private Random s_Random = new Random();

        /// <summary>
        /// Current generator.
        /// </summary>
        static public Random Instance
        {
            get { return s_Random; }
            set
            {
                Assert.NotNull(value);
                s_Random = value;
            }
        }

        #region Extensions

        /// <summary>
        /// Returns a random int.
        /// </summary>
        static public int Int(this Random inRandom)
        {
            return inRandom.Next();
        }

        /// <summary>
        /// Returns a random integer from [0, inMax)
        /// </summary>
        static public int Int(this Random inRandom, int inMax)
        {
            return inRandom.Next(inMax);
        }

        /// <summary>
        /// Returns a random integer from [inMin, inMax)
        /// </summary>
        static public int Int(this Random inRandom, int inMin, int inMax)
        {
            return inRandom.Next(inMin, inMax);
        }

        /// <summary>
        /// Returns a random float from [0, 1)
        /// </summary>
        static public float Float(this Random inRandom)
        {
            return (float)inRandom.NextDouble();
        }

        /// <summary>
        /// Returns a random float from [0, inMax)
        /// </summary>
        static public float Float(this Random inRandom, float inMax)
        {
            return (float)inRandom.NextDouble() * inMax;
        }

        /// <summary>
        /// Returns a random float from [inMin, inMax)
        /// </summary>
        static public float Float(this Random inRandom, float inMin, float inMax)
        {
            return inMin + (float)inRandom.NextDouble() * (inMax - inMin);
        }

        /// <summary>
        /// Returns a random boolean.
        /// </summary>
        static public bool Bool(this Random inRandom)
        {
            return inRandom.NextDouble() < 0.5;
        }

        /// <summary>
        /// Returns a boolean with a chance of being true.
        /// </summary>
        static public bool Chance(this Random inRandom, float inPercent)
        {
            return inRandom.NextDouble() < inPercent;
        }

        /// <summary>
        /// Returns a random radian angle.
        /// </summary>
        static public float Radians(this Random inRandom)
        {
            return (float)(inRandom.NextDouble() * Math.PI * 2);
        }

        /// <summary>
        /// Returns a random degrees angle.
        /// </summary>
        static public float Degrees(this Random inRandom)
        {
            return (float)(inRandom.NextDouble() * 360);
        }

        /// <summary>
        /// Returns a random normalized vector.
        /// </summary>
        static public Vector2 Vector(this Random inRandom)
        {
            double direction = inRandom.NextDouble() * Math.PI * 2;
            return new Vector2((float)Math.Cos(direction), (float)Math.Sin(direction));
        }

        /// <summary>
        /// Returns a random position within the given bounds.
        /// </summary>
        static public Vector2 Position(this Random inRandom, Vector2 inMin, Vector2 inMax)
        {
            return new Vector2(inMin.x + (float)inRandom.NextDouble() * (inMax.x - inMin.x),
                inMin.y + (float)inRandom.NextDouble() * (inMax.y - inMin.y));
        }

        /// <summary>
        /// Returns a random position.
        /// </summary>
        static public Vector2 Position(this Random inRandom, Rect inRegion)
        {
            return new Vector2(inRegion.x + (float)inRandom.NextDouble() * inRegion.width,
                inRegion.y + (float)inRandom.NextDouble() * inRegion.height);
        }

        #endregion

        #region Collections

        /// <summary>
        /// Returns a random element from the given parameters.
        /// </summary>
        static public T Choose<T>(this Random inRandom, T inFirst, T inSecond, params T[] inMore)
        {
            int index = inRandom.Next(inMore.Length + 2);
            if (index == 0)
                return inFirst;
            if (index == 1)
                return inSecond;
            return inMore[index - 2];
        }

        /// <summary>
        /// Returns a random element from the given list.
        /// </summary>
        static public T Choose<T>(this Random inRandom, IList<T> inChoices)
        {
            Assert.True(inChoices.Count > 0, "Cannot provide empty list for choosing.");
            return inChoices[inRandom.Next(inChoices.Count)];
        }

        /// <summary>
        /// Shuffles the elements of the given list.
        /// </summary>
        static public void Shuffle<T>(this Random inRandom, IList<T> inList)
        {
            int i = inList.Count;
            int j;
            while(--i > 0)
            {
                T old = inList[i];
                inList[i] = inList[j = inRandom.Next(i + 1)];
                inList[j] = old;
            }
        }

        #endregion
    }
}
