﻿// MIT License - Copyright (C) The Mono.Xna Team
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.

using System;
using UnityEngine;

namespace UBeau
{
    /// <summary>
    /// Contains helper methods and pre-defined values for Colors.
    /// </summary>
    static public class Colors
    {
        /// <summary>
        /// Returns a version of the color with the given alpha.
        /// </summary>
        static public Color ToAlpha(this Color inColor, float inAlpha)
        {
            inColor.a = inAlpha;
            return inColor;
        }

        /// <summary>
        /// Returns an inverted version of the color.
        /// </summary>
        static public Color Invert(this Color inColor)
        {
            return new Color(1 - inColor.r, 1 - inColor.g, 1 - inColor.b, inColor.a);
        }

        /// <summary>
        /// Returns a color with the given RGBA values.
        /// </summary>
        static public Color RGB(byte inRed, byte inGreen, byte inBlue, byte inAlpha = 255)
        {
            return new Color(inRed / 255.0f, inGreen / 255.0f, inBlue / 255.0f, inAlpha / 255.0f);
        }

        /// <summary>
        /// Returns a grayscale color.
        /// </summary>
        static public Color Grayscale(float inPercent)
        {
            return new Color(inPercent, inPercent, inPercent, 1);
        }

        /// <summary>
        /// Returns the unsigned integer representation of the color.
        /// </summary>
        static public uint ToUInt(this Color inColor)
        {
            uint r = (byte)(inColor.r * 255);
            uint g = (byte)(inColor.g * 255);
            uint b = (byte)(inColor.b * 255);
            uint a = (byte)(inColor.a * 255);

            return (r << 24) + (g << 16) + (b << 8) + a;
        }

        /// <summary>
        /// Converts from a string to a color.
        /// </summary>
        static public Color FromString(string inString)
        {
            if (string.IsNullOrEmpty(inString))
                throw new ArgumentNullException();

            switch (inString.ToLower())
            {
                case "white":
                    return White;
                case "silver":
                    return Silver;
                case "gray":
                    return Gray;
                case "black":
                    return Black;
                case "red":
                    return Red;
                case "maroon":
                    return Maroon;
                case "yellow":
                    return Yellow;
                case "olive":
                    return Olive;
                case "lime":
                    return Lime;
                case "green":
                    return Green;
                case "aqua":
                    return Aqua;
                case "teal":
                    return Teal;
                case "blue":
                    return Blue;
                case "navy":
                    return Navy;
                case "fuchsia":
                    return Fuchsia;
                case "purple":
                    return Purple;
                default:
                    return Hex(inString);
            }
        }

        /// <summary>
        /// Translates from a hex string to a color.
        /// Valid formats are #RGB(A) and #RRGGBB(AA)
        /// </summary>
        static public Color Hex(string inHexString)
        {
            string hex = inHexString;
            if (string.IsNullOrEmpty(hex))
                throw new ArgumentNullException();

            if (hex[0] == '#')
                hex = hex.Substring(1);
            else if (hex[0] == '0' && hex.Length > 1 && (hex[1] == 'x' || hex[1] == 'X'))
                hex = hex.Substring(2);

            byte r, g, b, a;
            if (hex.Length == 3 || hex.Length == 4)
            {
                r = (byte)(UBeau.Hex.FromChar(hex[0]) * 16 + UBeau.Hex.FromChar(hex[0]));
                g = (byte)(UBeau.Hex.FromChar(hex[1]) * 16 + UBeau.Hex.FromChar(hex[1]));
                b = (byte)(UBeau.Hex.FromChar(hex[2]) * 16 + UBeau.Hex.FromChar(hex[2]));
                a = hex.Length == 4 ? (byte)(UBeau.Hex.FromChar(hex[3]) * 16 + UBeau.Hex.FromChar(hex[3])) : (byte)255;
            }
            else if (hex.Length == 6 || hex.Length == 8)
            {
                r = (byte)(UBeau.Hex.FromChar(hex[0]) * 16 + UBeau.Hex.FromChar(hex[1]));
                g = (byte)(UBeau.Hex.FromChar(hex[2]) * 16 + UBeau.Hex.FromChar(hex[3]));
                b = (byte)(UBeau.Hex.FromChar(hex[4]) * 16 + UBeau.Hex.FromChar(hex[5]));
                a = hex.Length == 8 ? (byte)(UBeau.Hex.FromChar(hex[6]) * 16 + UBeau.Hex.FromChar(hex[7])) : (byte)255;
            }
            else
                throw new ArgumentException();

            return RGB(r, g, b, a);
        }
        
        /// <summary>
        /// Translates the color to a hex value, starting with "#".
        /// </summary>
        static public string ToHex(this Color inColor)
        {
            uint value = ToUInt(inColor);
            if (inColor.a >= 1)
                return "#" + UBeau.Hex.ToString(value >> 8).PadLeft(6, '0');
            return "#" + UBeau.Hex.ToString(value).PadLeft(8, '0');
        }

        #region XNA Colors

        static Colors()
        {
            TransparentBlack = Color.clear;
            Transparent = Color.clear;
            AliceBlue = RGB(240, 248, 255);
            AntiqueWhite = RGB(250, 235, 215);
            Aqua = RGB(0, 255, 255);
            Aquamarine = RGB(127, 255, 212);
            Azure = RGB(240, 255, 255);
            Beige = RGB(245, 245, 220);
            Bisque = RGB(255, 228, 196);
            Black = RGB(0, 0, 0);
            BlanchedAlmond = RGB(255, 235, 205);
            Blue = RGB(0, 0, 255);
            BlueViolet = RGB(138, 43, 226);
            Brown = RGB(165, 42, 42);
            BurlyWood = RGB(222, 184, 135);
            CadetBlue = RGB(95, 158, 160);
            Chartreuse = RGB(127, 255, 0);
            Chocolate = RGB(210, 105, 30);
            Coral = RGB(255, 127, 80);
            CornflowerBlue = RGB(100, 149, 237);
            Cornsilk = RGB(255, 248, 220);
            Crimson = RGB(220, 20, 60);
            Cyan = RGB(0, 255, 255);
            DarkBlue = RGB(0, 0, 139);
            DarkCyan = RGB(0, 139, 139);
            DarkGoldenrod = RGB(184, 134, 11);
            DarkGray = RGB(169, 169, 169);
            DarkGreen = RGB(0, 100, 0);
            DarkKhaki = RGB(189, 183, 107);
            DarkMagenta = RGB(139, 0, 139);
            DarkOliveGreen = RGB(85, 107, 47);
            DarkOrange = RGB(255, 140, 0);
            DarkOrchid = RGB(153, 50, 204);
            DarkRed = RGB(139, 0, 0);
            DarkSalmon = RGB(233, 150, 122);
            DarkSeaGreen = RGB(143, 188, 139);
            DarkSlateBlue = RGB(72, 61, 139);
            DarkSlateGray = RGB(47, 79, 79);
            DarkTurquoise = RGB(0, 206, 209);
            DarkViolet = RGB(148, 0, 211);
            DeepPink = RGB(255, 20, 147);
            DeepSkyBlue = RGB(0, 191, 255);
            DimGray = RGB(105, 105, 105);
            DodgerBlue = RGB(30, 144, 255);
            Firebrick = RGB(178, 34, 34);
            FloralWhite = RGB(255, 250, 240);
            ForestGreen = RGB(34, 139, 34);
            Fuchsia = RGB(255, 0, 255);
            Gainsboro = RGB(220, 220, 220);
            GhostWhite = RGB(248, 248, 255);
            Gold = RGB(255, 215, 0);
            Goldenrod = RGB(218, 165, 32);
            Gray = RGB(128, 128, 128);
            Green = RGB(0, 128, 0);
            GreenYellow = RGB(173, 255, 47);
            Honeydew = RGB(240, 255, 240);
            HotPink = RGB(255, 105, 180);
            IndianRed = RGB(205, 92, 92);
            Indigo = RGB(75, 0, 130);
            Ivory = RGB(255, 255, 240);
            Khaki = RGB(240, 230, 140);
            Lavender = RGB(230, 230, 250);
            LavenderBlush = RGB(255, 240, 245);
            LawnGreen = RGB(124, 252, 0);
            LemonChiffon = RGB(255, 250, 205);
            LightBlue = RGB(173, 216, 230);
            LightCoral = RGB(240, 128, 128);
            LightCyan = RGB(224, 255, 255);
            LightGoldenrodYellow = RGB(250, 250, 210);
            LightGray = RGB(211, 211, 211);
            LightGreen = RGB(144, 238, 144);
            LightPink = RGB(255, 182, 193);
            LightSalmon = RGB(255, 160, 122);
            LightSeaGreen = RGB(32, 178, 170);
            LightSkyBlue = RGB(135, 206, 250);
            LightSlateGray = RGB(119, 136, 153);
            LightSteelBlue = RGB(176, 196, 222);
            LightYellow = RGB(255, 255, 224);
            Lime = RGB(0, 255, 0);
            LimeGreen = RGB(50, 205, 50);
            Linen = RGB(250, 240, 230);
            Magenta = RGB(255, 0, 255);
            Maroon = RGB(128, 0, 0);
            MediumAquamarine = RGB(102, 205, 170);
            MediumBlue = RGB(0, 0, 205);
            MediumOrchid = RGB(186, 85, 211);
            MediumPurple = RGB(147, 112, 219);
            MediumSeaGreen = RGB(60, 179, 113);
            MediumSlateBlue = RGB(123, 104, 238);
            MediumSpringGreen = RGB(0, 250, 154);
            MediumTurquoise = RGB(72, 209, 204);
            MediumVioletRed = RGB(199, 21, 133);
            MidnightBlue = RGB(25, 25, 112);
            MintCream = RGB(245, 255, 250);
            MistyRose = RGB(255, 228, 225);
            Moccasin = RGB(255, 228, 181);
            NavajoWhite = RGB(255, 222, 17);
            Navy = RGB(0, 0, 128);
            OldLace = RGB(253, 245, 230);
            Olive = RGB(128, 128, 0);
            OliveDrab = RGB(107, 142, 35);
            Orange = RGB(255, 165, 0);
            OrangeRed = RGB(255, 69, 0);
            Orchid = RGB(218, 112, 214);
            PaleGoldenrod = RGB(238, 232, 170);
            PaleGreen = RGB(152, 251, 152);
            PaleTurquoise = RGB(175, 238, 238);
            PaleVioletRed = RGB(219, 112, 147);
            PapayaWhip = RGB(255, 239, 213);
            PeachPuff = RGB(255, 218, 185);
            Peru = RGB(205, 133, 63);
            Pink = RGB(255, 192, 203);
            Plum = RGB(221, 160, 221);
            PowderBlue = RGB(176, 224, 230);
            Purple = RGB(128, 0, 128);
            Red = RGB(255, 0, 0);
            RosyBrown = RGB(188, 143, 143);
            RoyalBlue = RGB(65, 105, 225);
            SaddleBrown = RGB(139, 69, 19);
            Salmon = RGB(250, 128, 114);
            SandyBrown = RGB(244, 164, 96);
            SeaGreen = RGB(46, 139, 87);
            SeaShell = RGB(255, 245, 238);
            Sienna = RGB(160, 82, 45);
            Silver = RGB(192, 192, 192);
            SkyBlue = RGB(135, 206, 235);
            SlateBlue = RGB(106, 90, 205);
            SlateGray = RGB(112, 128, 144);
            Snow = RGB(255, 250, 250);
            SpringGreen = RGB(0, 255, 127);
            SteelBlue = RGB(70, 130, 180);
            Tan = RGB(210, 180, 140);
            Teal = RGB(0, 128, 128);
            Thistle = RGB(216, 191, 216);
            Tomato = RGB(255, 99, 71);
            Turquoise = RGB(64, 224, 208);
            Violet = RGB(238, 130, 238);
            Wheat = RGB(245, 222, 179);
            White = RGB(255, 255, 255);
            WhiteSmoke = RGB(245, 245, 245);
            Yellow = RGB(255, 255, 0);
            YellowGreen = RGB(154, 205, 50);
        }

        /// <summary>
        /// TransparentBlack color (R:0,G:0,B:0,A:0).
        /// </summary>
        static public Color TransparentBlack
        {
            get;
            private set;
        }
        /// <summary>
        /// Transparent color (R:0,G:0,B:0,A:0).
        /// </summary>
        static public Color Transparent
        {
            get;
            private set;
        }
        /// <summary>
        /// AliceBlue color (R:240,G:248,B:255,A:255).
        /// </summary>
        static public Color AliceBlue
        {
            get;
            private set;
        }
        /// <summary>
        /// AntiqueWhite color (R:250,G:235,B:215,A:255).
        /// </summary>
        static public Color AntiqueWhite
        {
            get;
            private set;
        }
        /// <summary>
        /// Aqua color (R:0,G:255,B:255,A:255).
        /// </summary>
        static public Color Aqua
        {
            get;
            private set;
        }
        /// <summary>
        /// Aquamarine color (R:127,G:255,B:212,A:255).
        /// </summary>
        static public Color Aquamarine
        {
            get;
            private set;
        }
        /// <summary>
        /// Azure color (R:240,G:255,B:255,A:255).
        /// </summary>
        static public Color Azure
        {
            get;
            private set;
        }
        /// <summary>
        /// Beige color (R:245,G:245,B:220,A:255).
        /// </summary>
        static public Color Beige
        {
            get;
            private set;
        }
        /// <summary>
        /// Bisque color (R:255,G:228,B:196,A:255).
        /// </summary>
        static public Color Bisque
        {
            get;
            private set;
        }
        /// <summary>
        /// Black color (R:0,G:0,B:0,A:255).
        /// </summary>
        static public Color Black
        {
            get;
            private set;
        }
        /// <summary>
        /// BlanchedAlmond color (R:255,G:235,B:205,A:255).
        /// </summary>
        static public Color BlanchedAlmond
        {
            get;
            private set;
        }
        /// <summary>
        /// Blue color (R:0,G:0,B:255,A:255).
        /// </summary>
        static public Color Blue
        {
            get;
            private set;
        }
        /// <summary>
        /// BlueViolet color (R:138,G:43,B:226,A:255).
        /// </summary>
        static public Color BlueViolet
        {
            get;
            private set;
        }
        /// <summary>
        /// Brown color (R:165,G:42,B:42,A:255).
        /// </summary>
        static public Color Brown
        {
            get;
            private set;
        }
        /// <summary>
        /// BurlyWood color (R:222,G:184,B:135,A:255).
        /// </summary>
        static public Color BurlyWood
        {
            get;
            private set;
        }
        /// <summary>
        /// CadetBlue color (R:95,G:158,B:160,A:255).
        /// </summary>
        static public Color CadetBlue
        {
            get;
            private set;
        }
        /// <summary>
        /// Chartreuse color (R:127,G:255,B:0,A:255).
        /// </summary>
        static public Color Chartreuse
        {
            get;
            private set;
        }
        /// <summary>
        /// Chocolate color (R:210,G:105,B:30,A:255).
        /// </summary>
        static public Color Chocolate
        {
            get;
            private set;
        }
        /// <summary>
        /// Coral color (R:255,G:127,B:80,A:255).
        /// </summary>
        static public Color Coral
        {
            get;
            private set;
        }
        /// <summary>
        /// CornflowerBlue color (R:100,G:149,B:237,A:255).
        /// </summary>
        static public Color CornflowerBlue
        {
            get;
            private set;
        }
        /// <summary>
        /// Cornsilk color (R:255,G:248,B:220,A:255).
        /// </summary>
        static public Color Cornsilk
        {
            get;
            private set;
        }
        /// <summary>
        /// Crimson color (R:220,G:20,B:60,A:255).
        /// </summary>
        static public Color Crimson
        {
            get;
            private set;
        }
        /// <summary>
        /// Cyan color (R:0,G:255,B:255,A:255).
        /// </summary>
        static public Color Cyan
        {
            get;
            private set;
        }
        /// <summary>
        /// DarkBlue color (R:0,G:0,B:139,A:255).
        /// </summary>
        static public Color DarkBlue
        {
            get;
            private set;
        }
        /// <summary>
        /// DarkCyan color (R:0,G:139,B:139,A:255).
        /// </summary>
        static public Color DarkCyan
        {
            get;
            private set;
        }
        /// <summary>
        /// DarkGoldenrod color (R:184,G:134,B:11,A:255).
        /// </summary>
        static public Color DarkGoldenrod
        {
            get;
            private set;
        }
        /// <summary>
        /// DarkGray color (R:169,G:169,B:169,A:255).
        /// </summary>
        static public Color DarkGray
        {
            get;
            private set;
        }
        /// <summary>
        /// DarkGreen color (R:0,G:100,B:0,A:255).
        /// </summary>
        static public Color DarkGreen
        {
            get;
            private set;
        }
        /// <summary>
        /// DarkKhaki color (R:189,G:183,B:107,A:255).
        /// </summary>
        static public Color DarkKhaki
        {
            get;
            private set;
        }
        /// <summary>
        /// DarkMagenta color (R:139,G:0,B:139,A:255).
        /// </summary>
        static public Color DarkMagenta
        {
            get;
            private set;
        }
        /// <summary>
        /// DarkOliveGreen color (R:85,G:107,B:47,A:255).
        /// </summary>
        static public Color DarkOliveGreen
        {
            get;
            private set;
        }
        /// <summary>
        /// DarkOrange color (R:255,G:140,B:0,A:255).
        /// </summary>
        static public Color DarkOrange
        {
            get;
            private set;
        }
        /// <summary>
        /// DarkOrchid color (R:153,G:50,B:204,A:255).
        /// </summary>
        static public Color DarkOrchid
        {
            get;
            private set;
        }
        /// <summary>
        /// DarkRed color (R:139,G:0,B:0,A:255).
        /// </summary>
        static public Color DarkRed
        {
            get;
            private set;
        }
        /// <summary>
        /// DarkSalmon color (R:233,G:150,B:122,A:255).
        /// </summary>
        static public Color DarkSalmon
        {
            get;
            private set;
        }
        /// <summary>
        /// DarkSeaGreen color (R:143,G:188,B:139,A:255).
        /// </summary>
        static public Color DarkSeaGreen
        {
            get;
            private set;
        }
        /// <summary>
        /// DarkSlateBlue color (R:72,G:61,B:139,A:255).
        /// </summary>
        static public Color DarkSlateBlue
        {
            get;
            private set;
        }
        /// <summary>
        /// DarkSlateGray color (R:47,G:79,B:79,A:255).
        /// </summary>
        static public Color DarkSlateGray
        {
            get;
            private set;
        }
        /// <summary>
        /// DarkTurquoise color (R:0,G:206,B:209,A:255).
        /// </summary>
        static public Color DarkTurquoise
        {
            get;
            private set;
        }
        /// <summary>
        /// DarkViolet color (R:148,G:0,B:211,A:255).
        /// </summary>
        static public Color DarkViolet
        {
            get;
            private set;
        }
        /// <summary>
        /// DeepPink color (R:255,G:20,B:147,A:255).
        /// </summary>
        static public Color DeepPink
        {
            get;
            private set;
        }
        /// <summary>
        /// DeepSkyBlue color (R:0,G:191,B:255,A:255).
        /// </summary>
        static public Color DeepSkyBlue
        {
            get;
            private set;
        }
        /// <summary>
        /// DimGray color (R:105,G:105,B:105,A:255).
        /// </summary>
        static public Color DimGray
        {
            get;
            private set;
        }
        /// <summary>
        /// DodgerBlue color (R:30,G:144,B:255,A:255).
        /// </summary>
        static public Color DodgerBlue
        {
            get;
            private set;
        }
        /// <summary>
        /// Firebrick color (R:178,G:34,B:34,A:255).
        /// </summary>
        static public Color Firebrick
        {
            get;
            private set;
        }
        /// <summary>
        /// FloralWhite color (R:255,G:250,B:240,A:255).
        /// </summary>
        static public Color FloralWhite
        {
            get;
            private set;
        }
        /// <summary>
        /// ForestGreen color (R:34,G:139,B:34,A:255).
        /// </summary>
        static public Color ForestGreen
        {
            get;
            private set;
        }
        /// <summary>
        /// Fuchsia color (R:255,G:0,B:255,A:255).
        /// </summary>
        static public Color Fuchsia
        {
            get;
            private set;
        }
        /// <summary>
        /// Gainsboro color (R:220,G:220,B:220,A:255).
        /// </summary>
        static public Color Gainsboro
        {
            get;
            private set;
        }
        /// <summary>
        /// GhostWhite color (R:248,G:248,B:255,A:255).
        /// </summary>
        static public Color GhostWhite
        {
            get;
            private set;
        }
        /// <summary>
        /// Gold color (R:255,G:215,B:0,A:255).
        /// </summary>
        static public Color Gold
        {
            get;
            private set;
        }
        /// <summary>
        /// Goldenrod color (R:218,G:165,B:32,A:255).
        /// </summary>
        static public Color Goldenrod
        {
            get;
            private set;
        }
        /// <summary>
        /// Gray color (R:128,G:128,B:128,A:255).
        /// </summary>
        static public Color Gray
        {
            get;
            private set;
        }
        /// <summary>
        /// Green color (R:0,G:128,B:0,A:255).
        /// </summary>
        static public Color Green
        {
            get;
            private set;
        }
        /// <summary>
        /// GreenYellow color (R:173,G:255,B:47,A:255).
        /// </summary>
        static public Color GreenYellow
        {
            get;
            private set;
        }
        /// <summary>
        /// Honeydew color (R:240,G:255,B:240,A:255).
        /// </summary>
        static public Color Honeydew
        {
            get;
            private set;
        }
        /// <summary>
        /// HotPink color (R:255,G:105,B:180,A:255).
        /// </summary>
        static public Color HotPink
        {
            get;
            private set;
        }
        /// <summary>
        /// IndianRed color (R:205,G:92,B:92,A:255).
        /// </summary>
        static public Color IndianRed
        {
            get;
            private set;
        }
        /// <summary>
        /// Indigo color (R:75,G:0,B:130,A:255).
        /// </summary>
        static public Color Indigo
        {
            get;
            private set;
        }
        /// <summary>
        /// Ivory color (R:255,G:255,B:240,A:255).
        /// </summary>
        static public Color Ivory
        {
            get;
            private set;
        }
        /// <summary>
        /// Khaki color (R:240,G:230,B:140,A:255).
        /// </summary>
        static public Color Khaki
        {
            get;
            private set;
        }
        /// <summary>
        /// Lavender color (R:230,G:230,B:250,A:255).
        /// </summary>
        static public Color Lavender
        {
            get;
            private set;
        }
        /// <summary>
        /// LavenderBlush color (R:255,G:240,B:245,A:255).
        /// </summary>
        static public Color LavenderBlush
        {
            get;
            private set;
        }
        /// <summary>
        /// LawnGreen color (R:124,G:252,B:0,A:255).
        /// </summary>
        static public Color LawnGreen
        {
            get;
            private set;
        }
        /// <summary>
        /// LemonChiffon color (R:255,G:250,B:205,A:255).
        /// </summary>
        static public Color LemonChiffon
        {
            get;
            private set;
        }
        /// <summary>
        /// LightBlue color (R:173,G:216,B:230,A:255).
        /// </summary>
        static public Color LightBlue
        {
            get;
            private set;
        }
        /// <summary>
        /// LightCoral color (R:240,G:128,B:128,A:255).
        /// </summary>
        static public Color LightCoral
        {
            get;
            private set;
        }
        /// <summary>
        /// LightCyan color (R:224,G:255,B:255,A:255).
        /// </summary>
        static public Color LightCyan
        {
            get;
            private set;
        }
        /// <summary>
        /// LightGoldenrodYellow color (R:250,G:250,B:210,A:255).
        /// </summary>
        static public Color LightGoldenrodYellow
        {
            get;
            private set;
        }
        /// <summary>
        /// LightGray color (R:211,G:211,B:211,A:255).
        /// </summary>
        static public Color LightGray
        {
            get;
            private set;
        }
        /// <summary>
        /// LightGreen color (R:144,G:238,B:144,A:255).
        /// </summary>
        static public Color LightGreen
        {
            get;
            private set;
        }
        /// <summary>
        /// LightPink color (R:255,G:182,B:193,A:255).
        /// </summary>
        static public Color LightPink
        {
            get;
            private set;
        }
        /// <summary>
        /// LightSalmon color (R:255,G:160,B:122,A:255).
        /// </summary>
        static public Color LightSalmon
        {
            get;
            private set;
        }
        /// <summary>
        /// LightSeaGreen color (R:32,G:178,B:170,A:255).
        /// </summary>
        static public Color LightSeaGreen
        {
            get;
            private set;
        }
        /// <summary>
        /// LightSkyBlue color (R:135,G:206,B:250,A:255).
        /// </summary>
        static public Color LightSkyBlue
        {
            get;
            private set;
        }
        /// <summary>
        /// LightSlateGray color (R:119,G:136,B:153,A:255).
        /// </summary>
        static public Color LightSlateGray
        {
            get;
            private set;
        }
        /// <summary>
        /// LightSteelBlue color (R:176,G:196,B:222,A:255).
        /// </summary>
        static public Color LightSteelBlue
        {
            get;
            private set;
        }
        /// <summary>
        /// LightYellow color (R:255,G:255,B:224,A:255).
        /// </summary>
        static public Color LightYellow
        {
            get;
            private set;
        }
        /// <summary>
        /// Lime color (R:0,G:255,B:0,A:255).
        /// </summary>
        static public Color Lime
        {
            get;
            private set;
        }
        /// <summary>
        /// LimeGreen color (R:50,G:205,B:50,A:255).
        /// </summary>
        static public Color LimeGreen
        {
            get;
            private set;
        }
        /// <summary>
        /// Linen color (R:250,G:240,B:230,A:255).
        /// </summary>
        static public Color Linen
        {
            get;
            private set;
        }
        /// <summary>
        /// Magenta color (R:255,G:0,B:255,A:255).
        /// </summary>
        static public Color Magenta
        {
            get;
            private set;
        }
        /// <summary>
        /// Maroon color (R:128,G:0,B:0,A:255).
        /// </summary>
        static public Color Maroon
        {
            get;
            private set;
        }
        /// <summary>
        /// MediumAquamarine color (R:102,G:205,B:170,A:255).
        /// </summary>
        static public Color MediumAquamarine
        {
            get;
            private set;
        }
        /// <summary>
        /// MediumBlue color (R:0,G:0,B:205,A:255).
        /// </summary>
        static public Color MediumBlue
        {
            get;
            private set;
        }
        /// <summary>
        /// MediumOrchid color (R:186,G:85,B:211,A:255).
        /// </summary>
        static public Color MediumOrchid
        {
            get;
            private set;
        }
        /// <summary>
        /// MediumPurple color (R:147,G:112,B:219,A:255).
        /// </summary>
        static public Color MediumPurple
        {
            get;
            private set;
        }
        /// <summary>
        /// MediumSeaGreen color (R:60,G:179,B:113,A:255).
        /// </summary>
        static public Color MediumSeaGreen
        {
            get;
            private set;
        }
        /// <summary>
        /// MediumSlateBlue color (R:123,G:104,B:238,A:255).
        /// </summary>
        static public Color MediumSlateBlue
        {
            get;
            private set;
        }
        /// <summary>
        /// MediumSpringGreen color (R:0,G:250,B:154,A:255).
        /// </summary>
        static public Color MediumSpringGreen
        {
            get;
            private set;
        }
        /// <summary>
        /// MediumTurquoise color (R:72,G:209,B:204,A:255).
        /// </summary>
        static public Color MediumTurquoise
        {
            get;
            private set;
        }
        /// <summary>
        /// MediumVioletRed color (R:199,G:21,B:133,A:255).
        /// </summary>
        static public Color MediumVioletRed
        {
            get;
            private set;
        }
        /// <summary>
        /// MidnightBlue color (R:25,G:25,B:112,A:255).
        /// </summary>
        static public Color MidnightBlue
        {
            get;
            private set;
        }
        /// <summary>
        /// MintCream color (R:245,G:255,B:250,A:255).
        /// </summary>
        static public Color MintCream
        {
            get;
            private set;
        }
        /// <summary>
        /// MistyRose color (R:255,G:228,B:225,A:255).
        /// </summary>
        static public Color MistyRose
        {
            get;
            private set;
        }
        /// <summary>
        /// Moccasin color (R:255,G:228,B:181,A:255).
        /// </summary>
        static public Color Moccasin
        {
            get;
            private set;
        }
        /// <summary>
        /// NavajoWhite color (R:255,G:222,B:173,A:255).
        /// </summary>
        static public Color NavajoWhite
        {
            get;
            private set;
        }
        /// <summary>
        /// Navy color (R:0,G:0,B:128,A:255).
        /// </summary>
        static public Color Navy
        {
            get;
            private set;
        }
        /// <summary>
        /// OldLace color (R:253,G:245,B:230,A:255).
        /// </summary>
        static public Color OldLace
        {
            get;
            private set;
        }
        /// <summary>
        /// Olive color (R:128,G:128,B:0,A:255).
        /// </summary>
        static public Color Olive
        {
            get;
            private set;
        }
        /// <summary>
        /// OliveDrab color (R:107,G:142,B:35,A:255).
        /// </summary>
        static public Color OliveDrab
        {
            get;
            private set;
        }
        /// <summary>
        /// Orange color (R:255,G:165,B:0,A:255).
        /// </summary>
        static public Color Orange
        {
            get;
            private set;
        }
        /// <summary>
        /// OrangeRed color (R:255,G:69,B:0,A:255).
        /// </summary>
        static public Color OrangeRed
        {
            get;
            private set;
        }
        /// <summary>
        /// Orchid color (R:218,G:112,B:214,A:255).
        /// </summary>
        static public Color Orchid
        {
            get;
            private set;
        }
        /// <summary>
        /// PaleGoldenrod color (R:238,G:232,B:170,A:255).
        /// </summary>
        static public Color PaleGoldenrod
        {
            get;
            private set;
        }
        /// <summary>
        /// PaleGreen color (R:152,G:251,B:152,A:255).
        /// </summary>
        static public Color PaleGreen
        {
            get;
            private set;
        }
        /// <summary>
        /// PaleTurquoise color (R:175,G:238,B:238,A:255).
        /// </summary>
        static public Color PaleTurquoise
        {
            get;
            private set;
        }
        /// <summary>
        /// PaleVioletRed color (R:219,G:112,B:147,A:255).
        /// </summary>
        static public Color PaleVioletRed
        {
            get;
            private set;
        }
        /// <summary>
        /// PapayaWhip color (R:255,G:239,B:213,A:255).
        /// </summary>
        static public Color PapayaWhip
        {
            get;
            private set;
        }
        /// <summary>
        /// PeachPuff color (R:255,G:218,B:185,A:255).
        /// </summary>
        static public Color PeachPuff
        {
            get;
            private set;
        }
        /// <summary>
        /// Peru color (R:205,G:133,B:63,A:255).
        /// </summary>
        static public Color Peru
        {
            get;
            private set;
        }
        /// <summary>
        /// Pink color (R:255,G:192,B:203,A:255).
        /// </summary>
        static public Color Pink
        {
            get;
            private set;
        }
        /// <summary>
        /// Plum color (R:221,G:160,B:221,A:255).
        /// </summary>
        static public Color Plum
        {
            get;
            private set;
        }
        /// <summary>
        /// PowderBlue color (R:176,G:224,B:230,A:255).
        /// </summary>
        static public Color PowderBlue
        {
            get;
            private set;
        }
        /// <summary>
        /// Purple color (R:128,G:0,B:128,A:255).
        /// </summary>
        static public Color Purple
        {
            get;
            private set;
        }
        /// <summary>
        /// Red color (R:255,G:0,B:0,A:255).
        /// </summary>
        static public Color Red
        {
            get;
            private set;
        }
        /// <summary>
        /// RosyBrown color (R:188,G:143,B:143,A:255).
        /// </summary>
        static public Color RosyBrown
        {
            get;
            private set;
        }
        /// <summary>
        /// RoyalBlue color (R:65,G:105,B:225,A:255).
        /// </summary>
        static public Color RoyalBlue
        {
            get;
            private set;
        }
        /// <summary>
        /// SaddleBrown color (R:139,G:69,B:19,A:255).
        /// </summary>
        static public Color SaddleBrown
        {
            get;
            private set;
        }
        /// <summary>
        /// Salmon color (R:250,G:128,B:114,A:255).
        /// </summary>
        static public Color Salmon
        {
            get;
            private set;
        }
        /// <summary>
        /// SandyBrown color (R:244,G:164,B:96,A:255).
        /// </summary>
        static public Color SandyBrown
        {
            get;
            private set;
        }
        /// <summary>
        /// SeaGreen color (R:46,G:139,B:87,A:255).
        /// </summary>
        static public Color SeaGreen
        {
            get;
            private set;
        }
        /// <summary>
        /// SeaShell color (R:255,G:245,B:238,A:255).
        /// </summary>
        static public Color SeaShell
        {
            get;
            private set;
        }
        /// <summary>
        /// Sienna color (R:160,G:82,B:45,A:255).
        /// </summary>
        static public Color Sienna
        {
            get;
            private set;
        }
        /// <summary>
        /// Silver color (R:192,G:192,B:192,A:255).
        /// </summary>
        static public Color Silver
        {
            get;
            private set;
        }
        /// <summary>
        /// SkyBlue color (R:135,G:206,B:235,A:255).
        /// </summary>
        static public Color SkyBlue
        {
            get;
            private set;
        }
        /// <summary>
        /// SlateBlue color (R:106,G:90,B:205,A:255).
        /// </summary>
        static public Color SlateBlue
        {
            get;
            private set;
        }
        /// <summary>
        /// SlateGray color (R:112,G:128,B:144,A:255).
        /// </summary>
        static public Color SlateGray
        {
            get;
            private set;
        }
        /// <summary>
        /// Snow color (R:255,G:250,B:250,A:255).
        /// </summary>
        static public Color Snow
        {
            get;
            private set;
        }
        /// <summary>
        /// SpringGreen color (R:0,G:255,B:127,A:255).
        /// </summary>
        static public Color SpringGreen
        {
            get;
            private set;
        }
        /// <summary>
        /// SteelBlue color (R:70,G:130,B:180,A:255).
        /// </summary>
        static public Color SteelBlue
        {
            get;
            private set;
        }
        /// <summary>
        /// Tan color (R:210,G:180,B:140,A:255).
        /// </summary>
        static public Color Tan
        {
            get;
            private set;
        }
        /// <summary>
        /// Teal color (R:0,G:128,B:128,A:255).
        /// </summary>
        static public Color Teal
        {
            get;
            private set;
        }
        /// <summary>
        /// Thistle color (R:216,G:191,B:216,A:255).
        /// </summary>
        static public Color Thistle
        {
            get;
            private set;
        }
        /// <summary>
        /// Tomato color (R:255,G:99,B:71,A:255).
        /// </summary>
        static public Color Tomato
        {
            get;
            private set;
        }
        /// <summary>
        /// Turquoise color (R:64,G:224,B:208,A:255).
        /// </summary>
        static public Color Turquoise
        {
            get;
            private set;
        }
        /// <summary>
        /// Violet color (R:238,G:130,B:238,A:255).
        /// </summary>
        static public Color Violet
        {
            get;
            private set;
        }
        /// <summary>
        /// Wheat color (R:245,G:222,B:179,A:255).
        /// </summary>
        static public Color Wheat
        {
            get;
            private set;
        }
        /// <summary>
        /// White color (R:255,G:255,B:255,A:255).
        /// </summary>
        static public Color White
        {
            get;
            private set;
        }
        /// <summary>
        /// WhiteSmoke color (R:245,G:245,B:245,A:255).
        /// </summary>
        static public Color WhiteSmoke
        {
            get;
            private set;
        }
        /// <summary>
        /// Yellow color (R:255,G:255,B:0,A:255).
        /// </summary>
        static public Color Yellow
        {
            get;
            private set;
        }
        /// <summary>
        /// YellowGreen color (R:154,G:205,B:50,A:255).
        /// </summary>
        static public Color YellowGreen
        {
            get;
            private set;
        }

        #endregion
    }
}
