﻿using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UBeau;

namespace UBeau
{
    /// <summary>
    /// Contains helper functions for Unity objects.
    /// </summary>
    static public class Unity
    {
        #region Paths

        /// <summary>
        /// Returns the path from the root to this transform.
        /// </summary>
        static public string GetPath(this Transform inTransform)
        {
            return GetPathTo(inTransform, null);
        }

        /// <summary>
        /// Returns the path from this transform to the given parent.
        /// </summary>
        static public string GetPathTo(this Transform inTransform, Transform inParent)
        {
            using (PooledStringBuilder stringBuilder = PooledStringBuilder.Create())
            {
                StringBuilder builder = stringBuilder.Builder;
                Transform current = inTransform;
                builder.Append(inTransform.gameObject.name);
                while ((current = current.parent) != null && current != inParent)
                {
                    builder.Insert(0, '/');
                    builder.Insert(0, current.gameObject.name);
                }
                return builder.ToString();
            }
        }

        #endregion

        #region Safe deletion

        static public void SafeDestroy<T>(ref T ioObject) where T : UnityEngine.Object
        {
            if (object.ReferenceEquals(ioObject, null))
                return;

            if (ioObject)
            {
                UnityEngine.Object.Destroy(ioObject);
            }

            ioObject = null;
        }

        static public void SafeDestroy(ref Transform ioTransform)
        {
            if (object.ReferenceEquals(ioTransform, null))
                return;

            if (ioTransform && ioTransform.gameObject)
            {
                UnityEngine.Object.Destroy(ioTransform.gameObject);
            }

            ioTransform = null;
        }

        static public bool IsNull(this UnityEngine.Object inObject)
        {
            return UnityEngine.Object.ReferenceEquals(inObject, null);
        }

        #endregion

        #region Quaternions

        static public float GetZRotation(this Quaternion inQuaternion)
        {
            return inQuaternion.eulerAngles.z;
        }

        static public Quaternion ToZRotation(this Quaternion inQuaternion, float inZRotation)
        {
            Vector3 euler = inQuaternion.eulerAngles;
            euler.z = inZRotation;
            return Quaternion.Euler(euler);
        }

        #endregion

        #region Component Colors

        // UnityEngine.UI.Graphic

        static public float GetAlpha(this Graphic inRenderer)
        {
            return inRenderer.color.a;
        }

        static public void SetAlpha(this Graphic inRenderer, float inAlpha)
        {
            inRenderer.color = inRenderer.color.ToAlpha(inAlpha);
        }

        static public void SetColor(this Graphic inRenderer, Color inColor)
        {
            inColor.a = inRenderer.color.a;
            inRenderer.color = inColor;
        }

        // UnityEngine.TextMesh

        static public float GetAlpha(this TextMesh inRenderer)
        {
            return inRenderer.color.a;
        }

        static public void SetAlpha(this TextMesh inRenderer, float inAlpha)
        {
            inRenderer.color = inRenderer.color.ToAlpha(inAlpha);
        }

        static public void SetColor(this TextMesh inRenderer, Color inColor)
        {
            inColor.a = inRenderer.color.a;
            inRenderer.color = inColor;
        }

        // UnityEngine.SpriteRenderer

        static public float GetAlpha(this SpriteRenderer inRenderer)
        {
            return inRenderer.color.a;
        }

        static public void SetAlpha(this SpriteRenderer inRenderer, float inAlpha)
        {
            inRenderer.color = inRenderer.color.ToAlpha(inAlpha);
        }

        static public void SetColor(this SpriteRenderer inRendere, Color inColor)
        {
            inColor.a = inRendere.color.a;
            inRendere.color = inColor;
        }

        #endregion

        #region GetComponentInParent

        static public T GetComponentInParent<T>(this Component inComponent, bool inbIncludeInactive)
        {
            if (!inbIncludeInactive)
                return inComponent.GetComponentInParent<T>();

            Transform current = inComponent.transform;
            T component = current.GetComponent<T>();
            while (component == null && (current = current.parent) != null)
            {
                component = current.GetComponent<T>();
            }
            return component;
        }

        static public T GetComponentInParent<T>(this GameObject inObject, bool inbIncludeInactive)
        {
            if (!inbIncludeInactive)
                return inObject.GetComponentInParent<T>();

            Transform current = inObject.transform;
            T component = current.GetComponent<T>();
            while (component == null && (current = current.parent) != null)
            {
                component = current.GetComponent<T>();
            }
            return component;
        }

        #endregion

        #region Hit tests

        static public bool HitTest(this RectTransform inTransform, Vector2 inScreenPosition)
        {
            Vector2 localPos = inTransform.InverseTransformPoint(inScreenPosition);
            return inTransform.rect.Contains(localPos);
        }

        #endregion
    }
}
