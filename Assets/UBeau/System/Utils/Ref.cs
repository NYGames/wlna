﻿using System;

namespace UBeau
{
    /// <summary>
    /// Helper functions operating on IDisposables and references.
    /// </summary>
    static public class Ref
    {
        /// <summary>
        /// Safely disposes of an object and sets the reference to null.
        /// </summary>
        static public void Dispose<T>(ref T inObjectToDispose) where T : class, IDisposable
        {
            if (inObjectToDispose != null)
            {
                inObjectToDispose.Dispose();
                inObjectToDispose = null;
            }
        }

        /// <summary>
        /// Safely disposes and switches a disposable object to another object.
        /// </summary>
        static public void Replace<T>(ref T inObject, T inObjectToReplace) where T : class, IDisposable
        {
            if (inObject != null && inObject != inObjectToReplace)
                inObject.Dispose();
            inObject = inObjectToReplace;
        }

        /// <summary>
        /// Swaps two references.
        /// </summary>
        static public void Swap<T>(ref T inA, ref T inB)
        {
            T temp = inA;
            inA = inB;
            inB = temp;
        }
    }

    /// <summary>
    /// To be used in place of "ref" parameters
    /// in cases where they aren't allowed (IEnumerator functions, for example)
    /// </summary>
    public sealed class Ref<T>
    {
        public T Value;

        public Ref()
        {
            Value = default(T);
        }

        public Ref(T inValue)
        {
            Value = inValue;
        }

        static public implicit operator T(Ref<T> inRef)
        {
            return inRef.Value;
        }

        static public implicit operator Ref<T>(T inValue)
        {
            return new Ref<T>(inValue);
        }
    }
}
