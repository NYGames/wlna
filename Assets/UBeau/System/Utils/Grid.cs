﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UBeau
{
    /// <summary>
    /// Wrapper for a two-dimensional array.
    /// </summary>
    [Serializable]
    public class Grid<T> : IEnumerable<T>
    {
        [SerializeField]
        protected T[] m_Data;

        [SerializeField]
        protected int m_Width;

        [SerializeField]
        protected int m_Height;

        /// <summary>
        /// Creates a 1x1 grid.
        /// </summary>
        public Grid()
            : this(1, 1)
        { }

        /// <summary>
        /// Creates a grid of the specified dimensions.
        /// </summary>
        public Grid(int inWidth, int inHeight)
        {
            m_Width = inWidth;
            m_Height = inHeight;

            m_Data = new T[inWidth * inHeight];
        }

        /// <summary>
        /// Creates a grid of the specified dimensions
        /// and initializes all cells to the given value.
        /// </summary>
        public Grid(int inWidth, int inHeight, T inDefault)
            : this(inWidth, inHeight)
        {
            for (int i = 0; i < m_Data.Length; ++i)
                m_Data[i] = inDefault;
        }

        /// <summary>
        /// Creates a grid from another grid.
        /// </summary>
        public Grid(Grid<T> inGrid)
        {
            m_Width = inGrid.m_Width;
            m_Height = inGrid.m_Height;

            m_Data = (T[])inGrid.m_Data.Clone();
        }

        /// <summary>
        /// The width of the grid.
        /// </summary>
        public int Width
        {
            get { return m_Width; }
        }

        /// <summary>
        /// The height of the grid.
        /// </summary>
        public int Height
        {
            get { return m_Height; }
        }

        /// <summary>
        /// Total number of cells.
        /// </summary>
        public int Size
        {
            get { return m_Width * m_Height; }
        }

        #region Indexes

        /// <summary>
        /// Calculates the x position for the given cell index.
        /// </summary>
        public int IndexToX(int inIndex)
        {
            return inIndex % m_Width;
        }

        /// <summary>
        /// Calculates the y position for the given cell index.
        /// </summary>
        public int IndexToY(int inIndex)
        {
            return inIndex / m_Width;
        }

        /// <summary>
        /// Calculates the cell index for the given position.
        /// </summary>
        public int PositionToIndex(int inX, int inY)
        {
            return inX + inY * m_Width;
        }

        /// <summary>
        /// Returns if the cell index is within bounds.
        /// </summary>
        public bool IsValidIndex(int inIndex)
        {
            return inIndex >= 0 && inIndex < m_Data.Length;
        }

        /// <summary>
        /// Returns if the cell position is within bounds.
        /// </summary>
        public bool IsValidPosition(int inX, int inY)
        {
            return inX >= 0 && inX < m_Width && inY >= 0 && inY < m_Height;
        }

        #endregion

        #region Grid values

        /// <summary>
        /// Value at the given cell by position.
        /// </summary>
        public T this[int inX, int inY]
        {
            get { return m_Data[inX + inY * m_Width]; }
            set { m_Data[inX + inY * m_Width] = value; }
        }

        /// <summary>
        /// Value at the given cell by index.
        /// </summary>
        public T this[int inIndex]
        {
            get { return m_Data[inIndex]; }
            set { m_Data[inIndex] = value; }
        }

        /// <summary>
        /// Attempts to retrieve the value at the given position.
        /// Returns the index of the value, or -1 if an invalid position.
        /// </summary>
        public int TryGetValue(int inX, int inY, out T outData)
        {
            if (inX < 0 || inX >= m_Width || inY < 0 || inY >= m_Height)
            {
                outData = default(T);
                return -1;
            }

            int index = inX + inY * m_Width;
            outData = m_Data[index];
            return index;
        }

        /// <summary>
        /// Returns the index of the given value.
        /// </summary>
        public int IndexOf(T inValue)
        {
            return Array.IndexOf(m_Data, inValue);
        }

        /// <summary>
        /// Returns the position of the given value.
        /// </summary>
        public int PositionOf(T inValue, out int outX, out int outY)
        {
            int index = Array.IndexOf(m_Data, inValue);
            if (index >= 0)
            {
                outX = index % m_Width;
                outY = index / m_Width;
            }
            else
            {
                outX = outY = -1;
            }

            return index;
        }

        /// <summary>
        /// Clears all cells to the default value.
        /// </summary>
        public void Clear()
        {
            Clear(default(T));
        }

        /// <summary>
        /// Clears all cells to the given value.
        /// </summary>
        public void Clear(T inValue)
        {
            for (int i = 0; i < m_Data.Length; ++i)
                m_Data[i] = inValue;
        }

        /// <summary>
        /// Resizes the array to the given dimensions.
        /// </summary>
        public void Resize(int inWidth, int inHeight)
        {
            // If we're only expanding vertically, we can simply
            // resize the array with no index changes
            if (m_Width == inWidth)
            {
                if (m_Height == inHeight)
                    return;

                m_Height = inHeight;
                Array.Resize(ref m_Data, m_Width * m_Height);
                return;
            }

            T[] newData = new T[inWidth * inHeight];
            int copyWidth = inWidth < m_Width ? inWidth : m_Width;
            int copyHeight = inHeight < m_Height ? inHeight : m_Height;
            for(int y = 0; y < copyHeight; ++y)
                for(int x = 0; x < copyWidth; ++x)
                    newData[x + y * inWidth] = m_Data[x + y * m_Width];

            m_Data = newData;
            m_Width = inWidth;
            m_Height = inHeight;
        }

        #endregion

        #region IEnumerable

        public IEnumerator<T> GetEnumerator()
        {
            if (typeof(T).IsClass)
            {
                for (int i = 0; i < m_Data.Length; ++i)
                    if (m_Data[i] != null)
                        yield return m_Data[i];
            }
            else
            {
                for (int i = 0; i < m_Data.Length; ++i)
                    yield return m_Data[i];
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
