﻿using System;
using UnityEngine;

namespace UBeau
{
    /// <summary>
    /// ScriptableObject with validation functionality.
    /// </summary>
    public abstract class DataAsset : ScriptableObject
    {
        [NonSerialized]
        private bool m_Validated;

        // Unity callback for when 
        private void OnValidate()
        {
            TryValidate();
        }

        /// <summary>
        /// Validates the object if it hasn't
        /// already been validated.
        /// </summary>
        public void TryValidate()
        {
            if (!m_Validated && CanValidate())
            {
                m_Validated = true;
                Validate();
            }
        }

        /// <summary>
        /// Returns if validation can be performed.
        /// </summary>
        static public bool CanValidate()
        {
#if UNITY_EDITOR
            return UnityEditor.EditorApplication.isPlaying;
#else
            return true;
#endif
        }

        /// <summary>
        /// Validates the object.
        /// </summary>
        public virtual void Validate() { }
    }
}
