﻿using System.Diagnostics;
using System.Collections.Generic;
using System.Text;

namespace UBeau
{
    /// <summary>
    /// Conditionally-compiled assertions
    /// </summary>
    static public class Assert
    {
        #region Initialization

        static private bool s_Initialized = false;

        static Assert()
        {
            Initialize();
        }

        /// <summary>
        /// Initializes exception handling.
        /// </summary>
        static public void Initialize()
        {
            if (s_Initialized)
                return;

            s_Initialized = true;
            UnityEngine.Application.logMessageReceived += Application_logMessageReceived;
        }

        static private void Application_logMessageReceived(string condition, string stackTrace, UnityEngine.LogType type)
        {
            if (type == UnityEngine.LogType.Exception)
            {
                OnFailure(GetExceptionLocation(stackTrace), string.Format("Uncaught Exception: {0}\nStack: {1}", condition, stackTrace));
            }
        }

        #endregion

        #region Error Response

        // list of hashed locations to ignore
        static private HashSet<int> s_LocationsToIgnore = new HashSet<int>();

        static private void OnFailure(string inLocation, string inMessage)
        {
            string message = "[ASSERT]";
            if (string.IsNullOrEmpty(inMessage))
                message += ": [LOCATION: " + inLocation + "]";
            else
                message += ": " + inMessage + "\n[LOCATION: " + inLocation + "]";

            UnityEngine.Debug.LogAssertion(message);

            int locationHash = UnityEngine.Animator.StringToHash(inLocation);
            if (s_LocationsToIgnore.Contains(locationHash))
                return;

            ErrorResult result = ShowErrorMessage(message);
            if (result == ErrorResult.IgnoreAll)
            {
                s_LocationsToIgnore.Add(locationHash);
            }
            else if (result == ErrorResult.Break)
            {
#if UNITY_EDITOR
                if (UnityEditor.EditorApplication.isPlaying)
                    UnityEngine.Debug.Break();
#else
                ShowCrashScreen(message);
#endif
            }
        }

        static private ErrorResult ShowErrorMessage(string inMessage)
        {
#if UNITY_EDITOR
            if (UnityEditor.EditorApplication.isPlaying)
            {
                int result = UnityEditor.EditorUtility.DisplayDialogComplex("Assert Fail", inMessage, "Ignore", "Ignore All", "Break");
                if (result == 0)
                    return ErrorResult.Ignore;
                else if (result == 1)
                    return ErrorResult.IgnoreAll;
            }
#endif
            return ErrorResult.Break;
        }

        static private void ShowCrashScreen(string inMessage)
        {
            //TODO(Alex): Implement crash screen
        }

        static private string GetStackLocation(int inDepth)
        {
#if !(DISABLE_STACK_TRACE)
            StackTrace stackTrace = new StackTrace();
            StackFrame frame = stackTrace.GetFrame(inDepth + 1);

            string typeName = frame.GetMethod().DeclaringType.Name;
            string methodName = frame.GetMethod().Name;
            int lineNumber = frame.GetFileLineNumber();
            int columnNmber = frame.GetFileColumnNumber();

            if (lineNumber == 0)
                return string.Format("{0}::{1}", typeName, methodName);

            return string.Format("{0}::{1} @ {2}({3})", typeName, methodName, lineNumber, columnNmber);
#else
            return "[No Stack Trace Available]";
#endif
        }

        static private string GetExceptionLocation(string inStackTrace)
        {
            string[] lines = inStackTrace.Split('\n');
            return lines.Length > 0 ? lines[0].Trim() : "[No Stack Trace Available]";
        }

        #endregion

        #region Asserts

        #region True

        [Conditional("DEVELOPMENT")]
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void True(bool inbCondition)
        {
            if (inbCondition)
                return;

            OnFailure(GetStackLocation(1), string.Empty);
        }

        [Conditional("DEVELOPMENT")]
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void True(bool inbCondition, string inMessage)
        {
            if (inbCondition)
                return;

            OnFailure(GetStackLocation(1), inMessage);
        }

        [Conditional("DEVELOPMENT")]
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void True(bool inbCondition, string inMessage, params object[] inMessageParams)
        {
            if (inbCondition)
                return;

            OnFailure(GetStackLocation(1), string.Format(inMessage, inMessageParams));
        }

        #endregion

        #region False

        [Conditional("DEVELOPMENT")]
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void False(bool inbCondition)
        {
            if (!inbCondition)
                return;

            OnFailure(GetStackLocation(1), string.Empty);
        }

        [Conditional("DEVELOPMENT")]
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void False(bool inbCondition, string inMessage)
        {
            if (!inbCondition)
                return;

            OnFailure(GetStackLocation(1), inMessage);
        }

        [Conditional("DEVELOPMENT")]
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void False(bool inbCondition, string inMessage, params object[] inMessageParams)
        {
            if (!inbCondition)
                return;

            OnFailure(GetStackLocation(1), string.Format(inMessage, inMessageParams));
        }

        #endregion

        #region NotNull

        [Conditional("DEVELOPMENT")]
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void NotNull<T>(T inValue) where T : class
        {
            if (inValue != null)
                return;

            OnFailure(GetStackLocation(1), "Object of type " + typeof(T).GetGenericName() + ") is null.");
        }

        [Conditional("DEVELOPMENT")]
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void NotNull<T>(T inValue, string inMessage) where T : class
        {
            if (inValue != null)
                return;

            OnFailure(GetStackLocation(1), inMessage);
        }

        [Conditional("DEVELOPMENT")]
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void NotNull<T>(T inValue, string inMessage, params object[] inMessageParams) where T : class
        {
            if (inValue != null)
                return;

            OnFailure(GetStackLocation(1), string.Format(inMessage, inMessageParams));
        }

        #endregion

        #region NotEmpty

        [Conditional("DEVELOPMENT")]
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void NotEmpty(string inValue)
        {
            if (!string.IsNullOrEmpty(inValue))
                return;

            OnFailure(GetStackLocation(1), string.Empty);
        }

        [Conditional("DEVELOPMENT")]
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void NotEmpty(string inValue, string inMessage)
        {
            if (!string.IsNullOrEmpty(inValue))
                return;

            OnFailure(GetStackLocation(1), inMessage);
        }

        [Conditional("DEVELOPMENT")]
        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void NotEmpty(string inValue, string inMessage, params object[] inMessageParams)
        {
            if (!string.IsNullOrEmpty(inValue))
                return;

            OnFailure(GetStackLocation(1), string.Format(inMessage, inMessageParams));
        }

        #endregion

        #endregion

        #region Fail

        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void Fail(string inMessage)
        {
            OnFailure(GetStackLocation(1), inMessage);
        }

        [DebuggerHidden]
        [DebuggerStepThrough]
        static public void Fail(string inMessage, params object[] inMessageParams)
        {
            OnFailure(GetStackLocation(1), string.Format(inMessage, inMessageParams));
        }

        #endregion

        public enum ErrorResult
        {
            Break,
            Ignore,
            IgnoreAll
        }
    }
}
