﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UBeau;

namespace UBeau
{
    /// <summary>
    /// Helper functions for strings.
    /// </summary>
    static public class Strings
    {
#if DEVELOPMENT
        static private Dictionary<int, string> s_ReverseHashes = new Dictionary<int, string>(256);
#endif

        /// <summary>
        /// Generates a hash value for the string.
        /// </summary>
        static public int Hash(this string inString)
        {
            int hash = Animator.StringToHash(inString);
            Assert.True(hash != 0, "Hash for '{0}' evaluates to 0!", inString);

#if DEVELOPMENT
            string reverseHash;
            if (s_ReverseHashes.TryGetValue(hash, out reverseHash))
            {
                Assert.True(inString == reverseHash, "Duplicate hash value {0} for '{1}' and '{2}'.", hash, inString, reverseHash);
            }
            else
            {
                s_ReverseHashes.Add(hash, inString);
            }
#endif

            return hash;
        }

        /// <summary>
        /// Retrieves the string that generated the hash value.
        /// Only functional in development builds.
        /// </summary>
        static public string ReverseHash(int inHash)
        {
#if DEVELOPMENT
            string reverseHash;
            if (!s_ReverseHashes.TryGetValue(inHash, out reverseHash))
            {
                Log.Warn("Unable to find reverse hash for {0}.", inHash);
                reverseHash = string.Empty;
            }

            return reverseHash;
#else
            throw new Exception("Cannot use Strings.ReverseHash in a non-development build.");
#endif
        }

        /// <summary>
        /// String split that doesn't allocate a new array.
        /// </summary>
        /// <param name="ioSplit">List to store results..</param>
        /// <param name="inDelim">Character to split at.</param>
        /// <param name="inOptions">Split options.</param>
        static public void Split(this string inString, List<string> ioSplit, char inDelim, StringSplitOptions inOptions = StringSplitOptions.RemoveEmptyEntries)
        {
            Assert.NotNull(ioSplit, "Cannot provide null list to Strings.Split");

            using (PooledStringBuilder stringBuilder = PooledStringBuilder.Create())
            {
                StringBuilder builder = stringBuilder.Builder;

                int withinTagCount = 0;
                for (int i = 0; i < inString.Length; ++i)
                {
                    char c = inString[i];

                    if (c == '<')
                        ++withinTagCount;
                    else if (c == '>' && withinTagCount > 0)
                        --withinTagCount;

                    if (c == inDelim)
                    {
                        if (withinTagCount == 0 && (builder.Length > 0 || inOptions != StringSplitOptions.RemoveEmptyEntries))
                        {
                            ioSplit.Add(builder.ToString());
                            builder.Length = 0;
                        }
                        else if (withinTagCount > 0)
                        {
                            builder.Append(c);
                        }
                    }
                    else
                    {
                        builder.Append(c);
                    }
                }

                if (builder.Length > 0)
                    ioSplit.Add(builder.ToString());
            }
        }

        /// <summary>
        /// Wraps the given string into multiple lines of a maximum length.
        /// </summary>
        static public string Wrap(this string inString, int inMaxLineLength)
        {
            using (PooledStringBuilder completeString = PooledStringBuilder.Create())
            using (PooledList<string> words = PooledList<string>.Create())
            {
                Split(inString, words, ' ', StringSplitOptions.None);
                int lineLength = 0;
                for (int i = 0; i < words.Count; ++i)
                {
                    string word = words[i];

                    // Account for newline characters in the word
                    int newlineIndex = word.IndexOf('\n');
                    bool bExtraNewline = false;
                    if (newlineIndex >= 0)
                    {
                        bExtraNewline = true;
                        words[i--] = word.Substring(newlineIndex + 1);
                        word = word.Substring(0, newlineIndex);
                    }

                    int wordLength = word.Length;

                    // Don't count tags towards our line length
                    int tagStart = word.IndexOf('<', 0);
                    while (tagStart >= 0)
                    {
                        int tagEnd = word.IndexOf('>', tagStart + 1);
                        wordLength -= tagEnd - tagStart + 1;
                        if (tagEnd != -1)
                        {
                            tagStart = word.IndexOf('<', tagEnd + 1);
                        }
                        else
                        {
                            tagStart = -1;
                        }
                    }

                    if (lineLength + wordLength > inMaxLineLength)
                    {
                        // Don't end a line with a space
                        if (completeString.Builder[completeString.Builder.Length - 1] == ' ')
                            --completeString.Builder.Length;

                        completeString.Builder.Append('\n');
                        lineLength = 0;
                    }
                    completeString.Builder.Append(word);
                    lineLength += wordLength;

                    if (i < words.Count - 1 && !bExtraNewline)
                    {
                        if (lineLength + 1 > inMaxLineLength)
                        {
                            completeString.Builder.Append('\n');
                            lineLength = 0;
                        }
                        else
                        {
                            completeString.Builder.Append(' ');
                            ++lineLength;
                        }
                    }

                    if (bExtraNewline)
                    {
                        completeString.Builder.Append('\n');
                        lineLength = 0;
                    }
                }

                // Don't end with a newline character
                if (completeString.Builder[completeString.Builder.Length - 1] == '\n')
                    --completeString.Builder.Length;

                return completeString.ToString();
            }
        }
    }
}
