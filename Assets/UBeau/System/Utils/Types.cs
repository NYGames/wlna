﻿using System;
using System.Reflection;
using System.Text;
using UBeau;

namespace UBeau
{
    static public class Types
    {
        /// <summary>
        /// Formats type names to include generic types.
        /// </summary>
        static public string GetGenericName(this Type inType)
        {
            if (!inType.IsGenericType)
                return inType.Name;

            using (PooledStringBuilder stringBuilder = PooledStringBuilder.Create())
            {
                StringBuilder builder = stringBuilder.Builder;
                builder.Append(inType.Name.Substring(0, inType.Name.IndexOf('`')));

                Type[] genericArguments = inType.GetGenericArguments();
                builder.Append('<');
                for (int i = 0; i < genericArguments.Length; ++i)
                {
                    builder.Append(GetGenericName(genericArguments[i]));

                    if (i < genericArguments.Length - 1)
                        builder.Append(", ");
                }
                builder.Append('>');

                return builder.ToString();
            }
        }

        static public T GetCustomAttribute<T>(MemberInfo inInfo) where T : Attribute
        {
            T[] attributes = (T[])Attribute.GetCustomAttributes(inInfo, typeof(T));
            if (attributes.Length > 0)
                return attributes[0];
            return null;
        }

        static public T[] GetCustomAttributes<T>(MemberInfo inInfo) where T : Attribute
        {
            return (T[])Attribute.GetCustomAttributes(inInfo, typeof(T));
        }

        static public T GetCustomAttribute<T>(Type inType) where T : Attribute
        {
            T[] attributes = (T[])Attribute.GetCustomAttributes(inType, typeof(T));
            if (attributes.Length > 0)
                return attributes[0];
            return null;
        }

        static public T[] GetCustomAttributes<T>(Type inType) where T : Attribute
        {
            return (T[])Attribute.GetCustomAttributes(inType, typeof(T));
        }

        static public bool HasAttribute<T>(Type inType) where T : Attribute
        {
            return Attribute.IsDefined(inType, typeof(T));
        }

        static public bool HasAttribute<T>(MemberInfo inInfo) where T : Attribute
        {
            return Attribute.IsDefined(inInfo, typeof(T));
        }
    }
}
