﻿using System;

namespace UBeau
{
    public struct CallOnDispose : IDisposable
    {
        public CallOnDispose(Action inAction)
        {
            Action = inAction;
        }

        public Action Action;

        public void Dispose()
        {
            if (Action != null)
            {
                Action();
                Action = null;
            }
        }
    }
}
