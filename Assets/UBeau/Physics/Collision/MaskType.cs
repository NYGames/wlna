﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UBeau.Collisions
{
    /// <summary>
    /// Types of collision masks.
    /// </summary>
    public enum CollisionMaskType
    {
        /// <summary>
        /// A single point.
        /// </summary>
        Point,

        /// <summary>
        /// A circle.
        /// </summary>
        Circle,

        /// <summary>
        /// A box.
        /// </summary>
        Box,

        /// <summary>
        /// A grid.
        /// </summary>
        Grid,
    }
}
