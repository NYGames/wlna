﻿using SimpleJSON;
using System;
using System.Collections.Generic;
using UBeau;

namespace UBeau.Serialization
{
    /// <summary>
    /// Serializes objects into JSON.
    /// </summary>
    public sealed partial class DataSerializer
    {
        private const string NULL = "__null";
        private const string TYPE = "__type";
        private const string VERSION = "Version";

        private bool m_IsReading;

        private Stack<JSONNode> m_Stack;
        private JSONNode m_Current;
        private JSONNode m_Root;

        private Stack<uint> m_Versions = new Stack<uint>();

        private DataSerializer(bool inbReading, JSONNode inStartingNode)
        {
            m_IsReading = inbReading;
            m_Stack = new Stack<JSONNode>();
            m_Stack.Push(m_Root = m_Current = inStartingNode);
        }

        /// <summary>
        /// If this is reading data.
        /// </summary>
        public bool IsReading { get { return m_IsReading; } }

        /// <summary>
        /// If this is writing data.
        /// </summary>
        public bool IsWriting { get { return !m_IsReading; } }

        /// <summary>
        /// Current node.
        /// </summary>
        public JSONNode Node { get { return m_Current; } }

        /// <summary>
        /// Root node.
        /// </summary>
        public JSONNode Root { get { return m_Root; } }

        #region Stack

        private void PushNodeKey(string inKey)
        {
            m_Stack.Push(m_Current = m_Current[inKey]);
        }

        private void PushNode(JSONNode inNode)
        {
            m_Stack.Push(m_Current = inNode);
        }

        private JSONNode AddPushNode(JSONNode inNode)
        {
            m_Current.Add(inNode);
            m_Stack.Push(m_Current = inNode);
            return inNode;
        }

        private JSONNode AddPushNode(string inKey, JSONNode inNode)
        {
            if (inKey == null)
                return AddPushNode(inNode);

            m_Current.Add(inKey, inNode);
            m_Stack.Push(m_Current = inNode);
            return inNode;
        }

        private void PopNode()
        {
            Assert.True(m_Stack.Count > 1, "Cannot pop a node.");
            m_Stack.Pop();
            m_Current = m_Stack.Peek();
        }

        private bool NullNode
        {
            get { return m_Current.Value == NULL; }
        }

        private bool MissingNode
        {
            get { return m_Current == null; }
        }

        private void WriteNull()
        {
            m_Current.Add(new JSONData(NULL));
        }

        private void WriteNull(string inKey)
        {
            if (inKey == null)
                WriteNull();
            else
                m_Current.Add(inKey, NULL);
        }

        #endregion

        #region Groups

        /// <summary>
        /// Starts serialization of a group.
        /// </summary>
        public bool BeginGroup(string inKey)
        {
            if (IsReading)
            {
                PushNodeKey(inKey);
                if (MissingNode)
                {
                    Log.Error("Unable to begin group at '{0}'!", inKey);
                    return false;
                }

                return true;
            }

            AddPushNode(inKey, new JSONClass());
            return true;
        }

        /// <summary>
        /// Returns if the current node has the given key.
        /// </summary>
        public bool HasProperty(string inKey)
        {
            return Node[inKey] != null;
        }

        /// <summary>
        /// Ends serialization of a group.
        /// </summary>
        public void EndGroup()
        {
            PopNode();
        }

        #endregion

        #region Versions

        /// <summary>
        /// Serializes version number with the default key "VERSION"
        /// and pushes it onto the version number stack.
        /// </summary>
        public bool PushVersion(ref uint ioVersion, uint? inDefault = null)
        {
            return PushVersion(VERSION, ref ioVersion, inDefault);
        }

        /// <summary>
        /// Serializes a version number
        /// and pushes it onto the version number stack.
        /// </summary>
        public bool PushVersion(string inKey, ref uint ioVersion, uint? inDefault = null)
        {
            bool bSuccess = Serialize(inKey, ref ioVersion, IsReading ? inDefault : null);
            m_Versions.Push(ioVersion);
            return bSuccess;
        }

        /// <summary>
        /// Current version number.
        /// </summary>
        public uint Version
        {
            get
            {
                if (m_Versions.Count > 0)
                    return m_Versions.Peek();
                return default(uint);
            }
        }

        /// <summary>
        /// Pops a version from the version number stack.
        /// </summary>
        public void PopVersion()
        {
            if (m_Versions.Count > 0)
                m_Versions.Pop();
            else
                Log.Error("Empty version number stack!");
        }

        #endregion

        #region Constructors

        private Dictionary<Type, Delegate> m_Constructors = new Dictionary<Type, Delegate>();

        /// <summary>
        /// Registers a constructor for the given type.
        /// </summary>
        public void RegisterConstructor<T>(Func<T> inConstructor) where T : ISerializedData
        {
            m_Constructors[typeof(T)] = inConstructor;
        }
        
        /// <summary>
        /// Unregisters the constructor for the given type.
        /// </summary>
        public void UnregisterConstructor<T>() where T : ISerializedData
        {
            m_Constructors.Remove(typeof(T));
        }

        // Attempts to create an instance using the per-serializer constructors
        private bool TryConstructor(Type inType, out ISerializedData outResult)
        {
            Delegate constructor;
            if (m_Constructors.TryGetValue(inType, out constructor))
            {
                outResult = (ISerializedData)constructor.DynamicInvoke();
                return true;
            }

            outResult = null;
            return false;
        }

        #endregion

        #region Default Constructors

        static private Dictionary<Type, Delegate> s_DefaultConstructors = new Dictionary<Type, Delegate>();

        /// <summary>
        /// Registers a constructor for the given type.
        /// </summary>
        static public void RegisterDefaultConstructor<T>(Func<T> inConstructor) where T : ISerializedData
        {
            s_DefaultConstructors[typeof(T)] = inConstructor;
        }

        /// <summary>
        /// Unregisters the constructor for the given type.
        /// </summary>
        static public void UnregisterDefaultConstructor<T>() where T : ISerializedData
        {
            s_DefaultConstructors.Remove(typeof(T));
        }

        // Attempts to create an instance using the default constructors
        static private bool TryDefaultConstructor(Type inType, out ISerializedData outResult)
        {
            Delegate constructor;
            if (s_DefaultConstructors.TryGetValue(inType, out constructor))
            {
                outResult = (ISerializedData)constructor.DynamicInvoke();
                return true;
            }

            outResult = null;
            return false;
        }

        #endregion

        #region Instantiation

        static private ISerializedData Instantiate(Type inType, DataSerializer inSerializer)
        {
            ISerializedData result = null;

            bool bSuccess = false;
            if (inSerializer != null)
                bSuccess = inSerializer.TryConstructor(inType, out result);
            if (!bSuccess)
                bSuccess = TryDefaultConstructor(inType, out result);
            if (!bSuccess)
                bSuccess = TrySystemConstructor(inType, out result);

            Assert.True(bSuccess, "Unable to create instance of type '{0}'!", inType.GetGenericName());
            return result;
        }

        static private bool TrySystemConstructor(Type inType, out ISerializedData outResult)
        {
            try
            {
                outResult = (ISerializedData)System.Activator.CreateInstance(inType);
                return true;
            }
            catch(Exception e)
            {
                Log.Error("Unable to instantiate object of type '{0}': {1}", inType.GetGenericName(), e.Message);
                outResult = null;
                return false;
            }
        }

        #endregion
    }
}
