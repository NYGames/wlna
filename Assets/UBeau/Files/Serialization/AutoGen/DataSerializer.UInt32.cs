using System;
using System.Collections.Generic;
using SimpleJSON;

namespace UBeau.Serialization
{
    public sealed partial class DataSerializer
    {
        #region Reading and Writing Routines

        // Reads the UInt32 value from the current node.
        private bool Read(ref UInt32 ioData, UInt32? inDefault)
        {
            if (MissingNode)
            {
                if (inDefault.HasValue)
				{
                    ioData = inDefault.Value;
					return true;
				}
                else
                    return false;
            }
            else if (NullNode)
            {
                if (inDefault.HasValue)
                    ioData = inDefault.Value;
                else
                    ioData = default(UInt32);
                return true;
            }

            ioData = (UInt32)Node.AsUInt;
            return true;
        }

        // Writes the UInt32 value to the current node.
        private bool Write(string inKey, ref UInt32 ioData, UInt32? inDefault)
        {
            if (!inDefault.HasValue || ioData != inDefault.Value)
            {
                AddPushNode(inKey, new JSONData(ioData));
                PopNode();
            }
            else
            {
                if (string.IsNullOrEmpty(inKey))
                    WriteNull();
            }
            return true;
        }

        #endregion

        /// <summary>
        /// Serializes a UInt32 value.
        /// </summary>
        public bool Serialize(string inKey, ref UInt32 ioData, UInt32? inDefault = null)
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    bSuccess = Read(ref ioData, inDefault);
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read UInt32 value at '{0}'.", inKey);

                return bSuccess;
            }

            return Write(inKey, ref ioData, inDefault);
        }

        /// <summary>
        /// Serializes a UInt32 list.
        /// </summary>
        public bool Array(string inKey, ref List<UInt32> ioArray)
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    if (MissingNode)
                    {
                        bSuccess = false;
                    }
                    else if (NullNode)
                    {
                        if (ioArray != null)
                            ioArray.Clear();
                        ioArray = null;
                    }
                    else
                    {
                        if (ioArray == null)
                            ioArray = new List<UInt32>(Node.AsArray.Count);
						else
							ioArray.Clear();

                        foreach (var node in Node.AsArray)
                        {
                            PushNode(node);
                            {
                                UInt32 part = default(UInt32);
                                bSuccess &= Read(ref part, null);
                                ioArray.Add(part);
                            }
                            PopNode();
                        }
                    }
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read UInt32 array at '{0}'.", inKey);

                return bSuccess;
            }

            if (ioArray != null)
            {
                AddPushNode(inKey, new JSONArray());
                for (int i = 0; i < ioArray.Count; ++i)
                {
                    UInt32 obj = ioArray[i];
                    Write(null, ref obj, null);
                }
                PopNode();
            }
            else
            {
                WriteNull(inKey);
            }

            return true;
        }

        /// <summary>
        /// Serializes a UInt32 array.
        /// </summary>
        public bool Array(string inKey, ref UInt32[] ioArray)
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    if (MissingNode)
                    {
                        bSuccess = false;
                    }
                    else if (NullNode)
                    {
                        ioArray = null;
                    }
                    else
                    {
                        int maxIndex;
                        if (ioArray == null)
                            ioArray = new UInt32[maxIndex = Node.Count];
                        else
                        {
                            maxIndex = Math.Max(Node.Count, ioArray.Length);
                            if (maxIndex > ioArray.Length)
                                System.Array.Resize(ref ioArray, maxIndex);
                        }

                        for (int i = 0; i < maxIndex; ++i)
                        {
                            PushNode(Node[i]);
                            Read(ref ioArray[i], null);
                            PopNode();
                        }
                        for (int i = maxIndex; i < ioArray.Length; ++i)
                        {
                            ioArray[i] = default(UInt32);
                        }
                    }
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read UInt32 array at '{0}'.", inKey);

                return bSuccess;
            }

            if (ioArray != null)
            {
                AddPushNode(inKey, new JSONArray());
                for (int i = 0; i < ioArray.Length; ++i)
                {
                    UInt32 obj = ioArray[i];
                    Write(null, ref obj, null);
                }
                PopNode();
            }
            else
            {
                WriteNull(inKey);
            }

            return true;
        }

        /// <summary>
        /// Serializes a string/UInt32 dictionary.
        /// </summary>
        public bool Map(string inKey, ref Dictionary<string, UInt32> ioMap)
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    if (MissingNode)
                    {
                        bSuccess = false;
                    }
                    else if (NullNode)
                    {
                        if (ioMap != null)
                            ioMap.Clear();
                        ioMap = null;
                    }
                    else
                    {
                        if (ioMap == null)
                            ioMap = new Dictionary<string, UInt32>(Node.AsObject.Count);
                        else
                            ioMap.Clear();

                        foreach (var keyValue in Node.AsObject)
                        {
                            string key = keyValue.Key;
                            PushNode(keyValue.Value);
                            {
                                UInt32 part = default(UInt32);
                                bSuccess &= Read(ref part, null);
                                ioMap.Add(key, part);
                            }
                            PopNode();
                        }
                    }
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read UInt32 map at '{0}'.", inKey);

                return bSuccess;
            }

            if (ioMap != null)
            {
                AddPushNode(inKey, new JSONClass());
                foreach (var keyValue in ioMap)
                {
                    UInt32 obj = keyValue.Value;
                    Write(keyValue.Key, ref obj, null);
                }
                PopNode();
            }
            else
            {
                WriteNull(inKey);
            }

            return true;
        }
    }
}
