using System;
using System.Collections.Generic;
using SimpleJSON;

namespace UBeau.Serialization
{
    public sealed partial class DataSerializer
    {
        #region Reading and Writing Routines

        // Reads the Double value from the current node.
        private bool Read(ref Double ioData, Double? inDefault)
        {
            if (MissingNode)
            {
                if (inDefault.HasValue)
				{
                    ioData = inDefault.Value;
					return true;
				}
                else
                    return false;
            }
            else if (NullNode)
            {
                if (inDefault.HasValue)
                    ioData = inDefault.Value;
                else
                    ioData = default(Double);
                return true;
            }

            ioData = (Double)Node.AsDouble;
            return true;
        }

        // Writes the Double value to the current node.
        private bool Write(string inKey, ref Double ioData, Double? inDefault)
        {
            if (!inDefault.HasValue || ioData != inDefault.Value)
            {
                AddPushNode(inKey, new JSONData(ioData));
                PopNode();
            }
            else
            {
                if (string.IsNullOrEmpty(inKey))
                    WriteNull();
            }
            return true;
        }

        #endregion

        /// <summary>
        /// Serializes a Double value.
        /// </summary>
        public bool Serialize(string inKey, ref Double ioData, Double? inDefault = null)
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    bSuccess = Read(ref ioData, inDefault);
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read Double value at '{0}'.", inKey);

                return bSuccess;
            }

            return Write(inKey, ref ioData, inDefault);
        }

        /// <summary>
        /// Serializes a Double list.
        /// </summary>
        public bool Array(string inKey, ref List<Double> ioArray)
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    if (MissingNode)
                    {
                        bSuccess = false;
                    }
                    else if (NullNode)
                    {
                        if (ioArray != null)
                            ioArray.Clear();
                        ioArray = null;
                    }
                    else
                    {
                        if (ioArray == null)
                            ioArray = new List<Double>(Node.AsArray.Count);
						else
							ioArray.Clear();

                        foreach (var node in Node.AsArray)
                        {
                            PushNode(node);
                            {
                                Double part = default(Double);
                                bSuccess &= Read(ref part, null);
                                ioArray.Add(part);
                            }
                            PopNode();
                        }
                    }
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read Double array at '{0}'.", inKey);

                return bSuccess;
            }

            if (ioArray != null)
            {
                AddPushNode(inKey, new JSONArray());
                for (int i = 0; i < ioArray.Count; ++i)
                {
                    Double obj = ioArray[i];
                    Write(null, ref obj, null);
                }
                PopNode();
            }
            else
            {
                WriteNull(inKey);
            }

            return true;
        }

        /// <summary>
        /// Serializes a Double array.
        /// </summary>
        public bool Array(string inKey, ref Double[] ioArray)
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    if (MissingNode)
                    {
                        bSuccess = false;
                    }
                    else if (NullNode)
                    {
                        ioArray = null;
                    }
                    else
                    {
                        int maxIndex;
                        if (ioArray == null)
                            ioArray = new Double[maxIndex = Node.Count];
                        else
                        {
                            maxIndex = Math.Max(Node.Count, ioArray.Length);
                            if (maxIndex > ioArray.Length)
                                System.Array.Resize(ref ioArray, maxIndex);
                        }

                        for (int i = 0; i < maxIndex; ++i)
                        {
                            PushNode(Node[i]);
                            Read(ref ioArray[i], null);
                            PopNode();
                        }
                        for (int i = maxIndex; i < ioArray.Length; ++i)
                        {
                            ioArray[i] = default(Double);
                        }
                    }
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read Double array at '{0}'.", inKey);

                return bSuccess;
            }

            if (ioArray != null)
            {
                AddPushNode(inKey, new JSONArray());
                for (int i = 0; i < ioArray.Length; ++i)
                {
                    Double obj = ioArray[i];
                    Write(null, ref obj, null);
                }
                PopNode();
            }
            else
            {
                WriteNull(inKey);
            }

            return true;
        }

        /// <summary>
        /// Serializes a string/Double dictionary.
        /// </summary>
        public bool Map(string inKey, ref Dictionary<string, Double> ioMap)
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    if (MissingNode)
                    {
                        bSuccess = false;
                    }
                    else if (NullNode)
                    {
                        if (ioMap != null)
                            ioMap.Clear();
                        ioMap = null;
                    }
                    else
                    {
                        if (ioMap == null)
                            ioMap = new Dictionary<string, Double>(Node.AsObject.Count);
                        else
                            ioMap.Clear();

                        foreach (var keyValue in Node.AsObject)
                        {
                            string key = keyValue.Key;
                            PushNode(keyValue.Value);
                            {
                                Double part = default(Double);
                                bSuccess &= Read(ref part, null);
                                ioMap.Add(key, part);
                            }
                            PopNode();
                        }
                    }
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read Double map at '{0}'.", inKey);

                return bSuccess;
            }

            if (ioMap != null)
            {
                AddPushNode(inKey, new JSONClass());
                foreach (var keyValue in ioMap)
                {
                    Double obj = keyValue.Value;
                    Write(keyValue.Key, ref obj, null);
                }
                PopNode();
            }
            else
            {
                WriteNull(inKey);
            }

            return true;
        }
    }
}
