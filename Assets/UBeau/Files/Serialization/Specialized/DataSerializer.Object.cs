using System;
using System.Collections.Generic;
using SimpleJSON;
using UBeau;

namespace UBeau.Serialization
{
    public sealed partial class DataSerializer
    {
        #region Reading and Writing Routines

        // Reads the T value from the current node.
        private bool ReadObject<T>(ref T ioData) where T : ISerializedData
        {
            if (MissingNode)
            {
                return false;
            }
            else if (NullNode)
            {
                if (ioData != null)
                    ioData.Reset();
                ioData = default(T);
                return true;
            }

            Type newType = typeof(T);
            if (Node[TYPE] != null)
                newType = Type.GetType(Node[TYPE].Value);

            if (ioData == null)
                ioData = (T)Instantiate(newType, this);
            else if (ioData.GetType().TypeHandle.Value != newType.TypeHandle.Value)
            {
                ioData.Reset();
                ioData = (T)Instantiate(newType, this);
            }

            return ioData.Serialize(this);
        }

        // Writes the T value to the current node.
        private bool WriteObject<T>(string inKey, ref T ioData) where T : ISerializedData
        {
            if (ioData != null)
            {
                AddPushNode(inKey, new JSONClass());
                if (typeof(T).TypeHandle.Value != ioData.GetType().TypeHandle.Value)
                    Node[TYPE].Value = ioData.GetType().AssemblyQualifiedName;
                ioData.Serialize(this);
                PopNode();
            }
            else
            {
                WriteNull(inKey);
            }
            return true;
        }

        #endregion

        /// <summary>
        /// Serializes the T value.
        /// </summary>
        public bool Object<T>(string inKey, ref T ioData) where T : ISerializedData
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    bSuccess = ReadObject(ref ioData);
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read value at '{0}'.", inKey);

                return bSuccess;
            }

            return WriteObject(inKey, ref ioData);
        }

        /// <summary>
        /// Serializes a list.
        /// </summary>
        public bool ObjectArray<T>(string inKey, ref List<T> ioArray) where T : ISerializedData
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    if (MissingNode)
                    {
                        bSuccess = false;
                    }
                    else if (NullNode)
                    {
                        if (ioArray != null)
                        {
                            for (int i = 0; i < ioArray.Count; ++i)
                            {
                                if (ioArray[i] != null)
                                    ioArray[i].Reset();
                            }
                            ioArray.Clear();
                        }
                        ioArray = null;
                    }
                    else
                    {
                        if (ioArray == null)
                            ioArray = new List<T>(Node.AsArray.Count);
                        else
                        {
                            for (int i = 0; i < ioArray.Count; ++i)
                            {
                                if (ioArray[i] != null)
                                    ioArray[i].Reset();
                            }
                            ioArray.Clear();
                        }

                        foreach (var node in Node.AsArray)
                        {
                            PushNode(node);
                            {
                                T part = default(T);
                                bSuccess &= ReadObject(ref part);
                                ioArray.Add(part);
                            }
                            PopNode();
                        }
                    }
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read T array at '{0}'.", inKey);

                return bSuccess;
            }

            if (ioArray != null)
            {
                AddPushNode(inKey, new JSONArray());
                for (int i = 0; i < ioArray.Count; ++i)
                {
                    T obj = ioArray[i];
                    WriteObject(null, ref obj);
                }
                PopNode();
            }
            else
            {
                WriteNull(inKey);
            }

            return true;
        }

        /// <summary>
        /// Serializes a T array.
        /// </summary>
        public bool ObjectArray<T>(string inKey, ref T[] ioArray) where T : ISerializedData
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    if (MissingNode)
                    {
                        bSuccess = false;
                    }
                    else if (NullNode)
                    {
                        if (ioArray != null)
                        {
                            for (int i = 0; i < ioArray.Length; ++i)
                            {
                                if (ioArray[i] != null)
                                    ioArray[i].Reset();
                            }
                        }
                        ioArray = null;
                    }
                    else
                    {
                        int maxIndex;
                        if (ioArray == null)
                            ioArray = new T[maxIndex = Node.Count];
                        else
                        {
                            for (int i = 0; i < ioArray.Length; ++i)
                            {
                                if (ioArray[i] != null)
                                    ioArray[i].Reset();
                                ioArray[i] = default(T);
                            }

                            maxIndex = Math.Max(Node.Count, ioArray.Length);
                            if (maxIndex > ioArray.Length)
                                System.Array.Resize(ref ioArray, maxIndex);
                        }

                        for (int i = 0; i < maxIndex; ++i)
                        {
                            PushNode(Node[i]);
                            ReadObject(ref ioArray[i]);
                            PopNode();
                        }
                        for (int i = maxIndex; i < ioArray.Length; ++i)
                        {
                            ioArray[i] = default(T);
                        }
                    }
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read T array at '{0}'.", inKey);

                return bSuccess;
            }

            if (ioArray != null)
            {
                AddPushNode(inKey, new JSONArray());
                for (int i = 0; i < ioArray.Length; ++i)
                {
                    T obj = ioArray[i];
                    WriteObject(null, ref obj);
                }
                PopNode();
            }
            else
            {
                WriteNull(inKey);
            }

            return true;
        }

        /// <summary>
        /// Serializes a string/T dictionary.
        /// </summary>
        public bool ObjectMap<T>(string inKey, ref Dictionary<string, T> ioMap) where T : ISerializedData
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    if (MissingNode)
                    {
                        bSuccess = false;
                    }
                    else if (NullNode)
                    {
                        if (ioMap != null)
                        {
                            foreach (T obj in ioMap.Values)
                            {
                                if (obj != null)
                                    obj.Reset();
                            }
                            ioMap.Clear();
                        }
                        ioMap = null;
                    }
                    else
                    {
                        if (ioMap == null)
                            ioMap = new Dictionary<string, T>(Node.AsObject.Count);
                        else
                        {
                            foreach (T obj in ioMap.Values)
                            {
                                if (obj != null)
                                    obj.Reset();
                            }
                            ioMap.Clear();
                        }

                        foreach (var keyValue in Node.AsObject)
                        {
                            string key = keyValue.Key;
                            PushNode(keyValue.Value);
                            {
                                T part = default(T);
                                bSuccess &= ReadObject(ref part);
                                ioMap.Add(key, part);
                            }
                            PopNode();
                        }
                    }
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read T map at '{0}'.", inKey);

                return bSuccess;
            }

            if (ioMap != null)
            {
                AddPushNode(inKey, new JSONClass());
                foreach (var keyValue in ioMap)
                {
                    T obj = keyValue.Value;
                    WriteObject(keyValue.Key, ref obj);
                }
                PopNode();
            }
            else
            {
                WriteNull(inKey);
            }

            return true;
        }
    }
}
