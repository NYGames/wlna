using System;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;
using UBeau;

namespace UBeau.Serialization
{
    public sealed partial class DataSerializer
    {
        #region Reading and Writing Routines

        // Reads the Vector2 value from the current node.
        private bool Read(ref Vector2 ioData, Vector2? inDefault)
        {
            if (MissingNode)
            {
                if (inDefault.HasValue)
                {
                    ioData = inDefault.Value;
                    return true;
                }
                else
                    return false;
            }
            else if (NullNode)
            {
                if (inDefault.HasValue)
                    ioData = inDefault.Value;
                else
                    ioData = default(Vector2);
                return true;
            }

            JSONArray array = Node.AsArray;
            if (array == null || array.Count < 2)
                return false;

            ioData.x = array[0].AsFloat;
            ioData.y = array[1].AsFloat;

            return true;
        }

        // Writes the Vector2 value to the current node.
        private bool Write(string inKey, ref Vector2 ioData, Vector2? inDefault)
        {
            if (!inDefault.HasValue || ioData != inDefault.Value)
            {
                AddPushNode(inKey, new JSONArray());
                Node[0].AsFloat = ioData.x;
                Node[1].AsFloat = ioData.y;
                PopNode();
            }
            else
            {
                if (string.IsNullOrEmpty(inKey))
                    WriteNull();
            }
            return true;
        }

        #endregion

        /// <summary>
        /// Serializes a Vector2 value.
        /// </summary>
        public bool Serialize(string inKey, ref Vector2 ioData, Vector2? inDefault = null)
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    bSuccess = Read(ref ioData, inDefault);
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read Vector2 value at '{0}'.", inKey);

                return bSuccess;
            }

            return Write(inKey, ref ioData, inDefault);
        }

        /// <summary>
        /// Serializes a Vector2 list.
        /// </summary>
        public bool Array(string inKey, ref List<Vector2> ioArray)
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    if (MissingNode)
                    {
                        bSuccess = false;
                    }
                    else if (NullNode)
                    {
                        if (ioArray != null)
                            ioArray.Clear();
                        ioArray = null;
                    }
                    else
                    {
                        if (ioArray == null)
                            ioArray = new List<Vector2>(Node.AsArray.Count);
						else
							ioArray.Clear();

                        foreach (var node in Node.AsArray)
                        {
                            PushNode(node);
                            {
                                Vector2 part = default(Vector2);
                                bSuccess &= Read(ref part, null);
                                ioArray.Add(part);
                            }
                            PopNode();
                        }
                    }
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read Vector2 array at '{0}'.", inKey);

                return bSuccess;
            }

            if (ioArray != null)
            {
                AddPushNode(inKey, new JSONArray());
                for (int i = 0; i < ioArray.Count; ++i)
                {
                    Vector2 obj = ioArray[i];
                    Write(null, ref obj, null);
                }
                PopNode();
            }
            else
            {
                WriteNull(inKey);
            }

            return true;
        }

        /// <summary>
        /// Serializes a Vector2 array.
        /// </summary>
        public bool Array(string inKey, ref Vector2[] ioArray)
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    if (MissingNode)
                    {
                        bSuccess = false;
                    }
                    else if (NullNode)
                    {
                        ioArray = null;
                    }
                    else
                    {
                        int maxIndex;
                        if (ioArray == null)
                            ioArray = new Vector2[maxIndex = Node.Count];
                        else
                        {
                            maxIndex = Math.Max(Node.Count, ioArray.Length);
                            if (maxIndex > ioArray.Length)
                                System.Array.Resize(ref ioArray, maxIndex);
                        }

                        for (int i = 0; i < maxIndex; ++i)
                        {
                            PushNode(Node[i]);
                            Read(ref ioArray[i], null);
                            PopNode();
                        }
                        for (int i = maxIndex; i < ioArray.Length; ++i)
                        {
                            ioArray[i] = default(Vector2);
                        }
                    }
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read Vector2 array at '{0}'.", inKey);

                return bSuccess;
            }

            if (ioArray != null)
            {
                AddPushNode(inKey, new JSONArray());
                for (int i = 0; i < ioArray.Length; ++i)
                {
                    Vector2 obj = ioArray[i];
                    Write(null, ref obj, null);
                }
                PopNode();
            }
            else
            {
                WriteNull(inKey);
            }

            return true;
        }

        /// <summary>
        /// Serializes a string/Vector2 dictionary.
        /// </summary>
        public bool Map(string inKey, ref Dictionary<string, Vector2> ioMap)
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    if (MissingNode)
                    {
                        bSuccess = false;
                    }
                    else if (NullNode)
                    {
                        if (ioMap != null)
                            ioMap.Clear();
                        ioMap = null;
                    }
                    else
                    {
                        if (ioMap == null)
                            ioMap = new Dictionary<string, Vector2>(Node.AsObject.Count);
                        else
                            ioMap.Clear();

                        foreach (var keyValue in Node.AsObject)
                        {
                            string key = keyValue.Key;
                            PushNode(keyValue.Value);
                            {
                                Vector2 part = default(Vector2);
                                bSuccess &= Read(ref part, null);
                                ioMap.Add(key, part);
                            }
                            PopNode();
                        }
                    }
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read Vector2 map at '{0}'.", inKey);

                return bSuccess;
            }

            if (ioMap != null)
            {
                AddPushNode(inKey, new JSONClass());
                foreach (var keyValue in ioMap)
                {
                    Vector2 obj = keyValue.Value;
                    Write(keyValue.Key, ref obj, null);
                }
                PopNode();
            }
            else
            {
                WriteNull(inKey);
            }

            return true;
        }
    }
}
