using System;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;
using UBeau;

namespace UBeau.Serialization
{
    public sealed partial class DataSerializer
    {
        #region Reading and Writing Routines

        // Reads the Vector3 value from the current node.
        private bool Read(ref Vector3 ioData, Vector3? inDefault)
        {
            if (MissingNode)
            {
                if (inDefault.HasValue)
                {
                    ioData = inDefault.Value;
                    return true;
                }
                else
                    return false;
            }
            else if (NullNode)
            {
                if (inDefault.HasValue)
                    ioData = inDefault.Value;
                else
                    ioData = default(Vector3);
                return true;
            }

            JSONArray array = Node.AsArray;
            if (array == null || array.Count < 2)
                return false;

            ioData.x = array[0].AsFloat;
            ioData.y = array[1].AsFloat;
            ioData.z = array.Count >= 3 ? array[2].AsFloat : 0;

            return true;
        }

        // Writes the Vector3 value to the current node.
        private bool Write(string inKey, ref Vector3 ioData, Vector3? inDefault)
        {
            if (!inDefault.HasValue || ioData != inDefault.Value)
            {
                AddPushNode(inKey, new JSONArray());
                Node[0].AsFloat = ioData.x;
                Node[1].AsFloat = ioData.y;
                Node[2].AsFloat = ioData.z;
                PopNode();
            }
            else
            {
                if (string.IsNullOrEmpty(inKey))
                    WriteNull();
            }
            return true;
        }

        #endregion

        /// <summary>
        /// Serializes a Vector3 value.
        /// </summary>
        public bool Serialize(string inKey, ref Vector3 ioData, Vector3? inDefault = null)
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    bSuccess = Read(ref ioData, inDefault);
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read Vector3 value at '{0}'.", inKey);

                return bSuccess;
            }

            return Write(inKey, ref ioData, inDefault);
        }

        /// <summary>
        /// Serializes a Vector3 list.
        /// </summary>
        public bool Array(string inKey, ref List<Vector3> ioArray)
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    if (MissingNode)
                    {
                        bSuccess = false;
                    }
                    else if (NullNode)
                    {
                        if (ioArray != null)
                            ioArray.Clear();
                        ioArray = null;
                    }
                    else
                    {
                        if (ioArray == null)
                            ioArray = new List<Vector3>(Node.AsArray.Count);
						else
							ioArray.Clear();

                        foreach (var node in Node.AsArray)
                        {
                            PushNode(node);
                            {
                                Vector3 part = default(Vector3);
                                bSuccess &= Read(ref part, null);
                                ioArray.Add(part);
                            }
                            PopNode();
                        }
                    }
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read Vector3 array at '{0}'.", inKey);

                return bSuccess;
            }

            if (ioArray != null)
            {
                AddPushNode(inKey, new JSONArray());
                for (int i = 0; i < ioArray.Count; ++i)
                {
                    Vector3 obj = ioArray[i];
                    Write(null, ref obj, null);
                }
                PopNode();
            }
            else
            {
                WriteNull(inKey);
            }

            return true;
        }

        /// <summary>
        /// Serializes a Vector3 array.
        /// </summary>
        public bool Array(string inKey, ref Vector3[] ioArray)
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    if (MissingNode)
                    {
                        bSuccess = false;
                    }
                    else if (NullNode)
                    {
                        ioArray = null;
                    }
                    else
                    {
                        int maxIndex;
                        if (ioArray == null)
                            ioArray = new Vector3[maxIndex = Node.Count];
                        else
                        {
                            maxIndex = Math.Max(Node.Count, ioArray.Length);
                            if (maxIndex > ioArray.Length)
                                System.Array.Resize(ref ioArray, maxIndex);
                        }

                        for (int i = 0; i < maxIndex; ++i)
                        {
                            PushNode(Node[i]);
                            Read(ref ioArray[i], null);
                            PopNode();
                        }
                        for (int i = maxIndex; i < ioArray.Length; ++i)
                        {
                            ioArray[i] = default(Vector3);
                        }
                    }
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read Vector3 array at '{0}'.", inKey);

                return bSuccess;
            }

            if (ioArray != null)
            {
                AddPushNode(inKey, new JSONArray());
                for (int i = 0; i < ioArray.Length; ++i)
                {
                    Vector3 obj = ioArray[i];
                    Write(null, ref obj, null);
                }
                PopNode();
            }
            else
            {
                WriteNull(inKey);
            }

            return true;
        }

        /// <summary>
        /// Serializes a string/Vector3 dictionary.
        /// </summary>
        public bool Map(string inKey, ref Dictionary<string, Vector3> ioMap)
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    if (MissingNode)
                    {
                        bSuccess = false;
                    }
                    else if (NullNode)
                    {
                        if (ioMap != null)
                            ioMap.Clear();
                        ioMap = null;
                    }
                    else
                    {
                        if (ioMap == null)
                            ioMap = new Dictionary<string, Vector3>(Node.AsObject.Count);
                        else
                            ioMap.Clear();

                        foreach (var keyValue in Node.AsObject)
                        {
                            string key = keyValue.Key;
                            PushNode(keyValue.Value);
                            {
                                Vector3 part = default(Vector3);
                                bSuccess &= Read(ref part, null);
                                ioMap.Add(key, part);
                            }
                            PopNode();
                        }
                    }
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read Vector3 map at '{0}'.", inKey);

                return bSuccess;
            }

            if (ioMap != null)
            {
                AddPushNode(inKey, new JSONClass());
                foreach (var keyValue in ioMap)
                {
                    Vector3 obj = keyValue.Value;
                    Write(keyValue.Key, ref obj, null);
                }
                PopNode();
            }
            else
            {
                WriteNull(inKey);
            }

            return true;
        }
    }
}
