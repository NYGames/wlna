using System;
using System.Collections.Generic;
using SimpleJSON;
using UBeau;

namespace UBeau.Serialization
{
    public sealed partial class DataSerializer
    {
        #region Reading and Writing Routines

        // Reads the T value from the current node.
        private bool ReadEnum<T>(ref T ioData, T? inDefault) where T : struct, IConvertible, IFormattable, IComparable
        {
            if (MissingNode)
            {
                if (inDefault.HasValue)
                {
                    ioData = inDefault.Value;
                    return true;
                }
                else
                    return false;
            }
            else if (NullNode)
            {
                if (inDefault.HasValue)
                    ioData = inDefault.Value;
                else
                    ioData = default(T);
                return true;
            }

            try
            {
                ioData = (T)System.Enum.Parse(typeof(T), Node.Value);
                return true;
            }
            catch(Exception e)
            {
                Log.Error("Unable to parse enum ({0}) value '{1}': {2}", typeof(T).Name, Node.Value, e.Message);
                return false;
            }
        }

        // Writes the T value to the current node.
        private bool WriteEnum<T>(string inKey, ref T ioData, T? inDefault) where T : struct, IConvertible, IFormattable, IComparable
        {
            if (!inDefault.HasValue || !ioData.Equals(inDefault.Value))
            {
                AddPushNode(inKey, new JSONData(System.Enum.GetName(typeof(T), ioData)));
                PopNode();
            }
            else
            {
                if (string.IsNullOrEmpty(inKey))
                    WriteNull();
            }
            return true;
        }

        #endregion

        /// <summary>
        /// Serializes a enum value.
        /// </summary>
        public bool Enum<T>(string inKey, ref T ioData, T? inDefault = null) where T : struct, IConvertible, IFormattable, IComparable
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    bSuccess = ReadEnum(ref ioData, inDefault);
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read enum at '{0}'.", inKey);

                return bSuccess;
            }

            return WriteEnum(inKey, ref ioData, inDefault);
        }

        /// <summary>
        /// Serializes a enum list.
        /// </summary>
        public bool EnumArray<T>(string inKey, ref List<T> ioArray) where T : struct, IConvertible, IFormattable, IComparable
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    if (MissingNode)
                    {
                        bSuccess = false;
                    }
                    else if (NullNode)
                    {
                        if (ioArray != null)
                            ioArray.Clear();
                        ioArray = null;
                    }
                    else
                    {
                        if (ioArray == null)
                            ioArray = new List<T>(Node.AsArray.Count);

                        foreach (var node in Node.AsArray)
                        {
                            PushNode(node);
                            {
                                T part = default(T);
                                bSuccess &= ReadEnum(ref part, null);
                                ioArray.Add(part);
                            }
                            PopNode();
                        }
                    }
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read enum array at '{0}'.", inKey);

                return bSuccess;
            }

            if (ioArray != null)
            {
                AddPushNode(inKey, new JSONArray());
                for (int i = 0; i < ioArray.Count; ++i)
                {
                    T obj = ioArray[i];
                    WriteEnum(null, ref obj, null);
                }
                PopNode();
            }
            else
            {
                WriteNull(inKey);
            }

            return true;
        }

        /// <summary>
        /// Serializes a enum array.
        /// </summary>
        public bool EnumArray<T>(string inKey, ref T[] ioArray) where T : struct, IConvertible, IFormattable, IComparable
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    if (MissingNode)
                    {
                        bSuccess = false;
                    }
                    else if (NullNode)
                    {
                        ioArray = null;
                    }
                    else
                    {
                        int maxIndex;
                        if (ioArray == null)
                            ioArray = new T[maxIndex = Node.Count];
                        else
                        {
                            maxIndex = Math.Max(Node.Count, ioArray.Length);
                            if (maxIndex > ioArray.Length)
                                System.Array.Resize(ref ioArray, maxIndex);
                        }

                        for (int i = 0; i < maxIndex; ++i)
                        {
                            PushNode(Node[i]);
                            ReadEnum(ref ioArray[i], null);
                            PopNode();
                        }
                        for (int i = maxIndex; i < ioArray.Length; ++i)
                        {
                            ioArray[i] = default(T);
                        }
                    }
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read enum array at '{0}'.", inKey);

                return bSuccess;
            }

            if (ioArray != null)
            {
                AddPushNode(inKey, new JSONArray());
                for (int i = 0; i < ioArray.Length; ++i)
                {
                    T obj = ioArray[i];
                    WriteEnum(null, ref obj, null);
                }
                PopNode();
            }
            else
            {
                WriteNull(inKey);
            }

            return true;
        }

        /// <summary>
        /// Serializes a string/enum dictionary.
        /// </summary>
        public bool EnumMap<T>(string inKey, ref Dictionary<string, T> ioMap) where T : struct, IConvertible, IFormattable, IComparable
        {
            if (IsReading)
            {
                bool bSuccess = true;

                PushNodeKey(inKey);
                {
                    if (MissingNode)
                    {
                        bSuccess = false;
                    }
                    else if (NullNode)
                    {
                        if (ioMap != null)
                            ioMap.Clear();
                        ioMap = null;
                    }
                    else
                    {
                        if (ioMap == null)
                            ioMap = new Dictionary<string, T>(Node.AsObject.Count);
                        else
                            ioMap.Clear();

                        foreach (var keyValue in Node.AsObject)
                        {
                            string key = keyValue.Key;
                            PushNode(keyValue.Value);
                            {
                                T part = default(T);
                                bSuccess &= ReadEnum(ref part, null);
                                ioMap.Add(key, part);
                            }
                            PopNode();
                        }
                    }
                }
                PopNode();

                if (!bSuccess)
                    Log.Warn("Unable to read enum map at '{0}'.", inKey);

                return bSuccess;
            }

            if (ioMap != null)
            {
                AddPushNode(inKey, new JSONClass());
                foreach (var keyValue in ioMap)
                {
                    T obj = keyValue.Value;
                    WriteEnum(keyValue.Key, ref obj, null);
                }
                PopNode();
            }
            else
            {
                WriteNull(inKey);
            }

            return true;
        }
    }
}
