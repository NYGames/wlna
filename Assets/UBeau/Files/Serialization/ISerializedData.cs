﻿namespace UBeau.Serialization
{
    public interface ISerializedData
    {
        bool Serialize(DataSerializer inSerializer);
        void Reset();
    }
}
