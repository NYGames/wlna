﻿using SimpleJSON;
using System;
using UnityEngine;
using UBeau;

namespace UBeau.Serialization
{
    public sealed partial class DataSerializer
    {
        /// <summary>
        /// Writes the given object to a JSON node.
        /// </summary>
        static public JSONNode WriteObject<T>(ref T inData) where T : ISerializedData
        {
            if (inData == null)
                return null;

            DataSerializer serializer = new DataSerializer(false, new JSONClass());
            if (typeof(T).TypeHandle.Value != inData.GetType().TypeHandle.Value)
                serializer.Node[TYPE] = inData.GetType().AssemblyQualifiedName;

            inData.Serialize(serializer);
            return serializer.Root;
        }

        /// <summary>
        /// Reads an object from a JSON node.
        /// Returns if successful.
        /// </summary>
        static public bool ReadObject<T>(ref T ioData, JSONNode inNode, bool inbResetIfNull = false) where T : ISerializedData
        {
            DataSerializer serializer = new DataSerializer(true, inNode);

            if (inNode == null)
            {
                if (inbResetIfNull)
                {
                    if (ioData == null)
                        ioData = (T)Instantiate(typeof(T), null);
                    ioData.Reset();
                    return true;
                }

                Log.Error("Unable to load data {0}.", typeof(T).GetGenericName());
                return false;
            }

            Type newType = typeof(T);
            if (serializer.Node[TYPE] != null)
                newType = Type.GetType(serializer.Node[TYPE].Value);

            if (ioData == null)
                ioData = (T)Instantiate(newType, null);
            else if (ioData.GetType().TypeHandle.Value != newType.TypeHandle.Value)
            {
                ioData.Reset();
                ioData = (T)Instantiate(newType, null);
            }

            bool bSuccess = ioData.Serialize(serializer);
            if (!bSuccess)
                Log.Error("Problems loading data {0}.", typeof(T).GetGenericName());

            return bSuccess;
        }

        /// <summary>
        /// Reads an object from a JSON node.
        /// Asserts if it fails.
        /// </summary>
        static public T Read<T>(JSONNode inNode, bool inbResetIfNull = false) where T : ISerializedData
        {
            T obj = default(T);
            bool bSuccess = ReadObject(ref obj, inNode, inbResetIfNull);
            Assert.True(bSuccess, "Unable to load data '{0}'!", typeof(T).GetGenericName());
            return obj;
        }

        /// <summary>
        /// Reads an object from a JSON string.
        /// Returns if successful.
        /// </summary>
        static public bool ReadString<T>(ref T ioData, string inJSON, bool inbResetIfNull = false) where T : ISerializedData
        {
            JSONNode node = string.IsNullOrEmpty(inJSON) ? null : JSON.Parse(inJSON);
            return ReadObject<T>(ref ioData, node, inbResetIfNull);
        }

        /// <summary>
        /// Reads an object from a JSON string.
        /// Asserts if it fails.
        /// </summary>
        static public T ReadString<T>(string inJSON, bool inbResetIfNull = false) where T : ISerializedData
        {
            T obj = default(T);
            bool bSuccess = ReadString(ref obj, inJSON, inbResetIfNull);
            Assert.True(bSuccess, "Unable to load data '{0}'!", typeof(T).GetGenericName());
            return obj;
        }

        /// <summary>
        /// Reads an object from a file.
        /// Returns if successful.
        /// </summary>
        static public bool ReadTextAsset<T>(ref T ioData, TextAsset inFile, bool inbResetIfNull = false) where T : ISerializedData
        {
            JSONNode node = inFile == null || string.IsNullOrEmpty(inFile.text) ? null : JSON.Parse(inFile.text);
            return ReadObject<T>(ref ioData, node, inbResetIfNull);
        }

        /// <summary>
        /// Reads an object from a file.
        /// Asserts if it fails.
        /// </summary>
        static public T ReadTextAsset<T>(TextAsset inFile, bool inbResetIfNull = false) where T : ISerializedData
        {
            T obj = default(T);
            bool bSuccess = ReadTextAsset(ref obj, inFile, inbResetIfNull);
            Assert.True(bSuccess, "Unable to load data '{0}'!", typeof(T).GetGenericName());
            return obj;
        }
    }
}
