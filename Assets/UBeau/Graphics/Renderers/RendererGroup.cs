﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UBeau.Graphics
{
    /// <summary>
    /// Scales the alpha of all child RendererColors.
    /// </summary>
    [DisallowMultipleComponent]
    public sealed class RendererGroup : MonoBehaviour
    {
        #region Inspector

        [SerializeField, Range(0, 1)]
        private float m_Alpha = 1.0f;

        #endregion

        [NonSerialized]
        private List<RendererColor> m_Children = new List<RendererColor>();

        /// <summary>
        /// Alpha scaling to apply to all
        /// child RendererColors.
        /// </summary>
        public float Alpha
        {
            get { return m_Alpha; }
            set
            {
                if (m_Alpha != value)
                {
                    m_Alpha = value;
                    SetDirty();
                }
            }
        }

        /// <summary>
        /// Refreshes the list of RendererColors in child objects.
        /// </summary>
        public void RefreshChildren()
        {
            using (PooledSet<RendererColor> old = PooledSet<RendererColor>.Create(m_Children))
            {
                m_Children.Clear();
                GetComponentsInChildren<RendererColor>(true, m_Children);

                for(int i = m_Children.Count - 1; i >= 0; --i)
                {
                    if (old.Contains(m_Children[i]))
                        old.Remove(m_Children[i]);
                    else
                        m_Children[i].AddGroup(this);
                }

                foreach (var renderer in old)
                    renderer.RemoveGroup(this);
            }
        }

        // Sets all child RendererColors to dirty.
        private void SetDirty()
        {
            for (int i = m_Children.Count - 1; i >= 0; --i)
                m_Children[i].SetDirty();
        }

        #region Unity Events

        private void Awake()
        {
            RefreshChildren();
        }

        private void OnDestroy()
        {
            for (int i = m_Children.Count - 1; i >= 0; --i)
                m_Children[i].RemoveGroup(this);
            m_Children.Clear();
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            RefreshChildren();
            SetDirty();
        }
#endif

#endregion
    }
}
