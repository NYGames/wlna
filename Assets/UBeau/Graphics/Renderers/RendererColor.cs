﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UBeau.Graphics
{
    /// <summary>
    /// Modifies the color of a Renderer or SpriteRenderer.
    /// Also provides integration with RendererGroup.
    /// </summary>
    [DisallowMultipleComponent, RequireComponent(typeof(Renderer)), ExecuteInEditMode]
    public sealed class RendererColor : MonoBehaviour
    {
        #region Types

        private enum Mode
        {
            None,
            Renderer,
            SpriteRenderer,
            Invalid
        }

        #endregion

        #region Inspector

        [SerializeField]
        private Color m_Color;

        [SerializeField, Range(0, 1)]
        private float m_AlphaScale = 1.0f;

        #endregion

        [NonSerialized]
        private MaterialPropertyBlock m_Block;
        [NonSerialized]
        private Renderer m_Renderer;
        [NonSerialized]
        private SpriteRenderer m_SpriteRenderer;

        [NonSerialized]
        private float m_GroupAlpha = 1.0f;
        [NonSerialized]
        private List<RendererGroup> m_Groups;

        [NonSerialized]
        private Mode m_Mode;
        [NonSerialized]
        private bool m_ColorDirty;
        [NonSerialized]
        private bool m_GroupDirty;

        #region Properties

        /// <summary>
        /// Current color of the renderer.
        /// </summary>
        public Color Color
        {
            get { return m_Color; }
            set
            {
                if (m_Color != value)
                {
                    m_Color = value;
                    m_ColorDirty = true;
                }
            }
        }

        /// <summary>
        /// Alpha component of the color.
        /// </summary>
        public float Alpha
        {
            get { return m_Color.a; }
            set
            {
                if (m_Color.a != value)
                {
                    m_Color.a = value;
                    m_ColorDirty = true;
                }
            }
        }

        /// <summary>
        /// Scaling to apply to alpha.
        /// </summary>
        public float AlphaScale
        {
            get { return m_AlphaScale; }
            set
            {
                if (m_AlphaScale != value)
                {
                    m_AlphaScale = value;
                    m_ColorDirty = true;
                }
            }
        }

        #endregion

        /// <summary>
        /// Adds the RendererColor to the given group.
        /// </summary>
        public void AddGroup(RendererGroup inGroup)
        {
            if (m_Groups == null)
                m_Groups = new List<RendererGroup>();
            m_Groups.Add(inGroup);
            SetDirty();
        }

        /// <summary>
        /// Removes the RendererColor from the given group.
        /// </summary>
        public void RemoveGroup(RendererGroup inGroup)
        {
            if (m_Groups != null && m_Groups.Remove(inGroup))
                SetDirty();
        }

        /// <summary>
        /// Sets this object as dirty.
        /// The overall alpha scaling will be calculated
        /// before the next render.
        /// </summary>
        public void SetDirty()
        {
            m_GroupDirty = true;
        }

        // Finds the appropriate renderer for the 
        private void FindRenderer(bool inbGetColor)
        {
            if (m_Mode != Mode.None)
                return;

            if (m_Block == null)
                m_Block = new MaterialPropertyBlock();

            m_Renderer = GetComponent<Renderer>();
            m_SpriteRenderer = m_Renderer as SpriteRenderer;

            if (m_SpriteRenderer != null)
            {
                m_Mode = Mode.SpriteRenderer;
                if (inbGetColor)
                    m_Color = m_SpriteRenderer.color;
            }
            else if (m_Renderer != null)
            {
                m_Mode = Mode.Renderer;
                if (inbGetColor)
                {
                    m_Renderer.GetPropertyBlock(m_Block);
                    m_Color = m_Block.GetVector("_Color");
                    if (m_Color.a == 0 && m_Color.r == 0 && m_Color.g == 0 && m_Color.b == 0)
                    {
                        m_Color = Color.white;
                        m_ColorDirty = true;
                    }
                }
            }
        }

        // Updates the alpha from the group.
        private void UpdateGroupAlpha()
        {
            m_GroupDirty = false;
            m_GroupAlpha = 1.0f;
            if (m_Groups == null)
                return;

            for (int i = m_Groups.Count - 1; i >= 0 && m_GroupAlpha > 0; --i)
                m_GroupAlpha *= m_Groups[i].Alpha;
        }

        // Applies the color as either a MaterialPropertyBlock
        // or directly to the SpriteRenderer
        private void Apply()
        {
            FindRenderer(false);
            m_ColorDirty = m_GroupDirty = false;

            if (m_Mode == Mode.Invalid)
                return;

            Color finalColor = m_Color;
            finalColor.a *= m_AlphaScale * m_GroupAlpha;

            switch (m_Mode)
            {
                case Mode.Renderer:
                    {
                        m_Renderer.GetPropertyBlock(m_Block);
                        m_Block.SetColor("_Color", finalColor);
                        m_Renderer.SetPropertyBlock(m_Block);
                        break;
                    }
                case Mode.SpriteRenderer:
                    {
                        m_SpriteRenderer.color = finalColor;
                        break;
                    }
            }
        }

        #region Unity Events

        private void Awake()
        {
            Apply();
        }

        private void Reset()
        {
            FindRenderer(true);
        }

        private void OnValidate()
        {
            Apply();
        }

        private void OnWillRenderObject()
        {
            if (m_GroupDirty)
            {
                UpdateGroupAlpha();
                Apply();
                return;
            }

            if (m_ColorDirty)
            {
                Apply();
            }
        }

        #endregion
    }
}
