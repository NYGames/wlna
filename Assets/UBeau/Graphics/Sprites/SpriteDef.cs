﻿using System;
using System.Collections.Generic;
using UBeau.Gameplay;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace UBeau.Graphics
{
    /// <summary>
    /// Frame-based sprite animation.
    /// </summary>
    public sealed class SpriteDef : DataAsset
    {
        /// <summary>
        /// List of all animations.
        /// </summary>
        public List<Animation> Animations = new List<Animation>();

        /// <summary>
        /// Name of the first animation.
        /// </summary>
        public string StartID = string.Empty;

        /// <summary>
        /// Returns the Animation with the given ID.
        /// </summary>
        public Animation this[string inID]
        {
            get
            {
                for (int i = 0; i < Animations.Count; ++i)
                    if (Animations[i].ID == inID)
                        return Animations[i];
                return null;
            }
        }

        /// <summary>
        /// Creates a new Animation.
        /// </summary>
        public Animation CreateAnim()
        {
            Animation anim = new Animation();
            Animations.Add(anim);
            return anim;
        }

        /// <summary>
        /// Returns the Animation with the given ID.
        /// </summary>
        public Animation FindAnim(string inID, out int outIndex)
        {
            for (int i = 0; i < Animations.Count; ++i)
            {
                if (Animations[i].ID == inID)
                {
                    outIndex = i;
                    return Animations[i];
                }
            }
            outIndex = -1;
            return null;
        }

        /// <summary>
        /// Animation to start with.
        /// </summary>
        public Animation StartingAnim
        {
            get { return this[StartID]; }
        }

        /// <summary>
        /// Sequence of frames.
        /// </summary>
        [Serializable]
        public sealed class Animation
        {
            /// <summary>
            /// Name of the animation.
            /// </summary>
            public string ID;

            /// <summary>
            /// List of frames to play.
            /// </summary>
            public Frame[] Frames = new Frame[0];

            /// <summary>
            /// ID of the next animation to play.
            /// </summary>
            public string NextID;

            /// <summary>
            /// Type of playback.
            /// </summary>
            public AnimType Type = AnimType.Single;
        }

        /// <summary>
        /// One frame of an animation.
        /// </summary>
        [Serializable]
        public struct Frame
        {
            /// <summary>
            /// Sprite to display.
            /// </summary>
            public Sprite Sprite;

            /// <summary>
            /// Seconds to display.
            /// </summary>
            public float Delay;

            /// <summary>
            /// Message to dispatch when triggered.
            /// </summary>
            [MsgRef]
            public MsgType Event;

            /// <summary>
            /// How to dispatch the message.
            /// </summary>
            public MsgTarget EventTarget;

            //TODO(Alex): Sound to play?
        }

        /// <summary>
        /// Determines behavior when the animation ends.
        /// </summary>
        public enum AnimType : byte
        {
            /// <summary>
            /// Plays once.
            /// </summary>
            Single,

            /// <summary>
            /// Plays once and transitions into another animation.
            /// </summary>
            Transition,

            /// <summary>
            /// Loops indefinitely.
            /// </summary>
            Loop,

            /// <summary>
            /// Loops with a random starting frame.
            /// </summary>
            LoopStartRandom
        }

        /// <summary>
        /// Target of a message.
        /// </summary>
        public enum MsgTarget : byte
        {
            /// <summary>
            /// Send the message to the assigned Messenger.
            /// </summary>
            Self,

            /// <summary>
            /// Broadcast the message from the assigned Messenger.
            /// </summary>
            Broadcast
        }

#if UNITY_EDITOR
        [MenuItem("Assets/Create/UBeau/Sprite Definition")]
        static private void CreateAsset()
        {
            var def = CreateInstance<SpriteDef>();
            ProjectWindowUtil.CreateAsset(def, "NewSprite.asset");
            EditorUtility.SetDirty(def);
            Selection.activeObject = def;
        }
#endif
    }
}
