﻿using BeauRoutine;
using System.Collections;
using UBeau.Gameplay;
using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace UBeau.Graphics
{
    /// <summary>
    /// Runs a SpiteDef animation.
    /// </summary>
    public sealed class SpriteAnimator : MonoBehaviour
    {
        [MsgDefinition(MsgFlags.Immediate, "Sprite/Animation Started")]
        static public readonly MsgType MSG_ANIM_STARTED;

        [MsgDefinition(MsgFlags.Immediate, "Sprite/Animation Ended")]
        static public readonly MsgType MSG_ANIM_ENDED;

        #region Target

        /// <summary>
        /// Target to assign
        /// </summary>
        public abstract class TextureTarget : MonoBehaviour
        {
            public abstract void OnSpriteChanged(Sprite inNewSprite);
        }

        #endregion

        #region Inspector

        [SerializeField]
        private SpriteDef m_SpriteDefinition;

        [SerializeField]
        private TextureTarget m_TextureTarget;

        [SerializeField]
        private bool m_PlayOnAwake = true;

        [SerializeField]
        private string m_StartingAnimation = string.Empty;

        #endregion

        [NonSerialized]
        private Messenger m_Messenger;

        [NonSerialized]
        private SpriteDef.Animation m_CurrentAnimation;

        [NonSerialized]
        private SpriteDef.Frame m_CurrentFrame;

        private Routine m_UpdateLoop;
        private int m_CurrentFrameIndex = 0;
        private float m_CurrentFrameTime = 0.0f;
        private bool m_Playing;

        /// <summary>
        /// Current animation collection.
        /// </summary>
        public SpriteDef SpriteDef
        {
            get { return m_SpriteDefinition; }
        }

        /// <summary>
        /// Index of the currently displayed frame.
        /// Setting this will freeze the animation in place.
        /// </summary>
        public int FrameIndex
        {
            get { return m_CurrentFrameIndex; }
            set
            {
                m_Playing = false;
                if (m_CurrentFrameIndex != value)
                {
                    Assert.True(value >= 0 && value < m_CurrentAnimation.Frames.Length, "Frame {0} does not exist in animation {1}", value, m_CurrentAnimation.Frames.Length);
                    m_CurrentFrameIndex = value;
                    m_CurrentFrameTime = 0.0f;
                    VisitFrame();
                }
            }
        }

        /// <summary>
        /// ID of the currently displayed animation.
        /// </summary>
        public string Animation
        {
            get { return m_CurrentAnimation.ID; }
        }

        /// <summary>
        /// Output of the animation.
        /// </summary>
        public TextureTarget Output
        {
            get { return m_TextureTarget; }
        }

        /// <summary>
        /// Plays the animation with the given ID.
        /// </summary>
        public IEnumerator Play(string inAnimation, bool inbRestart = false)
        {
            if (!m_Playing || inAnimation != m_CurrentAnimation.ID || inbRestart)
            {
                m_Playing = true;
                m_CurrentFrameTime = 0.0f;
                SwitchAnimation(inAnimation);

                if (!m_UpdateLoop)
                    m_UpdateLoop.Replace(this, Animate());
            }

            return WaitToStop(inAnimation);
        }

        /// <summary>
        /// Stops any animations from playing.
        /// </summary>
        public void Stop()
        {
            m_Playing = false;
            m_UpdateLoop.Stop();
        }

        /// <summary>
        /// Assigns a new SpriteDefinition.
        /// </summary>
        /// <param name="inSprite">Sprite to switch to.</param>
        /// <param name="inOverrideStart">If not null, the animation to play on the new sprite.</param>
        public void AssignSprite(SpriteDef inSprite, string inOverrideStart = null)
        {
            Assert.NotNull(inSprite, "Provided SpriteDef is null!");
            if (inSprite != m_SpriteDefinition)
            {
                m_SpriteDefinition = inSprite;
                Play(string.IsNullOrEmpty(inOverrideStart) ? inSprite.StartID : inOverrideStart);
            }
        }

        #region Unity Events

        private void Awake()
        {
            m_Messenger = Messenger.Require(this);

            if (m_PlayOnAwake)
            {
                Play(string.IsNullOrEmpty(m_StartingAnimation) ? m_SpriteDefinition.StartID : m_StartingAnimation);
            }
        }

        #endregion

        #region Routines

        /// <summary>
        /// Waits for the Animator to either stop playing
        /// or exit the given animation.
        /// </summary>
        public IEnumerator WaitToStop(string inAnimation)
        {
            while (m_Playing && m_CurrentAnimation.ID == inAnimation)
                yield return null;
        }

        #endregion

        #region Internal

        // Update loop
        private IEnumerator Animate()
        {
            while (m_Playing)
            {
                m_CurrentFrameTime += Routine.DeltaTime;
                while (m_Playing && m_CurrentFrameTime >= m_CurrentFrame.Delay)
                {
                    m_CurrentFrameTime -= m_CurrentFrame.Delay;
                    m_Playing = AdvanceFrame() && m_Playing;
                    if (m_CurrentFrame.Delay <= 0)
                    {
                        m_CurrentFrameTime = 0.0f;
                        m_Playing = false;
                        yield break;
                    }
                }
                yield return null;
            }
        }

        // Advances to the next frame
        // Returns if playing should stop.
        private bool AdvanceFrame()
        {
            ++m_CurrentFrameIndex;
            if (m_CurrentFrameIndex >= m_CurrentAnimation.Frames.Length)
            {
                switch (m_CurrentAnimation.Type)
                {
                    case SpriteDef.AnimType.Loop:
                    case SpriteDef.AnimType.LoopStartRandom:
                        {
                            m_CurrentFrameIndex = 0;
                            VisitFrame();
                            return true;
                        }
                    case SpriteDef.AnimType.Single:
                        {
                            --m_CurrentFrameIndex;
                            m_CurrentFrameTime = 0.0f;
                            m_Messenger.Raise(MSG_ANIM_ENDED, this);
                            return false;
                        }
                    case SpriteDef.AnimType.Transition:
                        {
                            m_Messenger.Raise(MSG_ANIM_ENDED, this);
                            SwitchAnimation(m_CurrentAnimation.NextID);
                            return true;
                        }
                    default:
                        {
                            Assert.Fail("Unknown animation mode {0}!", m_CurrentAnimation.Type);
                            return false;
                        }
                }
            }
            else
            {
                VisitFrame();
                return true;
            }
        }

        // Switches to another animation
        private void SwitchAnimation(string inAnimation)
        {
            SpriteDef.Animation anim = m_SpriteDefinition[inAnimation];
            Assert.NotNull(anim, "Could not find animation with name {0}!", inAnimation);
            m_CurrentAnimation = anim;
            m_CurrentFrameIndex = (anim.Type == SpriteDef.AnimType.LoopStartRandom ? RNG.Instance.Int(0, anim.Frames.Length) : 0);

            m_Messenger.Raise(MSG_ANIM_STARTED, this);

            VisitFrame();
        }

        // Visits the current frame.
        private void VisitFrame()
        {
            m_CurrentFrame = m_CurrentAnimation.Frames[m_CurrentFrameIndex];

            m_TextureTarget.OnSpriteChanged(m_CurrentFrame.Sprite);

            if (m_CurrentFrame.Event != MsgType.Null)
            {
                switch (m_CurrentFrame.EventTarget)
                {
                    case SpriteDef.MsgTarget.Self:
                        m_Messenger.Raise(m_CurrentFrame.Event);
                        break;
                    case SpriteDef.MsgTarget.Broadcast:
                        m_Messenger.Broadcast(m_CurrentFrame.Event);
                        break;
                }
            }

            //TODO(Alex): Play the sound
        }

        #endregion

        #region Inspector

#if UNITY_EDITOR
        [CustomEditor(typeof(SpriteAnimator))]
        private class Inspector : Editor
        {
            public override void OnInspectorGUI()
            {
                RenderEditor();
            }

            private void AssignStartingSprite(SpriteAnimator inTarget)
            {
                if (inTarget.m_TextureTarget == null || inTarget.m_SpriteDefinition == null)
                    return;

                string animName = string.IsNullOrEmpty(inTarget.m_StartingAnimation) ? inTarget.m_SpriteDefinition.StartID : inTarget.m_StartingAnimation;
                SpriteDef.Animation anim = inTarget.m_SpriteDefinition[animName];
                if (anim == null || anim.Frames == null || anim.Frames.Length == 0)
                    return;
                inTarget.m_TextureTarget.OnSpriteChanged(anim.Frames[0].Sprite);
            }

            private void RenderEditor()
            {
                SpriteAnimator animator = (SpriteAnimator)target;

                EditorGUI.BeginChangeCheck();
                {
                    EditorGUILayout.BeginVertical();

                    {
                        SpriteDef newDef = (SpriteDef)EditorGUILayout.ObjectField("Sprite Definition", animator.m_SpriteDefinition, typeof(SpriteDef), true);
                        if (newDef != animator.m_SpriteDefinition)
                        {
                            animator.m_SpriteDefinition = newDef;
                            AssignStartingSprite(animator);
                        }
                    }
                    {
                        TextureTarget newTarget = (TextureTarget)EditorGUILayout.ObjectField("Output Target", animator.m_TextureTarget, typeof(TextureTarget), true);
                        if (newTarget != animator.m_TextureTarget)
                        {
                            animator.m_TextureTarget = newTarget;
                            AssignStartingSprite(animator);
                        }
                    }

                    if (animator.m_SpriteDefinition != null)
                    {
                        animator.m_PlayOnAwake = EditorGUILayout.Toggle("Play On Awake", animator.m_PlayOnAwake);
                        if (animator.m_PlayOnAwake)
                        {
                            string[] animationNames = new string[animator.m_SpriteDefinition.Animations.Count + 1];
                            animationNames[0] = "[Default: " + animator.m_SpriteDefinition.StartID + "]";
                            for (int i = 0; i < animator.m_SpriteDefinition.Animations.Count; ++i)
                            {
                                animationNames[i + 1] = animator.m_SpriteDefinition.Animations[i].ID;
                            }

                            int currentIndex = 0;
                            if (!string.IsNullOrEmpty(animator.m_StartingAnimation))
                                currentIndex = Array.IndexOf(animationNames, animator.m_StartingAnimation);

                            int nextIndex = EditorGUILayout.Popup("Starting Animation", currentIndex, animationNames);
                            if (currentIndex != nextIndex)
                            {
                                if (nextIndex > 0)
                                    animator.m_StartingAnimation = animationNames[nextIndex];
                                else
                                    animator.m_StartingAnimation = string.Empty;

                                AssignStartingSprite(animator);
                            }
                        }
                    }
                    else
                    {
                        EditorGUILayout.HelpBox("No Sprite Definition has been assigned!", MessageType.Error);
                    }

                    if (animator.m_TextureTarget == null)
                        EditorGUILayout.HelpBox("No Output Target has been assigned.", MessageType.Error);

                    EditorGUILayout.EndVertical();
                }
                if (EditorGUI.EndChangeCheck())
                    EditorUtility.SetDirty(animator);
            }
        }
#endif

        #endregion
    }
}
