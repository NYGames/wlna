﻿using UnityEngine;
using UnityEngine.UI;

namespace UBeau.Graphics
{
    [RequireComponent(typeof(Image))]
    public sealed class ImageTarget : SpriteAnimator.TextureTarget
    {
        private Image m_Renderer;

        public override void OnSpriteChanged(Sprite inNewSprite)
        {
            if (m_Renderer == null)
                m_Renderer = GetComponent<Image>();
            m_Renderer.sprite = inNewSprite;
        }
    }
}
