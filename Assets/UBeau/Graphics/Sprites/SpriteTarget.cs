﻿using UnityEngine;
using UnityEngine.UI;

namespace UBeau.Graphics
{
    [RequireComponent(typeof(SpriteRenderer))]
    public sealed class SpriteTarget : SpriteAnimator.TextureTarget
    {
        private SpriteRenderer m_Renderer;

        public override void OnSpriteChanged(Sprite inNewSprite)
        {
            if (m_Renderer == null)
                m_Renderer = GetComponent<SpriteRenderer>();
            m_Renderer.sprite = inNewSprite;
        }
    }
}
