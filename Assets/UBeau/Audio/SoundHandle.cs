﻿using System;
using System.Collections;
using UnityEngine;

namespace UBeau.Audio
{
    public struct SoundHandle : IEquatable<SoundHandle>
    {
        private const uint INDEX_MASK = 0xFFFF;
        private const uint COUNTER_MASK = 0xFFFF0000;
        private const byte COUNTER_SHIFT = 16;

        private uint m_Value;

        internal SoundHandle(uint inValue)
        {
            m_Value = inValue;
        }

        /// <summary>
        /// Default, empty routine.
        /// </summary>
        static public readonly SoundHandle Null = new SoundHandle(0);

        #region Status

        /// <summary>
        /// Returns an IEnumerator waiting for the current routine to complete.
        /// </summary>
        public IEnumerator Wait()
        {
            //TODO(Alex): Implement
            return null;
        }

        /// <summary>
        /// Returns if the current routine is active.
        /// </summary>
        public bool Exists()
        {
            //TODO(Alex): Implement
            return false;
        }

        #endregion

        #region Flow Control

        /// <summary>
        /// Pauses execution of this routine.
        /// </summary>
        public SoundHandle Pause()
        {
            //TODO(Alex): Implement
            return this;
        }

        /// <summary>
        /// Resumes execution of this routine.
        /// </summary>
        public SoundHandle Resume()
        {
            //TODO(Alex): Implement
            return this;
        }

        /// <summary>
        /// Stops execution and clears the handle.
        /// </summary>
        public SoundHandle Stop()
        {
            //TODO(Alex): Implement
            m_Value = 0;
            return Null;
        }

        #endregion

        #region Overrides

        public bool Equals(SoundHandle other)
        {
            return m_Value == other.m_Value;
        }

        public override bool Equals(object obj)
        {
            if (obj is SoundHandle)
                return Equals((SoundHandle)obj);
            return false;
        }

        public override int GetHashCode()
        {
            return (int)m_Value;
        }

        static public implicit operator bool(SoundHandle inHandle)
        {
            return inHandle.Exists();
        }

        static public bool operator ==(SoundHandle first, SoundHandle second)
        {
            return first.m_Value == second.m_Value;
        }

        static public bool operator !=(SoundHandle first, SoundHandle second)
        {
            return first.m_Value != second.m_Value;
        }

        #endregion
    }
}
