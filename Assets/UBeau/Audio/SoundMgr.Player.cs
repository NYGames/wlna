﻿using System.Collections;
using UnityEngine;

namespace UBeau.Audio
{
    public partial class SoundMgr : Singleton<SoundMgr>
    {
        private class Player
        {
            private ushort m_Index;
            private ushort m_Counter = 0;

            private byte m_Flags = 0;

            private const byte FLAG_PAUSED = 1 << 0;
            private const byte FLAG_DISPOSING = 1 << 1;
            private const byte FLAG_PLAYING = 1 << 2;
            private const byte FLAG_SYSTEM_PAUSED = 1 << 3;
            private const byte FLAG_LOOPING = 1 << 4;
            private const byte FLAG_POSITIONAL = 1 << 5;

            private Transform m_Transform;
            private AudioSource m_Source;
            private SoundMgr m_Manager;

            private SoundCategory m_Category;

            public float Volume;
            public float Pitch;

            public Player(ushort inIndex, SoundMgr inManager, AudioSource inSource)
            {
                m_Index = inIndex;

                m_Manager = inManager;
                m_Source = inSource;
                m_Transform = inSource.transform;

                m_Source.gameObject.SetActive(false);
            }

            public bool Paused
            {
                get { return (m_Flags & (FLAG_PAUSED | FLAG_DISPOSING)) > 0; }
            }

            public SoundCategory Category
            {
                get { return m_Category; }
            }

            public bool Looping
            {
                get { return (m_Flags & FLAG_LOOPING) > 0; }
            }

            public bool IsPositional
            {
                get { return (m_Flags & FLAG_POSITIONAL) > 0; }
            }

            public Vector3 Position
            {
                get { return m_Transform.position; }
                set { m_Transform.position = value; }
            }

            public float Duration
            {
                get { return m_Source.clip != null ? m_Source.clip.length : 0.0f; }
            }

            public float Time
            {
                get { return m_Source.time; }
            }

            public void Start(AudioClip inClip, SoundCategory inCategory, float inVolume, float inPitch, bool inbLoop, bool inbPositional, Vector3 inPosition)
            {
                //TODO(Alex): Implement
            }

            public void Pause()
            {
                //TODO(Alex): Implement
            }

            public void Resume()
            {
                //TODO(Alex): Implement
            }

            public void Stop()
            {
                //TODO(Alex): Implement
            }

            private void Dispose()
            {
                //TODO(Alex): Implement
            }

            public IEnumerator Wait()
            {
                //TODO(Alex): Implement
                return null;
            }

            public bool Update()
            {
                if ((m_Flags & (FLAG_SYSTEM_PAUSED | FLAG_PAUSED)) == 0 && !m_Source.isPlaying)
                    m_Flags |= FLAG_DISPOSING;

                if ((m_Flags & FLAG_DISPOSING) > 0)
                {
                    Dispose();
                    return false;
                }

                ApplySettings();
                UpdatePausedStatus();
                return true;
            }

            private void ApplySettings()
            {
                //TODO(Alex): Implement
            }

            private void UpdatePausedStatus()
            {
                //TODO(Alex): Implement
            }
        }
    }
}
