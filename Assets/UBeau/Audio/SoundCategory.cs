﻿namespace UBeau.Audio
{
    /// <summary>
    /// Categorizes sounds into groups.
    /// </summary>
    public enum SoundCategory : byte
    {
        Generic,
        Music,
        Voice,
        World,
        Interface
    }
}
