﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UBeau;

namespace UBeau.Audio
{
    [SoloPrefab]
    public partial class SoundMgr : Singleton<SoundMgr>
    {
        private Player[] m_Players;
        private LinkedList<Player> m_ActivePlayers;
        private LinkedList<Player> m_FreePlayers;
    }
}
