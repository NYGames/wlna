﻿using System;
using UnityEditor;
using UBeau.Serialization;

namespace UBeau.Editor
{
    public abstract partial class DataEditor<T> : EditorWindow where T :class, ISerializedData
    {
        #region Commands

        /// <summary>
        /// No command.
        /// </summary>
        public const byte CMD_NONE = 0;

        /// <summary>
        /// Create a new file.
        /// </summary>
        public const byte CMD_NEW = 1;

        /// <summary>
        /// Save the current file.
        /// </summary>
        public const byte CMD_SAVE = 2;

        /// <summary>
        /// Save the current file in a new location.
        /// </summary>
        public const byte CMD_SAVE_AS = 3;

        /// <summary>
        /// Load the current file.
        /// </summary>
        public const byte CMD_LOAD = 4;

        /// <summary>
        /// Reload the current file.
        /// </summary>
        public const byte CMD_RELOAD = 5;

        /// <summary>
        /// Alert the user their file has changed.
        /// </summary>
        public const byte CMD_WARN_CHANGED = 10;

        /// <summary>
        /// Alert the user their file has been deleted.
        /// </summary>
        public const byte CMD_WARN_DELETED = 11;

        #endregion

        // Command to process next frame
        private byte m_QueuedCommand = CMD_NONE;

        // Optional argument for the command.
        private object m_QueuedCommandArgs;

        /// <summary>
        /// Queues a command to be processed next frame with optional arguments.
        /// </summary>
        public void QueueCommand(byte inCommand, object inArgs = null)
        {
            m_QueuedCommand = inCommand;
            m_QueuedCommandArgs = inArgs;
        }

        /// <summary>
        /// Returns an action that, when called, queues a command.
        /// </summary>
        protected GenericMenu.MenuFunction QueueCommandFunction(byte inCommand, object inArgs = null)
        {
            return () =>
            {
                m_QueuedCommand = inCommand;
                m_QueuedCommandArgs = inArgs;
            };
        }

        /// <summary>
        /// Processes the given command.
        /// </summary>
        protected virtual bool ProcessCommand(byte inCommand, object inArgs)
        {
            switch (inCommand)
            {
                case CMD_NEW:
                    TryNew();
                    return true;

                case CMD_SAVE:
                    TrySave(false);
                    return true;

                case CMD_SAVE_AS:
                    TrySave(true);
                    return true;

                case CMD_LOAD:
                    TryLoad();
                    return true;

                case CMD_RELOAD:
                    TryLoadFromPath(m_FilePath);
                    return true;

                case CMD_WARN_CHANGED:
                    TryFileChanged();
                    return true;

                case CMD_WARN_DELETED:
                    TryFileDeleted();
                    return true;

                case CMD_NONE:
                default:
                    return false;
            }
        }

        private void UpdateCommands()
        {
            if (m_QueuedCommand > CMD_NONE)
            {
                byte command = m_QueuedCommand;
                object args = m_QueuedCommandArgs;

                m_QueuedCommand = CMD_NONE;
                m_QueuedCommandArgs = null;

                bool bProcessed = ProcessCommand(command, args);
                if (bProcessed)
                    Repaint();
            }
        }
    }
}
