﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using UBeau.Serialization;
using UBeau;

namespace UBeau.Editor
{
    public abstract partial class DataEditor<T> : EditorWindow where T : class, ISerializedData
    {
        // Whether changes have been made to the current object.
        [SerializeField]
        private bool m_UnsavedChanges;

        // Watches for file changes
        private FileSystemWatcher m_FileWatcher;

        #region Unsaved Changes

        /// <summary>
        /// Call when you want to mark a change.
        /// </summary>
        protected void MarkUnsavedChanges()
        {
            m_UnsavedChanges = true;
        }

        /// <summary>
        /// Checks if there have been unsaved changes and prompts to proceed.
        /// </summary>
        private bool CheckUnsavedChanges()
        {
            if (m_UnsavedChanges)
            {
                int option = EditorUtility.DisplayDialogComplex("Unsaved Changes",
                    "You have unsaved changes. Save first?",
                    "Yes", "No", "Cancel");
                if (option == 2)
                    return false;
                if (option == 0)
                    return TrySave(false);
            }

            return true;
        }

        #endregion

        #region Saving

        /// <summary>
        /// Attempts to save the current file.
        /// </summary>
        private bool TrySave(bool inbForceDialog)
        {
            bool bSuccess = true;

            string filePath = m_FilePath;
            if (string.IsNullOrEmpty(filePath) || inbForceDialog)
                bSuccess = ShowSaveDialog(out filePath);

            if (bSuccess)
                bSuccess = Save(filePath);

            if (bSuccess)
                m_UnsavedChanges = false;

            return bSuccess;

        }

        /// <summary>
        /// Saves the current file.
        /// </summary>
        private bool Save(string inFilePath)
        {
            try
            {
                StopWatchingFile();

                T myObject = GetObject();
                var json = DataSerializer.WriteObject(ref myObject);
                File.WriteAllText(inFilePath, json.ToFormattedString());
                AssetDatabase.Refresh();
                ShowNotification(new GUIContent("Saved!"));
                m_FilePath = inFilePath;
                //TODO(Alex): Add to recent files
                WatchFile();
                return true;
            }
            catch (Exception e)
            {
                WatchFile();
                EditorUtility.DisplayDialog("Save Error",
                    "Unable to save to location '" + inFilePath + "': " + e.Message,
                    "OK");
                return false;
            }
        }

        /// <summary>
        /// Shows the save dialog.
        /// </summary>
        private bool ShowSaveDialog(out string outFilePath)
        {
            outFilePath = EditorUtility.SaveFilePanel("Save",
                string.IsNullOrEmpty(m_FilePath) ? "Assets" : Path.GetDirectoryName(m_FilePath),
                Path.GetFileNameWithoutExtension(m_FilePath), FileExtension);
            return !string.IsNullOrEmpty(outFilePath);
        }

        #endregion

        #region Loading

        /// <summary>
        /// Attempts to load a file.
        /// </summary>
        private bool TryLoad()
        {
            string filePath = m_FilePath;
            bool bSuccess = CheckUnsavedChanges();

            if (bSuccess)
                bSuccess = ShowLoadDialog(out filePath);
            if (bSuccess)
                bSuccess = Load(filePath);

            return bSuccess;
        }

        /// <summary>
        /// Attempts to load a file from a path.
        /// </summary>
        private bool TryLoadFromPath(string inFilePath)
        {
            bool bSuccess = CheckUnsavedChanges();

            if (bSuccess)
            {
                bSuccess = File.Exists(inFilePath);
                if (!bSuccess)
                {
                    //TODO(Alex): Check to remove from recent files list
                    EditorUtility.DisplayDialog("File Missing",
                        "File '" + inFilePath + "' no longer exists.",
                        "OK");
                }
            }

            if (bSuccess)
                bSuccess = Load(inFilePath);

            return bSuccess;
        }

        /// <summary>
        /// Loads a file from the given path.
        /// </summary>
        private bool Load(string inFilePath)
        {
            try
            {
                string fileString = File.ReadAllText(inFilePath);
                T newObject = null;
                if (DataSerializer.ReadString<T>(ref newObject, fileString, false))
                {
                    Reset(newObject, inFilePath);
                    ShowNotification(new GUIContent("Loaded!"));
                    //TODO(Alex): Push to recent files
                    WatchFile();
                    return true;
                }
                else
                {
                    EditorUtility.DisplayDialog("Load Error",
                        "File at '" + inFilePath + "' could not be parsed properly.",
                        "OK");
                    return false;
                }
            }
            catch (Exception e)
            {
                EditorUtility.DisplayDialog("Load Error",
                    "Unable to load from location '" + inFilePath + "': " + e.Message,
                    "OK");
                return false;
            }
        }

        /// <summary>
        /// Shows a load dialog box.
        /// </summary>
        private bool ShowLoadDialog(out string outFilePath)
        {
            outFilePath = EditorUtility.OpenFilePanel("Load",
                string.IsNullOrEmpty(m_FilePath) ? "Assets" : Path.GetDirectoryName(m_FilePath),
                FileExtension);
            return !string.IsNullOrEmpty(outFilePath);
        }

        #endregion

        #region New/Reset

        /// <summary>
        /// Saves unsaved changes and refreshes the current file
        /// </summary>
        private bool TryNew()
        {
            bool bSuccess = CheckUnsavedChanges();
            if (bSuccess)
            {
                ShowNotification(new GUIContent("New " + ObjectName + "!"));
                Reset(GetDefaultObject(), string.Empty);
            }
            return bSuccess;
        }

        /// <summary>
        /// Resets the current data.
        /// </summary>
        protected void Reset(T inData, string inFilePath)
        {
            m_FilePath = inFilePath;
            m_UnsavedChanges = false;
            SetObject(inData);
        }

        #endregion

        #region File Watcher

        /// <summary>
        /// Starts watching for changes on the current file.
        /// </summary>
        private void WatchFile()
        {
            Ref.Dispose(ref m_FileWatcher);
            if (!string.IsNullOrEmpty(m_FilePath))
            {
                m_FileWatcher = new FileSystemWatcher(Path.GetDirectoryName(m_FilePath), "*" + FileExtension);
                m_FileWatcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.Size;
                m_FileWatcher.Changed += OnFileSystemChanged;
                m_FileWatcher.Deleted += OnFileSystemDeleted;
                m_FileWatcher.EnableRaisingEvents = true;
            }
        }

        /// <summary>
        /// Stops watching for changes on the current file.
        /// </summary>
        private void StopWatchingFile()
        {
            Ref.Dispose(ref m_FileWatcher);
        }
        
        /// <summary>
        /// Triggered when a file changes.
        /// </summary>
        private void OnFileSystemChanged(object source, FileSystemEventArgs e)
        {
            if (e.FullPath == Path.GetFullPath(m_FilePath))
                QueueCommand(CMD_WARN_CHANGED);
        }

        /// <summary>
        /// Triggered when a file is deleted.
        /// </summary>
        private void OnFileSystemDeleted(object source, FileSystemEventArgs e)
        {
            if (e.FullPath == Path.GetFullPath(m_FilePath))
                QueueCommand(CMD_WARN_DELETED);
        }

        /// <summary>
        /// Alerts the user that the file changed and prompts them to reload.
        /// </summary>
        private bool TryFileChanged()
        {
            bool bSuccess = EditorUtility.DisplayDialog("New version detected", "An updated version of the file has been detected.  Reload?", "Yes", "No");
            if (bSuccess)
            {
                bSuccess = Load(m_FilePath);
                Repaint();
            }
            return bSuccess;
        }

        /// <summary>
        /// Alerts the user that the file was deleted and prompts them to reset.
        /// </summary>
        private bool TryFileDeleted()
        {
            bool bSuccess = EditorUtility.DisplayDialog("File deleted", "The loaded file no longer exists.  Keep in editor?", "Yes", "No");
            if (!bSuccess)
            {
                ShowNotification(new GUIContent("Cleared"));
                Reset(GetDefaultObject(), string.Empty);
                Repaint();
            }
            return !bSuccess;
        }

        #endregion
    }
}
