﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using UBeau.Serialization;

namespace UBeau.Editor
{
    public abstract partial class DataEditor<T> : EditorWindow where T : class, ISerializedData
    {
        private Vector2 m_Scroll;

        private void OnGUI()
        {
            HandleGUIEvents();

            try
            {
                // Render top menu
                EditorGUILayout.BeginHorizontal(GUILayout.Height(16));
                RenderTopMenu();
                EditorGUILayout.EndHorizontal();

                LayoutUtils.HorizontalDivider();

                // Render editor
                //TODO(Alex): Split scrolling into its own thing.
                //This will allow for multi-panel editors.
                m_Scroll = EditorGUILayout.BeginScrollView(m_Scroll);
                EditorGUILayout.BeginVertical();
                RenderEditor();
                EditorGUILayout.EndVertical();
                EditorGUILayout.EndScrollView();

                GUILayout.FlexibleSpace();
                LayoutUtils.HorizontalDivider();

                // Render stats bar
                EditorGUILayout.BeginHorizontal(GUILayout.Height(16));
                RenderBottomStats();
                EditorGUILayout.EndHorizontal();
            }
            catch(Exception e)
            {
                Debug.LogError("Exception when rendering editor: " + e.Message + "\nat " + e.StackTrace);
            }
        }

        protected virtual void RenderTopMenu()
        {
            if (GUILayout.Button("File", GUILayout.Width(50)))
            {
                GenericMenu fileMenu = new GenericMenu();
                fileMenu.AddItem(new GUIContent("New"), false, QueueCommandFunction(CMD_NEW));
                fileMenu.AddSeparator(string.Empty);
                fileMenu.AddItem(new GUIContent("Save"), false, QueueCommandFunction(CMD_SAVE));
                fileMenu.AddItem(new GUIContent("Save As"), false, QueueCommandFunction(CMD_SAVE_AS));
                fileMenu.AddItem(new GUIContent("Load"), false, QueueCommandFunction(CMD_LOAD));

                //TODO(Alex): Add recent items
                fileMenu.AddDisabledItem(new GUIContent("Recent/No Recent Files"));

                if (string.IsNullOrEmpty(m_FilePath))
                    fileMenu.AddDisabledItem(new GUIContent("Reload"));
                else
                    fileMenu.AddItem(new GUIContent("Reload"), false, QueueCommandFunction(CMD_RELOAD));
                fileMenu.ShowAsContext();
            }
        }

        protected virtual void RenderEditor()
        {

        }

        protected virtual void RenderBottomStats()
        {
            string currentLabel = string.Format("Editing: {0}{1}",
                string.IsNullOrEmpty(m_FilePath) ? "Unsaved File" : Path.GetFileNameWithoutExtension(m_FilePath),
                m_UnsavedChanges ? " (Unsaved Changes)" : string.Empty);
            GUILayout.Label(currentLabel);
        }
    }
}
