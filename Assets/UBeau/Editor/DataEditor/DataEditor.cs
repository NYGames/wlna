﻿using UnityEditor;
using UnityEngine;
using UBeau.Serialization;

namespace UBeau.Editor
{
    public abstract partial class DataEditor<T> : EditorWindow where T : class, ISerializedData
    {
        // Current file path.
        [SerializeField]
        private string m_FilePath;

        /// <summary>
        /// Name of the current type.
        /// </summary>
        public virtual string ObjectName
        {
            get { return ObjectNames.NicifyVariableName(typeof(T).Name); }
        }

        /// <summary>
        /// Extension to use for serializing to files.
        /// </summary>
        public virtual string FileExtension
        {
            get { return "json"; }
        }

        /// <summary>
        /// File path of the currently edited object.
        /// </summary>
        public string FilePath
        {
            get { return m_FilePath; }
            protected set { m_FilePath = value; }
        }

        #region Object

        /// <summary>
        /// Returns the current editing object.
        /// </summary>
        protected abstract T GetObject();

        /// <summary>
        /// Sets the current editing object.
        /// </summary>
        protected abstract void SetObject(T inObject);

        /// <summary>
        /// Returns a default object (used for resetting).
        /// </summary>
        protected abstract T GetDefaultObject();

        #endregion

        #region Events

        protected virtual void OnEnable()
        {
            //TODO(Alex): Load recent files
            WatchFile();
        }

        protected virtual void OnDestroy()
        {
            CheckUnsavedChanges();
            StopWatchingFile();
        }

        protected virtual void Update()
        {
            UpdateCommands();
        }

        protected virtual void HandleGUIEvents()
        {

        }

        #endregion
    }
}
