﻿using UnityEditor;
using UnityEngine;

namespace UBeau.Editor
{
    static public class LayoutUtils
    {
        static public void HorizontalDivider(float inHeight = 2.0f)
        {
            GUILayout.Box(string.Empty, GUILayout.ExpandWidth(true), GUILayout.Height(inHeight));
        }

        static public void VerticalDivider(float inWidth = 2.0f)
        {
            GUILayout.Box(string.Empty, GUILayout.ExpandHeight(true), GUILayout.Width(inWidth));
        }

        /// <summary>
        /// Begins an indented region.
        /// </summary>
        static public void BeginIndent(float inWidth = 20)
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(inWidth);
            EditorGUILayout.BeginVertical();
        }

        /// <summary>
        /// Ends a previously-declared indented region.
        /// </summary>
        static public void EndIndent()
        {
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();
        }

        static public void DrawArrow(Vector2 inPosition, Vector2 inFacing, Vector2 inSize)
        {
            // Ignore if we aren't repainting.
            if (Event.current.type != EventType.Repaint)
                return;

            inFacing = inFacing.normalized;

            Vector2 baseCenter = inPosition + -(inFacing * inSize.y);
            Vector2 perpendicular = new Vector2(-inFacing.y, inFacing.x);
            Vector2 baseA = baseCenter + (perpendicular * inSize.x / 2);
            Vector2 baseB = baseCenter - (perpendicular * inSize.x / 2);

            Handles.DrawLine(inPosition, baseA);
            Handles.DrawLine(inPosition, baseB);
            Handles.DrawLine(baseA, baseB);
        }
    }
}
