﻿using System;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace UBeau.Editor
{
    static public class DataSerializerCodeGen
    {
        static private readonly string AUTO_GEN_PATH = "Assets/UBeau/Files/Serialization/AutoGen";
        static private readonly string TEMPLATE_STRUCT_PATH = "Assets/UBeau/Editor/DataSerializerStructTemplate.txt";
        static private readonly string TEMPLATE_CLASS_PATH = "Assets/UBeau/Editor/DataSerializerClassTemplate.txt";

        static private readonly BindingData[] STRUCT_TYPES =
        {
            new BindingData(typeof(Int32), "AsInt"),
            new BindingData(typeof(Int64), "AsLong"),
            new BindingData(typeof(UInt32), "AsUInt"),
            new BindingData(typeof(Byte), "AsInt"),
            new BindingData(typeof(Double), "AsDouble"),
            new BindingData(typeof(Boolean), "AsBool"),
            new BindingData(typeof(Single), "AsFloat")
        };

        [MenuItem("Code Gen/DataSerializer")]
        static private void GenerateBindings()
        {
            for (int i = 0; i < STRUCT_TYPES.Length; ++i)
                GenerateBindingForType(STRUCT_TYPES[i]);
            AssetDatabase.Refresh();
        }

        static private void GenerateBindingForType(BindingData inBinding)
        {
            string template = File.ReadAllText(inBinding.Type.IsValueType ? TEMPLATE_STRUCT_PATH : TEMPLATE_CLASS_PATH);
            string replaced = template.Replace("$(type)", inBinding.Type.Name).Replace("$(jsonProperty)", inBinding.JsonProperty);
            File.WriteAllText(Path.Combine(AUTO_GEN_PATH, "DataSerializer." + inBinding.Type.Name + ".cs"), replaced);
        }

        private struct BindingData
        {
            public Type Type;
            public string JsonProperty;

            public BindingData(Type inType, string inJsonProperty)
            {
                Type = inType;
                JsonProperty = inJsonProperty;
            }
        }
    }
}