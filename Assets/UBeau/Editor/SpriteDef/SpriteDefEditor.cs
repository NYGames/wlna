﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

using UBeau;
using UBeau.Graphics;

namespace UBeau.Editor
{
    [CustomEditor(typeof(SpriteDef))]
    public sealed class SpriteDefEditor : UnityEditor.Editor
    {
        private SpriteDef m_Sprite;
        private Vector2 m_AnimRegionScroll;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            /*
            TODO(Alex): Make this work!
            m_Sprite = (SpriteDef)target;

            EditorGUI.BeginChangeCheck();
            {
                RenderAnimRegion();
                LayoutUtils.HorizontalDivider(2);
            }
            if (EditorGUI.EndChangeCheck())
                EditorUtility.SetDirty(m_Sprite);
            */
        }

        private void RenderAnimRegion()
        {
            m_AnimRegionScroll = EditorGUILayout.BeginScrollView(m_AnimRegionScroll, false, false, GUILayout.Height(128));
            {
                EditorGUILayout.BeginVertical();
                {
                    for (int i = 0; i < m_Sprite.Animations.Count; ++i)
                        RenderAnimSelector(m_Sprite.Animations[i], false);
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndScrollView();
        }

        private void RenderAnimSelector(SpriteDef.Animation inAnimation, bool inbSelected)
        {
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField(inAnimation.ID);
                GUILayout.FlexibleSpace();
                GUILayout.Button("X");
            }
            EditorGUILayout.EndHorizontal();
        }

        #region Styles

        [NonSerialized]
        static private bool s_InitializedStyles = false;

        #endregion
    }
}
