﻿using UnityEditor;
using UnityEngine;

namespace UBeau.Editor
{
    public class BuildProfileEditor : DataEditor<BuildProfile>
    {
        [MenuItem("Build/Edit Profiles %q")]
        static private void Open()
        {
            var window = EditorWindow.GetWindow<BuildProfileEditor>();
            window.titleContent = new GUIContent("BuildProfiles");
            window.minSize = new Vector2(350, 100);
            window.hideFlags = HideFlags.HideAndDontSave;
            window.Show();
        }

        #region Object Type

        [SerializeField]
        private BuildProfile m_Profile;

        public override string FileExtension
        {
            get { return "cfg.json"; }
        }

        public override string ObjectName
        {
            get { return "Build Profile"; }
        }

        protected override BuildProfile GetObject()
        {
            return m_Profile;
        }

        protected override BuildProfile GetDefaultObject()
        {
            return new BuildProfile();
        }

        protected override void SetObject(BuildProfile inObject)
        {
            m_Profile = inObject;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            if (m_Profile == null)
            {
                if (BuildProfile.HasLastConfig())
                {
                    BuildProfile profile;
                    BuildProfile.LoadLastConfig(out profile);
                    if (profile != null)
                    {
                        Reset(profile, BuildProfile.GetLastApplied());
                    }
                }

                if (m_Profile == null)
                {
                    var profile = new BuildProfile();
                    profile.Reset();
                    Reset(profile, string.Empty);
                }
            }
        }

        #endregion

        #region Commands

        private const byte CMD_APPLY_CONFIG = 100;

        private const byte CMD_SET_PRODUCT_NAME = 101;
        private const byte CMD_SET_BUNDLE_IDENTIFIER = 102;
        private const byte CMD_SET_DEVELOPMENT = 103;
        private const byte CMD_SET_BUILD_NAME = 104;
        private const byte CMD_SET_DEFINES = 105;
        private const byte CMD_SET_ICON = 106;
        private const byte CMD_SET_ICON_IOS = 107;
        private const byte CMD_SET_SPLASH = 108;
        private const byte CMD_SET_SPLASH_IOS = 109;

        protected override bool ProcessCommand(byte inCommand, object inArgs)
        {
            if (base.ProcessCommand(inCommand, inArgs))
                return true;

            switch (inCommand)
            {
                case CMD_APPLY_CONFIG:
                    {
                        m_Profile.Apply();
                        BuildProfile.SetLastApplied(FilePath);
                        return true;
                    }

                case CMD_SET_PRODUCT_NAME:
                    {
                        string str = (string)inArgs;
                        if (m_Profile.ProductName != str)
                        {
                            m_Profile.ProductName = str;
                            MarkUnsavedChanges();
                        }
                        return true;
                    }

                case CMD_SET_BUNDLE_IDENTIFIER:
                    {
                        string str = (string)inArgs;
                        if (m_Profile.BundleIdentifier != str)
                        {
                            m_Profile.BundleIdentifier = str;
                            MarkUnsavedChanges();
                        }
                        return true;
                    }

                case CMD_SET_DEVELOPMENT:
                    {
                        bool dev = (bool)inArgs;
                        if (m_Profile.IsDevelopment != dev)
                        {
                            m_Profile.IsDevelopment = dev;
                            MarkUnsavedChanges();
                        }
                        return true;
                    }

                case CMD_SET_BUILD_NAME:
                    {
                        string str = (string)inArgs;
                        if (m_Profile.BuildName != str)
                        {
                            m_Profile.BuildName = str;
                            MarkUnsavedChanges();
                        }
                        return true;
                    }

                case CMD_SET_DEFINES:
                    {
                        string str = (string)inArgs;
                        if (m_Profile.Defines != str)
                        {
                            m_Profile.Defines = str;
                            MarkUnsavedChanges();
                        }
                        return true;
                    }

                case CMD_SET_ICON:
                    {
                        Texture2D tex = (Texture2D)inArgs;
                        if (m_Profile.Icon != tex)
                        {
                            m_Profile.Icon = tex;
                            MarkUnsavedChanges();
                        }
                        return true;
                    }

                case CMD_SET_ICON_IOS:
                    {
                        Texture2D tex = (Texture2D)inArgs;
                        if (m_Profile.IconIOS != tex)
                        {
                            m_Profile.IconIOS = tex;
                            MarkUnsavedChanges();
                        }
                        return true;
                    }

                case CMD_SET_SPLASH:
                    {
                        Texture2D tex = (Texture2D)inArgs;
                        if (m_Profile.Splash != tex)
                        {
                            m_Profile.Splash = tex;
                            MarkUnsavedChanges();
                        }
                        return true;
                    }

                case CMD_SET_SPLASH_IOS:
                    {
                        Texture2D tex = (Texture2D)inArgs;
                        if (m_Profile.SplashIOS != tex)
                        {
                            m_Profile.SplashIOS = tex;
                            MarkUnsavedChanges();
                        }
                        return true;
                    }

                default:
                    return false;
            }
        }

        #endregion

        #region Rendering

        private bool m_ShowCompilerSettings;
        private bool m_ShowIconSettings;
        private bool m_ShowSplashSettings;

        protected override void RenderEditor()
        {
            base.RenderEditor();

            // Product Name
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Product Name: ", GUILayout.Width(100));
                EditorGUI.BeginChangeCheck();
                string productName = EditorGUILayout.TextField(m_Profile.ProductName, GUILayout.Width(200));
                if (EditorGUI.EndChangeCheck())
                    QueueCommand(CMD_SET_PRODUCT_NAME, productName);
            }
            EditorGUILayout.EndHorizontal();

            // Bundle Identifier
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Bundle ID: ", GUILayout.Width(100));
                EditorGUI.BeginChangeCheck();
                string bundleID = EditorGUILayout.TextField(m_Profile.BundleIdentifier, GUILayout.Width(200));
                if (EditorGUI.EndChangeCheck())
                    QueueCommand(CMD_SET_BUNDLE_IDENTIFIER, bundleID);
            }
            EditorGUILayout.EndHorizontal();

            RenderCompilerSettings();
            RenderIconSettings();
            RenderSplashSettings();

            GUILayout.Space(10);

            if (GUILayout.Button("Apply Settings", GUILayout.Width(300)))
                QueueCommand(CMD_APPLY_CONFIG);
        }

        private void RenderCompilerSettings()
        {
            m_ShowCompilerSettings = EditorGUILayout.Foldout(m_ShowCompilerSettings, "Compiler Settings");
            if (m_ShowCompilerSettings)
            {
                LayoutUtils.BeginIndent();

                // Defines
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.LabelField("Defines: ", GUILayout.Width(100));
                    EditorGUI.BeginChangeCheck();
                    string defines = EditorGUILayout.TextField(m_Profile.Defines, GUILayout.Width(200));
                    if (EditorGUI.EndChangeCheck())
                        QueueCommand(CMD_SET_DEFINES, defines);
                }
                EditorGUILayout.EndHorizontal();

                // Development Build
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.LabelField("Dev Build: ", GUILayout.Width(100));
                    EditorGUI.BeginChangeCheck();
                    bool dev = EditorGUILayout.Toggle(m_Profile.IsDevelopment);
                    if (EditorGUI.EndChangeCheck())
                        QueueCommand(CMD_SET_DEVELOPMENT, dev);
                }
                EditorGUILayout.EndHorizontal();

                // Warnings
                if (m_Profile.IsDevelopment && !m_Profile.Defines.Contains("DEVELOPMENT"))
                {
                    EditorGUILayout.HelpBox("You checked 'Dev Build' but didn't define 'DEVELOPMENT'.",
                        UnityEditor.MessageType.Warning);
                }
                else if (!m_Profile.IsDevelopment && m_Profile.Defines.Contains("DEVELOPMENT"))
                {
                    EditorGUILayout.HelpBox("You defined 'DEVELOPMENT' but didn't check 'Dev Build'.",
                        UnityEditor.MessageType.Warning);
                }

                LayoutUtils.EndIndent();
            }
        }

        private void RenderIconSettings()
        {
            m_ShowIconSettings = EditorGUILayout.Foldout(m_ShowIconSettings, "Icon");
            if (m_ShowIconSettings)
            {
                LayoutUtils.BeginIndent();

                // Normal Icon
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.LabelField("Normal: ", GUILayout.Width(100));
                    EditorGUI.BeginChangeCheck();
                    Texture2D icon = (Texture2D)EditorGUILayout.ObjectField(m_Profile.Icon, typeof(Texture2D), false, GUILayout.Width(200));
                    if (EditorGUI.EndChangeCheck())
                        QueueCommand(CMD_SET_ICON, icon);
                }
                EditorGUILayout.EndHorizontal();

                // IOS Icon
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.LabelField("iOS: ", GUILayout.Width(100));
                    EditorGUI.BeginChangeCheck();
                    Texture2D icon = (Texture2D)EditorGUILayout.ObjectField(m_Profile.IconIOS, typeof(Texture2D), false, GUILayout.Width(200));
                    if (EditorGUI.EndChangeCheck())
                        QueueCommand(CMD_SET_ICON_IOS, icon);
                }
                EditorGUILayout.EndHorizontal();

                LayoutUtils.EndIndent();
            }
        }

        private void RenderSplashSettings()
        {
            m_ShowSplashSettings = EditorGUILayout.Foldout(m_ShowSplashSettings, "Splash Screen");
            if (m_ShowSplashSettings)
            {
                LayoutUtils.BeginIndent();

                // Normal Splash Screen
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.LabelField("Normal: ", GUILayout.Width(100));
                    EditorGUI.BeginChangeCheck();
                    Texture2D splash = (Texture2D)EditorGUILayout.ObjectField(m_Profile.Splash, typeof(Texture2D), false, GUILayout.Width(200));
                    if (EditorGUI.EndChangeCheck())
                        QueueCommand(CMD_SET_SPLASH, splash);
                }
                EditorGUILayout.EndHorizontal();

                // IOS Splash Screen
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.LabelField("iOS: ", GUILayout.Width(100));
                    EditorGUI.BeginChangeCheck();
                    Texture2D splash = (Texture2D)EditorGUILayout.ObjectField(m_Profile.SplashIOS, typeof(Texture2D), false, GUILayout.Width(200));
                    if (EditorGUI.EndChangeCheck())
                        QueueCommand(CMD_SET_SPLASH_IOS, splash);
                }
                EditorGUILayout.EndHorizontal();

                LayoutUtils.EndIndent();
            }
        }

        #endregion
    }
}