﻿using SimpleJSON;
using System;
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;

namespace UBeau.Editor
{
    static public class BuildHelper
    {
        private const string STREAMING_ASSETS_PATH = "Assets/_Build/StreamingAssets";
        private const string BUILD_FILE_NAME = "buildInfo.json";
        private const string BUILD_PATH = STREAMING_ASSETS_PATH + "/" + BUILD_FILE_NAME;

        [MenuItem("Build/Load Profile/Most Recent")]
        static private void LoadRecentBuild()
        {
            BuildProfile.ApplyLastConfig();
        }

        #region Post-process

        [PostProcessScene]
        static private void AfterScene()
        {
            UpdateBuildInfo();
        }

        [PostProcessBuild]
        static private void AfterBuild(BuildTarget inTarget, string inPath)
        {
            UpdateBuildInfo();
        }

        static private void UpdateBuildInfo()
        {
            if (!Directory.Exists(STREAMING_ASSETS_PATH))
                Directory.CreateDirectory(STREAMING_ASSETS_PATH);

            JSONNode buildInfo;
            if (File.Exists(BUILD_PATH))
                buildInfo = JSON.Parse(File.ReadAllText(BUILD_PATH));
            else
                buildInfo = new JSONClass();

            var currentTime = DateTime.Now;

            buildInfo["BuildInfo"]["Date"].Value = currentTime.ToString("MM/dd/yy");
            buildInfo["BuildInfo"]["Time"].Value = currentTime.ToString("HH:mm");
            buildInfo["BuildInfo"]["Version"].Value = PlayerSettings.bundleVersion;
            buildInfo["BuildInfo"]["Code"].Value = currentTime.ToString("yyMMdd-HHmm");

            File.WriteAllText(BUILD_PATH, buildInfo.ToFormattedString());
        }

        #endregion
    }
}