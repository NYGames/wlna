﻿using System;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;
using UBeau;
using UBeau.Serialization;

namespace UBeau.Editor
{
    /// <summary>
    /// Editor configuration.  Provides conditional compilation,
    /// icons, splash screens, and more.  Useful for maintaining
    /// multiple versions.
    /// </summary>
    [Serializable]
    public class BuildProfile : ISerializedData
    {
        public BuildProfile() {}

        #region Properties

        /// <summary>
        /// Name of the product.
        /// </summary>
        public string ProductName = string.Empty;

        /// <summary>
        /// Identifier version.
        /// </summary>
        public string BundleIdentifier = string.Empty;

        /// <summary>
        /// If this is a development build.
        /// </summary>
        public bool IsDevelopment = false;

        /// <summary>
        /// Name of the built file.
        /// </summary>
        public string BuildName = string.Empty;

        #endregion

        #region Defines

        [SerializeField]
        private string m_Defines = string.Empty;

        /// <summary>
        /// Compiler defines.
        /// </summary>
        public string Defines
        {
            get { return m_Defines; }
            set { SetDefines(value); }
        }

        private void SetDefines(string inDefines)
        {
            using (PooledStringBuilder stringBuilder = PooledStringBuilder.Create())
            using(PooledList<string> defines = PooledList<string>.Create())
            {
                string correctDelims = inDefines.Replace(',', ';').Replace('.', ';').Replace(' ', ';');
                correctDelims.Split(defines, ';');
                for (int i = 0; i < defines.Count; ++i)
                    stringBuilder.Builder.Append(defines[i]).Append(';');
                if (defines.Count > 0)
                    stringBuilder.Builder.Length--;
                m_Defines = stringBuilder.Builder.ToString();
            }
        }

        #endregion

        #region Icon Image

        [SerializeField]
        private string m_IconPath;
        [SerializeField]
        private string m_IconPathIOS;

        /// <summary>
        /// Icon image to use
        /// </summary>
        public Texture2D Icon
        {
            get { return string.IsNullOrEmpty(m_IconPath) ? null : AssetDatabase.LoadAssetAtPath<Texture2D>(m_IconPath); }
            set { m_IconPath = value == null ? string.Empty : AssetDatabase.GetAssetPath(value); }
        }

        /// <summary>
        /// Icon image to use for iOS
        /// </summary>
        public Texture2D IconIOS
        {
            get { return string.IsNullOrEmpty(m_IconPathIOS) ? null : AssetDatabase.LoadAssetAtPath<Texture2D>(m_IconPathIOS); }
            set { m_IconPathIOS = value == null ? string.Empty : AssetDatabase.GetAssetPath(value); }
        }

        #endregion

        #region Splash Image

        [SerializeField]
        private string m_SplashPath;
        [SerializeField]
        private string m_SplashPathIOS;

        /// <summary>
        /// Splash image to use
        /// </summary>
        public Texture2D Splash
        {
            get { return string.IsNullOrEmpty(m_SplashPath) ? null : AssetDatabase.LoadAssetAtPath<Texture2D>(m_SplashPath); }
            set { m_SplashPath = value == null ? string.Empty : AssetDatabase.GetAssetPath(value); }
        }

        /// <summary>
        /// Splash image to use for iOS
        /// </summary>
        public Texture2D SplashIOS
        {
            get { return string.IsNullOrEmpty(m_SplashPathIOS) ? null : AssetDatabase.LoadAssetAtPath<Texture2D>(m_SplashPathIOS); }
            set { m_SplashPathIOS = value == null ? string.Empty : AssetDatabase.GetAssetPath(value); }
        }

        #endregion

        #region ISerializedData

        public void Reset()
        {
            ProductName = PlayerSettings.productName;
            BundleIdentifier = PlayerSettings.bundleIdentifier;
            BuildName = string.Empty;
            IsDevelopment = false;

            m_IconPath = m_IconPathIOS = string.Empty;
            m_SplashPath = m_SplashPathIOS = string.Empty;

            m_Defines = string.Empty;
        }

        public bool Serialize(DataSerializer inSerializer)
        {
            bool bSuccess;
            bSuccess = inSerializer.Serialize("ProductName", ref ProductName);
            bSuccess &= inSerializer.Serialize("BundleIdentifier", ref BundleIdentifier);

            bSuccess &= inSerializer.Serialize("BuildName", ref BuildName, string.Empty);
            bSuccess &= inSerializer.Serialize("Development", ref IsDevelopment, false);
            bSuccess &= inSerializer.Serialize("Defines", ref m_Defines, string.Empty);

            bSuccess &= inSerializer.BeginGroup("Images");
            if (bSuccess)
            {
                bSuccess &= inSerializer.Serialize("Icon", ref m_IconPath);
                bSuccess &= inSerializer.Serialize("IconIOS", ref m_IconPathIOS, string.Empty);

                bSuccess &= inSerializer.Serialize("Splash", ref m_SplashPath);
                bSuccess &= inSerializer.Serialize("SplashIOS", ref m_SplashPathIOS, string.Empty);
            }
            inSerializer.EndGroup();

            return bSuccess;
        }

        #endregion

        #region Applying

        public void Apply()
        {
            PlayerSettings.productName = ProductName;
            PlayerSettings.bundleIdentifier = BundleIdentifier;
            EditorUserBuildSettings.development = IsDevelopment;

            TryCopyFile(m_IconPath, ICON_FILE);
            TryCopyFile(m_IconPathIOS, ICON_IOS_FILE);

            TryCopyFile(m_SplashPath, SPLASH_FILE);
            TryCopyFile(m_SplashPathIOS, SPLASH_IOS_FILE);

            WriteDefines(m_Defines);
            ForceRecompile();
        }

        // Writes defines to all necessary files
        static private void WriteDefines(string inDefines)
        {
            string defines = string.Empty;
            if (!string.IsNullOrEmpty(inDefines))
                defines = "-define:" + inDefines;

            foreach (var fileName in DEFINE_FILES)
                File.WriteAllText(fileName, defines);
        }

        // Attempts to copy from one file to another
        static private void TryCopyFile(string inSource, string inTarget)
        {
            if (!string.IsNullOrEmpty(inSource) && File.Exists(inTarget))
                File.Copy(inSource, inTarget, true);
        }

        // Forces Unity to recompile editor and runtime assemblies
        static private void ForceRecompile()
        {
            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
            foreach (var filename in REIMPORT_FILES)
                AssetDatabase.ImportAsset(filename, ImportAssetOptions.ForceUpdate);
        }

        private const string ICON_FILE = "Assets/_Build/Icon.png";
        private const string ICON_IOS_FILE = "Assets/_Build/Icon_IOS.png";
        private const string SPLASH_FILE = "Assets/_Build/Splash.png";
        private const string SPLASH_IOS_FILE = "Assets/_Build/Splash_IOS.png";

        static private readonly string[] DEFINE_FILES = new string[]
        {
            "Assets/smcs.rsp",
            "Assets/gmcs.rsp",
            "Assets/csc.rsp"
        };

        static private readonly string[] REIMPORT_FILES = new string[]
        {
            "Assets/UBeau/Utils/Assert.cs",
            "Assets/UBeau/Editor/Build/BuildProfile.cs"
        };

        #endregion

        #region Applying from files

        /// <summary>
        /// Attempts to apply the config at the given path.
        /// </summary>
        static public bool ApplyConfig(string inFileName)
        {
            BuildProfile profile;
            return ApplyConfig(inFileName, out profile);
        }

        /// <summary>
        /// Attempts to apply the config at the given path.
        /// </summary>
        static public bool ApplyConfig(string inFileName, out BuildProfile outProfile)
        {
            try
            {
                if (string.IsNullOrEmpty(inFileName))
                {
                    outProfile = null;
                    return false;
                }

                if (!inFileName.EndsWith(".cfg.json"))
                    inFileName += ".cfg.json";

                string jsonString = File.ReadAllText(inFileName);
                outProfile = null;
                if (DataSerializer.ReadString(ref outProfile, jsonString, false))
                {
                    outProfile.Apply();
                    SetLastApplied(inFileName);
                    return true;
                }

                Log.Error("Unable to parse config: invalid format");
                return false;
            }
            catch(Exception e)
            {
                Log.Error("Unable to parse config: {0}", e.Message);
                outProfile = null;
                return false;
            }
        }

        /// <summary>
        /// Returns if a config was ever applied.
        /// </summary>
        static public bool HasLastConfig()
        {
            ReadLastApplied();
            return !string.IsNullOrEmpty(s_LastAppliedFile);
        }

        /// <summary>
        /// Applies the last applied config.
        /// </summary>
        static public bool ApplyLastConfig(out BuildProfile outProfile)
        {
            ReadLastApplied();
            return ApplyConfig(s_LastAppliedFile, out outProfile);
        }

        /// <summary>
        /// Applies the last applied config.
        /// </summary>
        static public bool ApplyLastConfig()
        {
            ReadLastApplied();
            return ApplyConfig(s_LastAppliedFile);
        }

        /// <summary>
        /// Sets the last applied config filepath.
        /// </summary>
        static public void SetLastApplied(string inFilePath)
        {
            if (string.IsNullOrEmpty(inFilePath))
            {
                Log.Info("[CONFIG] Applied anonymous config!");
            }
            else
            {
                s_LastAppliedFile = inFilePath;
                Log.Info("[CONFIG] Applied '{0}'!", inFilePath);
                File.WriteAllText(LAST_APPLIED_FILE, inFilePath);
            }
        }

        static public string GetLastApplied()
        {
            ReadLastApplied();
            return s_LastAppliedFile;
        }

        static private void ReadLastApplied()
        {
            if (File.Exists(LAST_APPLIED_FILE))
                s_LastAppliedFile = File.ReadAllText(LAST_APPLIED_FILE);
            else
                s_LastAppliedFile = string.Empty;
        }

        static private string s_LastAppliedFile = string.Empty;

        private const string LAST_APPLIED_FILE = "Assets/_Build/lastAppliedConfig";

        #endregion

        #region Loading from Files

        /// <summary>
        /// Loads the last applied config.
        /// </summary>
        static public bool LoadLastConfig(out BuildProfile outProfile)
        {
            ReadLastApplied();
            return LoadConfig(s_LastAppliedFile, out outProfile);
        }

        /// <summary>
        /// Attempts to load config at the given path.
        /// </summary>
        static public bool LoadConfig(string inFileName, out BuildProfile outProfile)
        {
            try
            {
                if (string.IsNullOrEmpty(inFileName))
                {
                    outProfile = null;
                    return false;
                }

                if (!inFileName.EndsWith(".cfg.json"))
                    inFileName += ".cfg.json";

                string jsonString = File.ReadAllText(inFileName);
                outProfile = null;
                return DataSerializer.ReadString(ref outProfile, jsonString, false);
            }
            catch (Exception e)
            {
                Log.Error("Unable to parse config: {0}", e.Message);
                outProfile = null;
                return false;
            }
        }

        #endregion
    }
}