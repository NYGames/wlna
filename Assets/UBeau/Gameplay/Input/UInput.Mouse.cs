﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UBeau;

namespace UBeau.Gameplay
{
    static public partial class UInput
    {
        /// <summary>
        /// Interface for mouse input.
        /// </summary>
        static public class Mouse
        {
            static private State s_Current;
            static private State s_Previous;
            static private float s_ScrollWheel;

            private class State
            {
                public Vector2 Position;
                public bool[] States = new bool[3];
                public float Scroll;

                public void Update()
                {
                    Position = UnityEngine.Input.mousePosition;
                    States[0] = UnityEngine.Input.GetMouseButton(0);
                    States[1] = UnityEngine.Input.GetMouseButton(1);
                    States[2] = UnityEngine.Input.GetMouseButton(2);
                    Scroll = s_ScrollWheel + UnityEngine.Input.mouseScrollDelta.y / 10.0f;
                }
            }

            static internal void Initialize()
            {
                s_Current = new State();
                s_Previous = new State();
                s_ScrollWheel = 0;
            }

            static internal void Update()
            {
                Ref.Swap(ref s_Current, ref s_Previous);
                s_Current.Update();
                s_ScrollWheel = s_Current.Scroll;
            }

            /// <summary>
            /// Returns if the given mouse button is down.
            /// </summary>
            static public bool Down(MouseButton inButton = MouseButton.Left)
            {
                return s_Current.States[(int)inButton];
            }

            /// <summary>
            /// Returns if the given mouse button was pressed.
            /// </summary>
            static public bool Pressed(MouseButton inButton = MouseButton.Left)
            {
                return s_Current.States[(int)inButton] && !s_Previous.States[(int)inButton];
            }

            /// <summary>
            /// Returns if the given mouse button was released.
            /// </summary>
            static public bool Released(MouseButton inButton = MouseButton.Left)
            {
                return !s_Current.States[(int)inButton] && s_Previous.States[(int)inButton];
            }

            /// <summary>
            /// Current scroll position.
            /// </summary>
            static public float Scroll
            {
                get { return s_Current.Scroll; }
            }

            /// <summary>
            /// Change in scroll since last update.
            /// </summary>
            static public float ScrollDelta
            {
                get { return s_Current.Scroll - s_Previous.Scroll; }
            }

            /// <summary>
            /// Current pointer position.
            /// </summary>
            static public Vector2 Position
            {
                get { return s_Current.Position; }
            }

            /// <summary>
            /// Change in pointer position since last update.
            /// </summary>
            static public Vector2 PositionDelta
            {
                get { return s_Current.Position - s_Previous.Position; }
            }
        }
    }
}
