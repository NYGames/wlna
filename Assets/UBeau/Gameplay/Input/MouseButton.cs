﻿namespace UBeau.Gameplay
{
    public enum MouseButton : byte
    {
        Left = 0,
        Right = 1,
        Middle = 2,
    }
}
