﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UBeau;

namespace UBeau.Gameplay
{
    static public partial class UInput
    {
        /// <summary>
        /// Interface for mouse input.
        /// </summary>
        static public class Keyboard
        {
            static private State s_Current;
            static private State s_Previous;
            static private KeyCode[] s_KeyMapping;

            private class State
            {
                public bool[] States;

                public State()
                {
                    States = new bool[s_KeyMapping.Length];
                }

                internal void Update()
                {
                    for (int i = 0; i < States.Length; ++i)
                        States[i] = UnityEngine.Input.GetKey(s_KeyMapping[i]);
                }
            }

            static internal void Initialize()
            {
                // Translate from Key values to KeyCode values
                string[] keyNames = Enum.GetNames(typeof(Key));
                s_KeyMapping = new KeyCode[keyNames.Length];
                for (int i = 0; i < s_KeyMapping.Length; ++i)
                    s_KeyMapping[i] = (KeyCode)(Enum.Parse(typeof(KeyCode), keyNames[i]));

                s_Current = new State();
                s_Previous = new State();
            }

            static internal void Update()
            {
                Ref.Swap(ref s_Current, ref s_Previous);
                s_Current.Update();
            }

            /// <summary>
            /// Returns if the given key is down.
            /// </summary>
            static public bool Down(Key inButton)
            {
                return s_Current.States[(int)inButton];
            }

            /// <summary>
            /// Returns if the given key was pressed.
            /// </summary>
            static public bool Pressed(Key inButton)
            {
                return s_Current.States[(int)inButton] && !s_Previous.States[(int)inButton];
            }

            /// <summary>
            /// Returns if the given key was released.
            /// </summary>
            static public bool Released(Key inButton)
            {
                return !s_Current.States[(int)inButton] && s_Previous.States[(int)inButton];
            }
        }
    }
}
