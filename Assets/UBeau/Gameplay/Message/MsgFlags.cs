﻿using System;

namespace UBeau.Gameplay
{
    /// <summary>
    /// Set of flags determining message behavior.
    /// </summary>
    [Flags]
    public enum MsgFlags : byte
    {
        /// <summary>
        /// Default behavior. Message will execute at the end of frame
        /// and queued if unable to execute.
        /// </summary>
        Default             = 0x00,

        /// <summary>
        /// Message will be discarded if unable to execute.
        /// </summary>
        Discard             = 0x01,

        /// <summary>
        /// Message will always be executed.
        /// </summary>
        Force               = 0x02,

        /// <summary>
        /// Message will execute immediately.
        /// </summary>
        Immediate           = 0x04,

        /// <summary>
        /// Message must have at least one handler.
        /// </summary>
        RequireListener     = 0x08,

        DiscardImmediate    = Discard | Immediate,
        ForceImmediate      = Force | Immediate
    }
}