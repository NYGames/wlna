﻿using System;

namespace UBeau.Gameplay
{
    /// <summary>
    /// Defines a type of message.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = false)]
    public sealed class MsgDefinition : Attribute, IComparable<MsgDefinition>
    {
        private string m_Name;
        private MsgType m_Type;

        /// <summary>
        /// Name to display in the editor.
        /// </summary>
        public readonly string EditorName;

        /// <summary>
        /// Message behavior.
        /// </summary>
        public readonly MsgFlags Flags;

        /// <summary>
        /// Unhashed name of the message.
        /// </summary>
        public string Name
        {
            get { return m_Name; }
        }

        /// <summary>
        /// Associated type.
        /// </summary>
        public MsgType Type
        {
            get { return m_Type; }
        }

        /// <summary>
        /// Returns if the definition has any
        /// of the given flags.
        /// </summary>
        public bool HasFlags(MsgFlags inFlags)
        {
            return (Flags & inFlags) != 0;
        }

        /// <summary>
        /// Defines a message type.
        /// </summary>
        /// <param name="inFlags">How the message should behave.</param>
        /// <param name="inEditorName">Name to show in the editor. Null to hide in editor.</param>
        public MsgDefinition(MsgFlags inFlags, string inEditorName = null)
        {
            Flags = inFlags;
            EditorName = inEditorName;
        }

        internal void OnProcessed(string inName, MsgType inType)
        {
            if (m_Type != MsgType.Null)
                throw new InvalidOperationException("MessageDefine has already been processed!");

            m_Name = inName;
            m_Type = inType;
        }

        public int CompareTo(MsgDefinition inOther)
        {
            return EditorName.CompareTo(inOther.EditorName);
        }
    }
}