﻿using System;
using UnityEngine;

namespace UBeau.Gameplay
{
    /// <summary>
    /// Facilitates the passing of messages between objects.
    /// </summary>
    public sealed partial class Messenger : MonoBehaviour
    {
        /// <summary>
        /// Temporary binding of a handler to a message type.
        /// </summary>
        public sealed class Binding : IDisposable
        {
            private Messenger m_Messenger;
            private MsgType m_Type;
            private MsgHandler m_Handler;
            private int m_Counter;

            public Binding(Messenger inMessenger, MsgType inType, MsgHandler inHandler)
            {
                Assert.False(inMessenger == null, "Provided Messenger is null!");
                Assert.False(inType == MsgType.Null, "Provided type is null!");
                Assert.False(inHandler == null, "Provided handler is null!");

                m_Messenger = inMessenger;
                m_Type = inType;
                m_Handler = inHandler;
                m_Counter = 0;

                m_Messenger.Register(m_Type, OnMessage);
            }

            /// <summary>
            /// Number of times the handler was invoked.
            /// </summary>
            public int Counter
            {
                get { return m_Counter; }
            }

            /// <summary>
            /// If the handler was invoked at least once.
            /// </summary>
            public bool WasHit
            {
                get { return m_Counter > 0; }
            }

            /// <summary>
            /// Resets the handler invoke counter to zero.
            /// </summary>
            public void ResetCounter()
            {
                m_Counter = 0;
            }

            /// <summary>
            /// Unregisters and disposes of the binding.
            /// </summary>
            public void Dispose()
            {
                if (m_Messenger != null)
                    m_Messenger.Unregister(m_Type, OnMessage);

                m_Messenger = null;
                m_Handler = null;
                m_Type = MsgType.Null;
                m_Counter = 0;
            }

            private void OnMessage(Message inMessage)
            {
                ++m_Counter;
                m_Handler(inMessage);
            }
        }
    }
}
