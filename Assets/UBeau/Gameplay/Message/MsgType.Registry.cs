﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace UBeau.Gameplay
{
    public partial struct MsgType : IEquatable<MsgType>
    {
        #region Registry

        static private HashSet<Assembly> s_Processed = new HashSet<Assembly>();
        static private Dictionary<MsgType, MsgDefinition> s_Definitions = new Dictionary<MsgType, MsgDefinition>();
#if UNITY_EDITOR
        static private List<MsgDefinition> s_EditorTypes = new List<MsgDefinition>();
#endif

        /// <summary>
        /// Returns the definition for the given type.
        /// </summary>
        static public MsgDefinition Define(MsgType inType)
        {
            return s_Definitions[inType];
        }

        /// <summary>
        /// Parses the current assembly and generates the definitions.
        /// </summary>
        static public void GenerateDefinitions()
        {
            ProcessAssembly(Assembly.GetAssembly(typeof(MsgType)));

#if UNITY_EDITOR
            s_EditorTypes.Sort();
#endif
        }

        #region Parsing

        static private void ProcessAssembly(Assembly inAssembly)
        {
            if (s_Processed.Contains(inAssembly))
                return;

            s_Processed.Add(inAssembly);

            using (var t = Timer.Start("MsgType Processing"))
            {
                Log.Debug("Current Assembly: {0}", inAssembly.FullName);
                var types = inAssembly.GetTypes();
                for (int i = 0; i < types.Length; ++i)
                    ProcessType(types[i]);
            }
        }

        static private void ProcessType(Type inType)
        {
            FieldInfo[] fields = inType.GetFields(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
            for (int i = 0; i < fields.Length; ++i)
            {
                MsgDefinition define = Types.GetCustomAttribute<MsgDefinition>(fields[i]);
                if (define != null)
                {
                    Assert.True(fields[i].IsStatic, "Cannot assign MsgType to static field.");
                    Assert.True(fields[i].FieldType == typeof(MsgType), "Cannot assign tag type to non-MsgType field.");

                    string msgName = inType.FullName + "::" + fields[i].Name;

                    MsgType type = new MsgType(msgName.Hash());
                    define.OnProcessed(msgName, type);
                    s_Definitions.Add(type, define);
                    fields[i].SetValue(null, type);
#if UNITY_EDITOR
                    if (!string.IsNullOrEmpty(define.EditorName))
                        s_EditorTypes.Add(define);
#endif
                }
            }
        }

        #endregion

        #endregion

        #region Editor

#if UNITY_EDITOR

        [CustomPropertyDrawer(typeof(MsgRefAttribute), true)]
        private class MsgRefEditor : PropertyDrawer
        {
            public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
            {
                Initialize();

                EditorGUI.BeginProperty(position, label, property);
                {
                    var valueProp = property.FindPropertyRelative("m_Value");
                    int currentIndex = FindIndexByHash(valueProp.intValue);
                    int nextIndex = EditorGUI.Popup(position, label.text, currentIndex, s_MsgNames);
                    if (currentIndex != nextIndex)
                        valueProp.intValue = FindHashByIndex(nextIndex);
                }
                EditorGUI.EndProperty();
            }

            static private string[] s_MsgNames;

            static private void Initialize()
            {
                if (s_MsgNames != null)
                    return;

                GenerateDefinitions();
                s_MsgNames = new string[s_EditorTypes.Count + 1];
                s_MsgNames[0] = "[Null]";
                for (int i = 0; i < s_EditorTypes.Count; ++i)
                    s_MsgNames[i + 1] = s_EditorTypes[i].EditorName;
            }

            static private int FindIndexByHash(int inHash)
            {
                if (inHash == 0)
                    return 0;

                for (int i = 0; i < s_EditorTypes.Count; ++i)
                    if (s_EditorTypes[i].Type.m_Value == inHash)
                        return i + 1;
                return -1;
            }

            static private int FindHashByIndex(int inIndex)
            {
                if (inIndex <= 0)
                    return 0;
                return s_EditorTypes[inIndex - 1].Type.m_Value;
            }
        }
#endif

        #endregion
    }

    /// <summary>
    /// Displays a MsgType as a dropdown list of all MsgTypes
    /// available in the editor.
    /// </summary>
    public sealed class MsgRefAttribute : PropertyAttribute { }
}