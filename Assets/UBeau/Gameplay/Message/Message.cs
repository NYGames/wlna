﻿namespace UBeau.Gameplay
{
    /// <summary>
    /// Contains info about a dispatched message.
    /// </summary>
    public struct Message
    {
        /// <summary>
        /// Type of message passed.
        /// </summary>
        public readonly MsgType Type;

        /// <summary>
        /// Optional arguments.
        /// </summary>
        public readonly object Args;

        // Id of the GameObject.
        private int m_SourceID;

        /// <summary>
        /// Messenger that dispatched the message.
        /// </summary>
        public Messenger Source
        {
            get { return Messenger.Find(m_SourceID); }
        }

        public Message(MsgType inType, Messenger inSource, object inArgs)
        {
            Type = inType;
            Args = inArgs;
            m_SourceID = inSource.gameObject.GetInstanceID();
        }

        public override int GetHashCode()
        {
            return Type.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("[Message '{0}' from {1} with args {2}]", MsgType.Define(Type).Name, Source, Args);
        }
    }
}
