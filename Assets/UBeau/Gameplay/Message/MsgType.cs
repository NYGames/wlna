﻿using System;
using UnityEngine;

namespace UBeau.Gameplay
{
    /// <summary>
    /// Handle representing a type of message.
    /// </summary>
    [Serializable]
    public partial struct MsgType : IEquatable<MsgType>
    {
        [SerializeField]
        private int m_Value;

        private MsgType(int inValue)
        {
            m_Value = inValue;
        }

        /// <summary>
        /// Unregistered or empty MessageType.
        /// </summary>
        static public readonly MsgType Null = default(MsgType);

        #region Overrides

        public bool Equals(MsgType other)
        {
            return m_Value == other.m_Value;
        }

        public override bool Equals(object obj)
        {
            if (obj is MsgType)
                return Equals((MsgType)obj);
            return false;
        }

        public override int GetHashCode()
        {
            return m_Value;
        }

        public override string ToString()
        {
            return m_Value == 0 ? "Null" : Define(this).Name;
        }

        static public implicit operator bool(MsgType inType)
        {
            return inType.m_Value != 0;
        }

        static public bool operator ==(MsgType first, MsgType second)
        {
            return first.m_Value == second.m_Value;
        }

        static public bool operator !=(MsgType first, MsgType second)
        {
            return first.m_Value != second.m_Value;
        }

        #endregion
    }
}