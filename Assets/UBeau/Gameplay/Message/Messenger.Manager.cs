﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UBeau.Gameplay
{
    public sealed partial class Messenger : MonoBehaviour
    {
        static private List<Messenger> s_ProcessQueue = new List<Messenger>();
        static private Dictionary<int, Messenger> s_Registry = new Dictionary<int, Messenger>();

        static private Messenger s_Root;

        /// <summary>
        /// Root Messenger and default parent.
        /// Used for broadcasting and routing messages.
        /// </summary>
        static public Messenger Root
        {
            get
            {
                if (!s_Initialized)
                    Initialize();
                return s_Root;
            }
        }

        #region Initialization

        static private bool s_Initialized = false;
        static private void Initialize()
        {
            if (s_Initialized)
                return;
            s_Initialized = true;

            MsgType.GenerateDefinitions();

            GameObject managerGO = new GameObject("Messenger::Manager");
            managerGO.AddComponent<Manager>();

            s_Root = managerGO.AddComponent<Messenger>();
            //TODO(Alex): Reset parent to null

            managerGO.hideFlags = HideFlags.HideAndDontSave;
            DontDestroyOnLoad(managerGO);
        }

        #endregion

        #region Handlers

        /// <summary>
        /// A message handler that does nothing.
        /// </summary>
        static public readonly MsgHandler EmptyHandler = (m) => { };

        /// <summary>
        /// Returns a message handler that calls the given function.
        /// </summary>
        static public MsgHandler MakeHandler(Action inAction)
        {
            return new MsgHandler((m) => { inAction(); });
        }

        /// <summary>
        /// Returns a message handler that calls the given function
        /// with the message arguments.
        /// </summary>
        static public MsgHandler MakeHandler<T>(Action<T> inAction)
        {
            return new MsgHandler((m) => { inAction((T)m.Args); });
        }

        /// <summary>
        /// Returns a message handler that calls the given function
        /// with the given argument.
        /// </summary>
        static public MsgHandler MakeHandler<T>(Action<T> inAction, T inArg)
        {
            return new MsgHandler((m) => { inAction(inArg); });
        }

        #endregion

        #region Manager

        private sealed class Manager : MonoBehaviour
        {
            private void LateUpdate()
            {
                for (int i = s_ProcessQueue.Count - 1; i >= 0; --i)
                {
                    Messenger m = s_ProcessQueue[i];
                    bool bKeep = m != null && m.ProcessQueue();
                    if (!bKeep)
                        s_ProcessQueue.RemoveAt(i);
                }
            }
        }

        #endregion

        #region Registry

        // Registers the messenger so it can be quickly found later
        static private void RegisterMessenger(Messenger inMessenger)
        {
            s_Registry[inMessenger.gameObject.GetInstanceID()] = inMessenger;
        }

        // UNregisters the messenger
        static private void UnregisterMessenger(Messenger inMessenger)
        {
            s_Registry.Remove(inMessenger.gameObject.GetInstanceID());
        }

        #region Queries

        /// <summary>
        /// Returns the Messenger associated with the given GameObject.
        /// </summary>
        static public Messenger Find(GameObject inGO)
        {
            Messenger m;
            if (!s_Registry.TryGetValue(inGO.GetInstanceID(), out m))
                m = inGO.GetComponent<Messenger>();
            return m;
        }

        /// <summary>
        /// Returns the Messenger associated with the given MonoBehaviour.
        /// </summary>
        static public Messenger Find(MonoBehaviour inBehaviour)
        {
            Messenger m;
            if (!s_Registry.TryGetValue(inBehaviour.gameObject.GetInstanceID(), out m))
                m = inBehaviour.GetComponent<Messenger>();
            return m;
        }

        /// <summary>
        /// Returns the Messenger associated with the given GameObject ID.
        /// </summary>
        static public Messenger Find(int inID)
        {
            Messenger m;
            s_Registry.TryGetValue(inID, out m);
            return m;
        }

        /// <summary>
        /// Gets or creates a Messenger for the given GameObject.
        /// </summary>
        static public Messenger Require(GameObject inGO)
        {
            Messenger m = Find(inGO);
            if (m == null)
                m = inGO.AddComponent<Messenger>();
            return m;
        }

        /// <summary>
        /// Gets or creates a Messenger for the given MonoBehaviour.
        /// </summary>
        static public Messenger Require(MonoBehaviour inBehaviour)
        {
            Messenger m = Find(inBehaviour);
            if (m == null)
                m = inBehaviour.gameObject.AddComponent<Messenger>();
            return m;
        }

        #endregion

        #endregion
    }
}
