﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace UBeau.Gameplay
{
    /// <summary>
    /// Signature for functions that can handle messages.
    /// </summary>
    public delegate void MsgHandler(Message inMessage);

    /// <summary>
    /// Facilitates the passing of messages between objects.
    /// </summary>
    public sealed partial class Messenger : MonoBehaviour
    {
        private Dictionary<MsgType, MsgHandler> m_Handlers;
        private Messenger m_Parent;
        private Queue<Message> m_Queue;

        private readonly MsgHandler m_CachedHandler;

        private Messenger()
        {
            m_Parent = null;
            m_Handlers = new Dictionary<MsgType, MsgHandler>();
            m_Queue = new Queue<Message>();
            m_CachedHandler = TryDispatch;
        }

        /// <summary>
        /// Parent of this Messenger.
        /// Parents are used for broadcasting messages.
        /// </summary>
        public Messenger Parent
        {
            get { return m_Parent; }
        }

        /// <summary>
        /// Switches the parent Messenger.
        /// Only necessary if creating custom broadcasting hierarchies.
        /// </summary>
        public Messenger SetParent(Messenger inParent)
        {
            Assert.False(ReferenceEquals(inParent, this), "Cannot set parent to self!");

            if (inParent != m_Parent)
            {
                foreach(var type in m_Handlers.Keys)
                {
                    if (m_Parent != null)
                        m_Parent.Unregister(type, m_CachedHandler);
                    if (inParent != null)
                        inParent.Register(type, m_CachedHandler);
                }
            }

            m_Parent = inParent;
            ValidateGraph();

            return this;
        }

        #region Handlers

        /// <summary>
        /// Registers a message handler for the given message type.
        /// </summary>
        public Messenger Register(MsgType inType, MsgHandler inHandler)
        {
            Assert.False(inType == MsgType.Null, "Provided type is null!");
            Assert.False(inHandler == null, "Provided handler is null!");

            MsgHandler handler;
            if (m_Handlers.TryGetValue(inType, out handler))
            {
                m_Handlers[inType] = handler + inHandler;
            }
            else
            {
                m_Handlers[inType] = inHandler;
                if (m_Parent != null)
                    m_Parent.Register(inType, m_CachedHandler);
            }

            return this;
        }

        /// <summary>
        /// Removes a message handler from the given message type.
        /// </summary>
        public Messenger Unregister(MsgType inType, MsgHandler inHandler)
        {
            Assert.False(inType == MsgType.Null, "Provided type is null!");
            Assert.False(inHandler == null, "Provided handler is null!");

            MsgHandler handler;
            if (m_Handlers.TryGetValue(inType, out handler))
            {
                handler -= inHandler;
                if (handler == null)
                {
                    m_Handlers.Remove(inType);
                    if (m_Parent != null)
                        m_Parent.Unregister(inType, m_CachedHandler);
                }
                else
                {
                    m_Handlers[inType] = handler;
                }
            }

            return this;
        }

        /// <summary>
        /// Returns if a message handler is registered for the given message type.
        /// </summary>
        public bool IsRegister(MsgType inType)
        {
            return m_Handlers.ContainsKey(inType);
        }

        /// <summary>
        /// Removes all message handlers.
        /// </summary>
        public Messenger Clear()
        {
            if (m_Parent != null)
            {
                foreach (MsgType type in m_Handlers.Keys)
                    m_Parent.Unregister(type, m_CachedHandler);
            }

            m_Handlers.Clear();
            return this;
        }

        #endregion

        #region Raising Events

        /// <summary>
        /// Dispatches a message to this Messenger and its children.
        /// </summary>
        public Messenger Raise(MsgType inType, object inArgs = null)
        {
            Assert.False(inType == MsgType.Null, "Provided type is null!");
            Message msg = new Message(inType, this, inArgs);

            MsgDefinition define = MsgType.Define(inType);
            if (define.HasFlags(MsgFlags.Immediate))
                TryDispatch(msg);
            else
                QueueMessage(msg);

            return this;
        }

        /// <summary>
        /// Dispatches a message from the root Messenger to all children.
        /// </summary>
        public Messenger Broadcast(MsgType inType, object inArgs = null)
        {
            if (m_Parent == null)
            {
                Raise(inType, inArgs);
                return this;
            }

            Assert.False(inType == MsgType.Null, "Provided type is null!");
            Message msg = new Message(inType, this, inArgs);

            Messenger root = m_Parent;
            while (root.m_Parent != null)
                root = root.m_Parent;

            MsgDefinition define = MsgType.Define(inType);
            if (define.HasFlags(MsgFlags.Immediate))
                root.TryDispatch(msg);
            else
                root.QueueMessage(msg);

            return this;
        }

        #endregion

        #region Sending Events

        /// <summary>
        /// Dispatches a message to the given Messenger and its children.
        /// </summary>
        public Messenger SendTo(Messenger inTarget, MsgType inType, object inArgs = null)
        {
            Assert.False(inType == MsgType.Null, "Provided type is null!");
            Assert.False(inTarget == null, "Provided messenger is null!");

            Message msg = new Message(inType, this, inArgs);
            MsgDefinition define = MsgType.Define(inType);
            if (define.HasFlags(MsgFlags.Immediate))
                inTarget.TryDispatch(msg);
            else
                inTarget.QueueMessage(msg);

            return this;
        }

        /// <summary>
        /// Dispatches a message to the Messenger on the given GameObject and its children.
        /// </summary>
        public Messenger SendTo(GameObject inTarget, MsgType inType, object inArgs = null)
        {
            Assert.False(inType == MsgType.Null, "Provided type is null!");
            Assert.False(inTarget == null, "Provided GO is null!");

            Message msg = new Message(inType, this, inArgs);
            MsgDefinition define = MsgType.Define(inType);

            Messenger targetMessenger = Find(inTarget);
            if (targetMessenger == null)
            {
                Assert.False(define.HasFlags(MsgFlags.RequireListener), "Message requires listener!");
                return this;
            }

            if (define.HasFlags(MsgFlags.Immediate))
                targetMessenger.TryDispatch(msg);
            else
                targetMessenger.QueueMessage(msg);

            return this;
        }

        /// <summary>
        /// Dispatches a message to the Messenger on the given MonoBehaviour and its children.
        /// </summary>
        public Messenger SendTo(MonoBehaviour inTarget, MsgType inType, object inArgs = null)
        {
            Assert.False(inType == MsgType.Null, "Provided type is null!");
            Assert.False(inTarget == null, "Provided GO is null!");

            Message msg = new Message(inType, this, inArgs);
            MsgDefinition define = MsgType.Define(inType);

            Messenger targetMessenger = Find(inTarget);
            if (targetMessenger == null)
            {
                Assert.False(define.HasFlags(MsgFlags.RequireListener), "Message requires listener!");
                return this;
            }

            if (define.HasFlags(MsgFlags.Immediate))
                targetMessenger.TryDispatch(msg);
            else
                targetMessenger.QueueMessage(msg);

            return this;
        }

        #endregion

        #region Temporary

        /// <summary>
        /// Creates a temporary handler for the given message type.
        /// </summary>
        public Binding RegisterTemp(MsgType inType, MsgHandler inHandler)
        {
            return new Binding(this, inType, inHandler);
        }

        /// <summary>
        /// Creates a temporary handler for the given message type.
        /// </summary>
        public Binding RegisterTemp(MsgType inType)
        {
            return new Binding(this, inType, EmptyHandler);
        }

        #endregion

        #region Routines

        /// <summary>
        /// Waits for the given message type to be received.
        /// </summary>
        public IEnumerator WaitFor(MsgType inType)
        {
            using (Binding binding = RegisterTemp(inType))
                while (!binding.WasHit)
                    yield return null;
        }

        #endregion

        #region Unity Events

        private void Awake()
        {
            if (!s_Initialized)
                Initialize();
            SetParent(s_Root);
            RegisterMessenger(this);
        }

        private void OnDestroy()
        {
            Clear();
            SetParent(null);
            UnregisterMessenger(this);
        }

        #endregion

        #region Internal

        // Processes the messages available at the
        // end of the frame
        private bool ProcessQueue()
        {
            if (isActiveAndEnabled)
            {
                int msgCount = m_Queue.Count;
                while (msgCount-- > 0)
                    TryDispatch(m_Queue.Dequeue());
            }

            return m_Queue.Count > 0;
        }

        // Attempts to dispatch the message
        private void TryDispatch(Message inMessage)
        {
            MsgDefinition define = MsgType.Define(inMessage.Type);

            if (define.HasFlags(MsgFlags.Force) || isActiveAndEnabled)
            {
                MsgHandler handler;
                if (m_Handlers.TryGetValue(inMessage.Type, out handler))
                    handler(inMessage);
                else
                    Assert.False(define.HasFlags(MsgFlags.RequireListener));
            }
            else if (!define.HasFlags(MsgFlags.Discard))
                QueueMessage(inMessage);
        }

        // Queues to be processed at the end of the frame
        private void QueueMessage(Message inMessage)
        {
            if (m_Queue.Count == 0)
                s_ProcessQueue.Add(this);
            m_Queue.Enqueue(inMessage);
        }

        // Makes sure the graph is single-directional
        [Conditional("DEVELOPMENT")]
        private void ValidateGraph()
        {
            using (PooledSet<Messenger> visited = PooledSet<Messenger>.Create())
            {
                Messenger current = this;
                while(current != null)
                {
                    if (visited.Contains(current))
                    {
                        Assert.Fail("Invalid or looping hierarchy, starting with {0}", gameObject.name);
                        return;
                    }

                    visited.Add(current);
                    current = current.m_Parent;
                }
            }
        }

        #endregion
    }
}
