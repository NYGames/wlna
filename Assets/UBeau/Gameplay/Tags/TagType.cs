﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace UBeau.Gameplay
{
    /// <summary>
    /// Represents a type of tag used for classifying objects.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public sealed class TagType : Attribute
    {
        public TagType(byte inIndex, string inName)
        {
            Name = inName;
            Index = inIndex;
            Mask = (1 << inIndex);
        }

        public string Name { get; private set; }
        public byte Index { get; private set; }
        public int Mask { get; private set; }
    }

    /// <summary>
    /// Shows a list of tag types to select from.
    /// </summary>
    public class TagRefAttribute : PropertyAttribute
    {
#if UNITY_EDITOR
        [CustomPropertyDrawer(typeof(TagRefAttribute), true)]
        private class Editor : PropertyDrawer
        {
            public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
            {
                Initialize();

                Rect indentedPos = EditorGUI.IndentedRect(position);
                float toShift = EditorGUIUtility.labelWidth - 2 * (indentedPos.x - position.x);

                EditorGUI.BeginProperty(indentedPos, label, property);
                {
                    label.tooltip = Tag.ToString(property.intValue);

                    GUI.Label(indentedPos, label);

                    Rect popupRect = indentedPos;
                    popupRect.x += toShift;
                    popupRect.width -= toShift;

                    property.intValue = EditorGUI.MaskField(popupRect, property.intValue, s_TagNames);
                }
                EditorGUI.EndProperty();
            }

            #region Tag cache

            static private bool s_Initialized = false;
            static private string[] s_TagNames;

            static private void Initialize()
            {
                if (s_Initialized)
                    return;

                s_Initialized = true;

                Tag.Process();
                s_TagNames = new string[Tag.MaxBit + 1];
                for (int i = 0; i <= Tag.MaxBit; ++i)
                {
                    TagType type = Tag.FindType((byte)i);
                    s_TagNames[i] = type == null ? "[???]" : type.Name;
                }
            }

            #endregion
        }
#endif
    }
}
