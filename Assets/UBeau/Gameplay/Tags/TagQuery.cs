﻿namespace UBeau.Gameplay
{
    /// <summary>
    /// Query for searching the tag database.
    /// </summary>
    public struct TagQuery
    {
        /// <summary>
        /// Tags to search.
        /// </summary>
        [TagRef]
        public int Tags;

        /// <summary>
        /// Tags to exclude.
        /// </summary>
        [TagRef]
        public int Exclude;
    }
}
