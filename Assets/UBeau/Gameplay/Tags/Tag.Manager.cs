﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UBeau;

namespace UBeau.Gameplay
{
    public sealed partial class Tag : MonoBehaviour
    {
        static private bool s_Initialized = false;

        static private void Initialize()
        {
            if (s_Initialized)
                return;

            s_Initialized = true;

            Process();
        }

        // List of all Tag objects with the bits set.
        static private List<Tag>[] s_TagLists = new List<Tag>[Bits.LENGTH];

        // Adds a tag to the given bit list
        static private void AddTag(Tag inTag, byte inBit)
        {
            List<Tag> list = s_TagLists[inBit];
            if (list == null)
                list = s_TagLists[inBit] = new List<Tag>();

            list.Add(inTag);
        }

        // Removes a tag from the given bit list
        static private void RemoveTag(Tag inTag, byte inBit)
        {
            List<Tag> list = s_TagLists[inBit];
            if (list != null)
                list.Remove(inTag);
        }

        /// <summary>
        /// Queries the tag list and adds all matching Tags to a list.
        /// Returns if any were found.
        /// </summary>
        static public bool Query(TagQuery inQuery, IList<Tag> ioTags)
        {
            if (inQuery.Exclude != 0)
                return Query(inQuery.Tags, inQuery.Exclude, ioTags);
            return Query(inQuery.Tags, ioTags);
        }

        /// <summary>
        /// Queries the tag list and adds all matching Tags to a list.
        /// Returns if any were found.
        /// </summary>
        static public int Count(TagQuery inQuery)
        {
            if (inQuery.Exclude != 0)
                return Count(inQuery.Tags, inQuery.Exclude);
            return Count(inQuery.Tags);
        }

        #region Registry

        static private Dictionary<int, Tag> s_TagIDs = new Dictionary<int, Tag>();

        static private void AddTagObject(Tag inTag)
        {
            s_TagIDs[inTag.gameObject.GetInstanceID()] = inTag;
        }

        static private void RemoveTagObject(Tag inTag)
        {
            s_TagIDs.Remove(inTag.gameObject.GetInstanceID());
        }

        /// <summary>
        /// Returns the Tag attached to the given GameObject.
        /// </summary>
        static public Tag Find(GameObject inGameObject)
        {
            Tag m;
            if (!s_TagIDs.TryGetValue(inGameObject.GetInstanceID(), out m))
                m = inGameObject.GetComponent<Tag>();
            return m;
        }

        /// <summary>
        /// Returns the Tag attached to the given MonoBehaviour's gameObject.
        /// </summary>
        static public Tag Find(MonoBehaviour inBehavior)
        {
            Tag m;
            if (!s_TagIDs.TryGetValue(inBehavior.gameObject.GetInstanceID(), out m))
                m = inBehavior.GetComponent<Tag>();
            return m;
        }

        /// <summary>
        /// Returns the Tag associated with the given GameObject ID.
        /// </summary>
        static public Tag Find(int inGameObjectID)
        {
            Tag m;
            s_TagIDs.TryGetValue(inGameObjectID, out m);
            return m;
        }

        /// <summary>
        /// Gets or creates a Tag on the given MonoBehaviour's gameObject.
        /// </summary>
        static public Tag Require(MonoBehaviour inBehaviour)
        {
            Tag m = Find(inBehaviour.gameObject);
            if (m == null)
                m = inBehaviour.gameObject.AddComponent<Tag>();
            return m;
        }

        /// <summary>
        /// Gets or creates a Tag on the given GameObject.
        /// </summary>
        static public Tag Require(GameObject inGameObject)
        {
            Tag m = Find(inGameObject);
            if (m == null)
                m = inGameObject.AddComponent<Tag>();
            return m;
        }

        #endregion

        #region Query

        /// <summary>
        /// Queries the tag list and adds all matching Tags to a list.
        /// Returns if any were found.
        /// </summary>
        static public bool Query(int inTags, IList<Tag> ioTags)
        {
            Assert.NotNull(ioTags, "Cannot provide null list.");

            bool bFound = false;
            for (int bit = s_MinBit; bit <= s_MaxBit; ++bit)
            {
                if (Bits.Contains(inTags, (byte)bit))
                {
                    List<Tag> tags = s_TagLists[bit];
                    if (tags != null)
                    {
                        for (int i = tags.Count - 1; i >= 0; --i)
                        {
                            Tag t = tags[i];
                            if (t != null)
                            {
                                bFound = true;
                                if (!ioTags.Contains(t))
                                    ioTags.Add(t);
                            }
                        }
                    }

                    Bits.Remove(ref inTags, (byte)bit);
                    if (inTags == 0)
                        break;
                }
            }

            return bFound;
        }

        /// <summary>
        /// Queries the tag list and adds all matching Tags to a list.
        /// Returns if any were found.
        /// </summary>
        static public bool Query(int inTags, int inExclude, IList<Tag> ioTags)
        {
            Assert.NotNull(ioTags, "Cannot provide null list.");

            // If they are equal nothing will match
            if (inTags == inExclude)
                return false;

            bool bFound = false;
            for (int bit = s_MinBit; bit <= s_MaxBit; ++bit)
            {
                if (Bits.Contains(inTags, (byte)bit))
                {
                    List<Tag> tags = s_TagLists[bit];
                    if (tags != null)
                    {
                        for (int i = tags.Count - 1; i >= 0; --i)
                        {
                            Tag t = tags[i];
                            if (t != null && !Bits.ContainsMask(t.m_Tags, inExclude))
                            {
                                bFound = true;
                                if (!ioTags.Contains(t))
                                    ioTags.Add(t);
                            }
                        }
                    }

                    Bits.Remove(ref inTags, (byte)bit);
                    if (inTags == 0)
                        break;
                }
            }

            return bFound;
        }

        #endregion

        #region Count

        /// <summary>
        /// Queries the tag list and returns the number of objects that match.
        /// </summary>
        static public int Count(int inTags)
        {
            using (PooledList<Tag> query = PooledList<Tag>.Create())
            {
                Query(inTags, query);
                return query.Count;
            }
        }

        /// <summary>
        /// Queries the tag list and returns the number of objects that match.
        /// </summary>
        static public int Count(int inTags, int inExclude)
        {
            using (PooledList<Tag> query = PooledList<Tag>.Create())
            {
                Query(inTags, inExclude, query);
                return query.Count;
            }
        }

        #endregion

        #region Types

        #region Static Accessors

        static private int s_MinBit = 31;
        static private int s_MaxBit = 0;
        static private TagType[] s_TagTypes = new TagType[Bits.LENGTH];

        /// <summary>
        /// Max tag index to check.
        /// </summary>
        static public int MaxBit
        {
            get { return s_MaxBit; }
        }

        /// <summary>
        /// Min tag index to check.
        /// </summary>
        static public int MinBit
        {
            get { return s_MinBit; }
        }

        /// <summary>
        /// Returns the Tag type at the given index.
        /// </summary>
        static public TagType FindType(byte inIndex)
        {
            return s_TagTypes[inIndex];
        }

        /// <summary>
        /// Returns a string representation of all tags
        /// toggled on in the given bitstring.
        /// </summary>
        static public string ToString(int inTags)
        {
            using (PooledStringBuilder builder = PooledStringBuilder.Create())
            {
                for(byte i = (byte)s_MinBit; i <= s_MaxBit; ++i)
                {
                    if (Bits.Contains(inTags, i))
                    {
                        if (builder.Builder.Length > 0)
                            builder.Builder.Append(", ");

                        TagType type = s_TagTypes[i];
                        if (type == null)
                            builder.Builder.Append("[???]");
                        else
                            builder.Builder.Append('[').Append(type.Name).Append(']');
                    }
                }

                if (builder.Builder.Length == 0)
                    builder.Builder.Append("NONE");

                return builder.Builder.ToString();
            }
        }

        #endregion

        #region Processing

        static private HashSet<Assembly> s_Processed = new HashSet<Assembly>();

        /// <summary>
        /// Assigns all auto-generated handles.
        /// </summary>
        static public void Process()
        {
            ProcessAssembly(Assembly.GetAssembly(typeof(TagType)));
        }

        static private void ProcessAssembly(Assembly inAssembly)
        {
            if (s_Processed.Contains(inAssembly))
                return;

            s_Processed.Add(inAssembly);

            using (var t = Timer.Start("TagType Processing"))
            {
                Log.Debug("Current Assembly: {0}", inAssembly.FullName);
                var types = inAssembly.GetTypes();
                for (int i = 0; i < types.Length; ++i)
                    ProcessType(types[i]);
            }
        }

        static private void ProcessType(Type inType)
        {
            FieldInfo[] fields = inType.GetFields(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
            for (int i = 0; i < fields.Length; ++i)
            {
                TagType tag = Types.GetCustomAttribute<TagType>(fields[i]);
                if (tag != null)
                {
                    Assert.True(fields[i].IsStatic, "Cannot assign tag type to static field.");
                    Assert.True(fields[i].FieldType == typeof(int), "Cannot assign tag type to non-integer field.");

                    if (s_TagTypes[tag.Index] != null)
                        Assert.Fail("Tags {0} and {1} have the same index {2}", tag.Name, s_TagTypes[tag.Index].Name, tag.Index);
                    s_TagTypes[tag.Index] = tag;

                    if (tag.Index < s_MinBit)
                        s_MinBit = tag.Index;
                    if (tag.Index > s_MaxBit)
                        s_MaxBit = tag.Index;

                    fields[i].SetValue(null, tag.Mask);
                }
            }
        }

        #endregion

        #endregion
    }
}
