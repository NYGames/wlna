﻿using System;
using UnityEngine;
using UBeau;

namespace UBeau.Gameplay
{
    /// <summary>
    /// Classifies objects in a database and provides
    /// an interface for querying against it.
    /// </summary>
    [DisallowMultipleComponent]
    public sealed partial class Tag : MonoBehaviour
    {
        [SerializeField]
        [TagRef]
        private int m_Tags;

        [NonSerialized]
        private int m_PrevTags = 0;

        private void Awake()
        {
            Initialize();
            RegisterTags();
            AddTagObject(this);
        }

        private void OnDestroy()
        {
            m_Tags = 0;
            RegisterTags();
            RemoveTagObject(this);
        }

        /// <summary>
        /// Current tags applied to the object.
        /// </summary>
        public int Tags
        {
            get { return m_Tags; }
            set
            {
                if (m_Tags != value)
                {
                    m_Tags = value;
                    RegisterTags();
                }
            }
        }

        /// <summary>
        /// Returns if any of the given tags are applied.
        /// </summary>
        public bool HasTags(int inTags)
        {
            return (m_Tags & inTags) != 0;
        }

        /// <summary>
        /// Returns if all of the given tags are applied.
        /// </summary>
        public bool HasAllTags(int inTags)
        {
            return (m_Tags & inTags) == inTags;
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            if (!UnityEditor.EditorApplication.isPlaying)
                return;

            // if tags have changed, we should check
            Initialize();
            RegisterTags();
        }
#endif

        // Checks for changes to our tag registration
        private void RegisterTags()
        {
            if (m_Tags != m_PrevTags)
            {
                for(byte i = (byte)MinBit; i <= MaxBit; ++i)
                {
                    bool bHas = Bits.Contains(m_Tags, i);
                    bool bPrev = Bits.Contains(m_PrevTags, i);

                    if (bHas != bPrev)
                    {
                        if (bHas)
                            AddTag(this, i);
                        else
                            RemoveTag(this, i);
                    }
                }

                m_PrevTags = m_Tags;
            }
        }
    }
}