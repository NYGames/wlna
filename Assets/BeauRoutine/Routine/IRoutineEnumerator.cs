﻿using System;
using System.Collections;

namespace BeauRoutine
{
    /// <summary>
    /// Specialization of IEnumerator.
    /// Has specific callbacks for interacting with Routines.
    /// </summary>
    public interface IRoutineEnumerator : IEnumerator, IDisposable
    {
        /// <summary>
        /// Called when the routine is added to the stack.
        /// Returns whether to continue.
        /// </summary>
        bool OnRoutineStart();
    }
}
