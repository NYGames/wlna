﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

namespace BeauRoutine
{
    /// <summary>
    /// Contains helper functions for generating routines.
    /// </summary>
    static public class RoutineShortcuts
    {
        #region Animator

        /// <summary>
        /// Waits for the animator to either reach the end of the current state
        /// or switch to a different state.
        /// </summary>
        static public IEnumerator WaitToCompleteAnimation(this Animator inAnimator, int inLayer = 0)
        {
            AnimatorStateInfo stateInfo = inAnimator.GetCurrentAnimatorStateInfo(inLayer);
            int initialHash = stateInfo.fullPathHash;
			float initialNormalizedTime = stateInfo.normalizedTime;
			while(true)
			{
				yield return null;
				stateInfo = inAnimator.GetCurrentAnimatorStateInfo(inLayer);
				if (stateInfo.fullPathHash != initialHash
					|| stateInfo.normalizedTime >= 1 || stateInfo.normalizedTime < initialNormalizedTime)
					break;
			}
        }

		/// <summary>
		/// Waits for the animator to play and exit the given state.
		/// </summary>
		static public IEnumerator WaitToFinishState(this Animator inAnimator, string inStateName, int inLayer = 0)
		{
			yield return WaitForState(inAnimator, inStateName, inLayer);
			yield return WaitForNotState(inAnimator, inStateName, inLayer);
		}

		/// <summary>
		/// Waits for the animator to be in the given state.
		/// </summary>
		static public IEnumerator WaitForState(this Animator inAnimator, string inStateName, int inLayer = 0)
		{
			while (true)
			{
				AnimatorStateInfo stateInfo = inAnimator.GetCurrentAnimatorStateInfo(inLayer);
				if (stateInfo.IsName(inStateName))
					yield break;
				yield return null;
			}
		}

		/// <summary>
		/// Waits for the animator to stop being in the given state.
		/// </summary>
		static public IEnumerator WaitForNotState(this Animator inAnimator, string inStateName, int inLayer = 0)
		{
			while (true)
			{
				AnimatorStateInfo stateInfo = inAnimator.GetCurrentAnimatorStateInfo(inLayer);
				if (!stateInfo.IsName(inStateName))
					yield break;
				yield return null;
			}
		}

        #endregion

        #region AudioSource

        /// <summary>
        /// Waits for the AudioSource to stop playing.
        /// Make sure it's not looping.
        /// </summary>
        static public IEnumerator WaitToFinish(this AudioSource inAudioSource)
        {
            while (inAudioSource.isPlaying)
                yield return null;
        }

        #endregion

		#region Threading

		/// <summary>
		/// Waits for the given thread to finish running.
		/// </summary>
		static public IEnumerator WaitToComplete(this Thread inThread)
		{
			while (inThread.IsAlive)
				yield return null;
		}

		#endregion
	}
}
