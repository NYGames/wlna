﻿namespace BeauRoutine
{
    /// <summary>
    /// How an euler rotation should be tweened.
    /// </summary>
    public enum AngleMode : byte
    {
        /// <summary>
        /// Rotate along the shortest path.
        /// </summary>
        Shortest,

        /// <summary>
        /// Rotate directly to the value.
        /// Helpful for rotating more than 180 degrees.
        /// </summary>
        Absolute
    }
}
