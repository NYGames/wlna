﻿namespace BeauRoutine
{
    /// <summary>
    /// Premade easing functions.
    /// </summary>
    public enum Curve : byte
    {
        Linear = 0,
        Smooth,

        QuadIn,
        QuadOut,
        QuadInOut,

        CubeIn,
        CubeOut,
        CubeInOut,

        QuartIn,
        QuartOut,
        QuartInOut,

        QuintIn,
        QuintOut,
        QuintInOut,

        BackIn,
        BackOut,
        BackInOut,

        SineIn,
        SineOut,
        SineInOut,

        CircleIn,
        CircleOut,
        CircleInOut,

        BounceIn,
        BounceOut,
        BounceInOut,

        ElasticIn,
        ElasticOut,
        ElasticInOut
    }
}
