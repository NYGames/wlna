﻿namespace BeauRoutine
{
    /// <summary>
    /// Contains tweening values and how to apply them.
    /// </summary>
    public interface ITweenData
    {
        void OnTweenStart();
        void ApplyTween(float inPercent);
        void OnTweenEnd();
    }
}
