﻿using System;

namespace BeauRoutine
{
    /// <summary>
    /// Basic tween settings in a struct.
    /// Useful for grouping animations in the inspector.
    /// </summary>
    [Serializable]
    public struct TweenSettings
    {
        /// <summary>
        /// Duration of the tween.
        /// </summary>
        public float Time;

        /// <summary>
        /// Easing function for the value.
        /// </summary>
        public Curve Curve;
    }
}
