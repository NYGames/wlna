﻿namespace BeauRoutine
{
    /// <summary>
    /// How a color should be tweened.
    /// </summary>
    public enum ColorUpdate : byte
    {
        /// <summary>
        /// Alpha will be tweened.
        /// </summary>
        FullColor,

        /// <summary>
        /// The alpha of the initial value will be preserved.
        /// In the case of renderers, the alpha of the renderer
        /// during the tween will be preserved to allow Color
        /// and Alpha tweens to function concurrently.
        /// </summary>
        PreserveAlpha
    }
}
