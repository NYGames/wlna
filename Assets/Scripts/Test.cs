﻿using UBeau.Gameplay;
using UnityEngine;

public class Test : MonoBehaviour {

    [MsgDefinition(MsgFlags.Default, "Some Bullshit Name")]
    static public readonly MsgType MSG_TEST;

    [MsgRef]
    public MsgType MSG;
}
